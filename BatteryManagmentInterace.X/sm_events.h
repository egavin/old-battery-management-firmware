/****************************************************************************
 ** Copyright (c) 2021 Supira Medical
 ** Author(s): rmerchant, Ed Gavin
 ** Purpose: Event names for all state machines
 ****************************************************************************/

#ifndef SM_EVENTS_H
#define	SM_EVENTS_H
#include <stdint.h>


typedef enum
{
  SM_NO_EVENT = 0,
  SM_ERROR,                 // used to indicate an error from the service 
  SM_INIT,                  // used to transition from initial pseudo-state   
  SM_TIMEOUT,               // signals that the timer has expired  
  SM_ENTRY,                 // signal entry of state 
  SM_EXIT,                  // signals exit of state 
  /* User-defined events start here */
  SMBUS_START,              // signals SMBus machine to initiate process      
  SMBUS_SSPIF_SET,          // signals interrupt flag is set                  
  SMBUS_EOT,                // signals the most recent SMBus protocol done    
  SMBUS_BUS_COLL,           // signals there was a bus collision
  SMBUS_P_FND,              // signals a stop bit was found on the bus
  PD_F_EDGE,                // signals there was a falling edge               
  PD_R_EDGE,                // signals there was a rising edge                
  /*PC_DATA_RDY, */              // signals the PC has sent data                   
  RR_READ_DONE,             // signals reading of desired register is done
  RR_START_READ,            // signals to ReadRegFSM to begin read process          
  RR_START_WRITE,           // signals to ReadRegFSM to begin write process
  RR_WRITE_DONE,            // write register done        
  LED_GRN_C,                // signals change to constant green
  LED_YLW_C,                // signals change to constant yellow  
  LED_YLW_F,                // signals change to yellow flashing              
  LED_RED_C,                // signals change to constant red
  LED_RED_F,                // signals change to red flashing       
  LED_OFF,
  ES_NEW_KEY,               // signals a new key received from terminal   
  SET_SHIPMENT_MODE,        // signals to set shipment mode        
  EVENT_LIST_END            // keep this as the end of the list        
}SM_EventType_t;

typedef struct 
{   
    SM_EventType_t EventType;
    uint16_t data;
}event_t;






char* GetEventName(SM_EventType_t e);
    
#endif	/* SM_EVENTS_H */