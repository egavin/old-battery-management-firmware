/****************************************************************************
 ** Copyright (c) 2021 Supira Medical
 ** Author(s): rmerchant, Ed Gavin
 ** Purpose: Event names for all state machines
 ****************************************************************************/

#include <stdint.h>
#include "sm_events.h"

static char* eventNames[] = {
    "SM_NO_EVENT",
     "SM_ERROR",
    "SM_INIT",
    "SM_TIMEOUT",
    "SM_ENTRY",
    "SM_EXIT",  
    "SMBUS_START",
    "SMBUS_SSPIF_SET",          // signals interrupt flag is set                  
    "SMBUS_EOT",                // signals the most recent SMBus protocol done    
    "SMBUS_BUS_COLL",           // signals there was a bus collision
    "SMBUS_P_FND",              // signals a stop bit was found on the bus
    "PD_F_EDGE",                // signals there was a falling edge               
    "PD_R_EDGE",                // signals there was a rising edge                    
    "RR_READ_DONE",             // signals reading of desired register is done
    "RR_START_READ",            // signals to ReadRegFSM to begin read process
    "RR_START_WRITE",
    "RR_WRITE_DONE",
    "LED_GRN_C",                // signals change to constant green
    "LED_YLW_C",                // signals change to constant yellow  
    "LED_YLW_F",                // signals change to yellow flashing              
    "LED_RED_C",                // signals change to constant red
    "LED_RED_F",                // signals change to fast red flashing       
    "LED_OFF",
    "ES_NEW_KEY",               // signals a new key received from terminal  
    "SET_SHIPMENT_MODE",        // set batteries to shipment mode
 };


/**
 * @function GetEventName
 * @param e
 * @return char* pointer to event name
 */
char* GetEventName(SM_EventType_t e)
{            
    if(e >= EVENT_LIST_END){        
        return "Error: past end of Event Names!";
    }
    return eventNames[(uint8_t)e];
}