/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Authors: rmerchant, Ed Gavin
** Purpose: Provides a light weight but limited printf
****************************************************************************/
#include "sm_port.h"
void DB_printf(const char *Format, ...);

// Note: these definitions are for a little Endian processor
#define LOWORD(l) (*((unsigned int *)(&l)))
#define HIWORD(l) (*(((unsigned int *)(&l))+1))
char* GetSecondsMinutesHours(void);

#define printf    DB_printf

//#undef DEBUG_PRINT_ENABLED

#ifdef DEBUG_PRINT_ENABLED
#define DBG_printf printf
#else
#define DBG_printf(fmt, ...)  (0)
#endif

void DBG_OUT(uint8_t Level,  const char* Format, ...);