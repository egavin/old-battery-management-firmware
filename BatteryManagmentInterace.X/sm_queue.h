/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Author(s): Ed Gavin
** Purpose:  functions for event queues
****************************************************************************/

#ifndef SM_QUEUE_H
#define	SM_QUEUE_H
#include <stdint.h>
#include <stdbool.h>
#include "sm_events.h"

// #define TEST_QUEUE 1

#define MAX_EVENTS 16

typedef struct 
{
    uint8_t queueSize;
    uint8_t currentIndex;
    uint8_t numEntries;
} queue_info;

typedef struct queue_t
{
    queue_info info;
    event_t events[MAX_EVENTS];        
}queue_t;

uint8_t initQueue(queue_t* pQueue);
bool EnqueueFIFO(queue_t* pQueue, event_t newEvent);
uint8_t Dequeue(queue_t*, event_t *event);
bool isQueueEmpty(queue_t* pQueue);


#ifdef TEST_QUEUE
bool SM_QueueTest(void);
void printQueue(queue_t* pQueue);
#endif // TEST_QUEUE

#endif	/* SM_QUEUE_H */

