/****************************************************************************
 ** Copyright (c) 2021 Supira Medical
 ** Authors: R. Merchant, Ed Gavin
 ****************************************************************************/

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include "mcc_generated_files/device_config.h"
#include "mcc_generated_files/pin_manager.h"
#include "SMBusMaster.h"
#include "BattStat.h"
#include "sm_main.h"
#include "sm_events.h"
#include "sm_queue.h"
#include "sm_timer.h"
#if  defined(SMBUS_ERROR_DEBUG) || defined(SMBUS_I2C_DEBUG)
#define DEBUG_PRINT_ENABLED
#include "dbprintf.h"
#endif

#undef SMBUS_I2C_DEBUG
/*----------------------------- Module Defines ----------------------------*/
#define WAIT_TIME 10
#define TIMEOUT_TIME 10
#define MAX_RETRIES 10
#define IMPROVING_STATES 1
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
 */
static void SMBus_HWInit(void);
static void PostEOT(void);
/*---------------------------- Module Variables ---------------------------*/
// variable to hold current state of machine (defined in header enum)
static SMBusState_t currentState;
// variable to hold protocol machine needs to execute (enum in header)
static SMBusMode_t modeSelect;
// variable to hold the target slave's address (7-bit ONLY!)
static uint8_t targetAddr;
// variable to hold the command to write
static uint8_t cmd2Send;
// module level pointer to write to/read from user given buffer
static uint8_t* pBuffer;

static uint8_t retries;

// module level var to determine number of bytes left
static uint8_t numBytesLeft;
// module level var to store current error state
static SMBusError_t currentError = SMBUS_NO_ERROR;
// flag to enable event checker to look for a stop bit
static bool enChk4Stop = false;

#ifdef KEYSTROKE_DEBUG
static uint8_t testBuffer[2] = {0x55, 0x18};
#endif

/* The Event Queue for this state machine */
static queue_t* pQueue;

/**
 * InitSMBusFSM
 * @param pq pointer to FSM's queue
 * @return true if init event is queued, false otherwise.
 */
bool InitSMBusFSM(queue_t* pq) {
    event_t ThisEvent;
    pQueue = pq;

    // Initialize hardware
    SMBus_HWInit();
    // put us into the Initial PseudoState
    currentState = InitPState;
    // post the initial transition event
    ThisEvent.EventType = SM_INIT;
    //if (ES_PostToService(MyPriority, ThisEvent) == true)
    return EnqueueFIFO(pQueue, ThisEvent);
}

/**
 * Posts an event to the SMBus queue
 * @param ThisEvent
 * @return true if the event is queued, false otherwise.
 */
bool PostSMBusFSM(event_t ThisEvent) {
    return EnqueueFIFO(pQueue, ThisEvent);
}

char* GetSMBusErrorString(uint16_t error) {
    static char* errorNames[] = {
        "SMBUS_NO_ERROR", "ADDR_NACK", "CMD_NACK", "DATA_NACK", "BUS_COLLISION",
        "SMBUS_BUSY", "INVALID_ADDRESS", "INVALID_CMD", "INVALID_MODE", "SMBUS_TIMEOUT"
    };

    if ((uint16_t) error > SMBUS_TIMEOUT) {
        return "";
    }

    return errorNames[(uint16_t) error];
}

/**
 * RunSMBusFSM is state machine to handle low level SMBus communications.
 * @param ThisEvent
 * @return
 */
__reentrant event_t RunSMBusFSM(event_t ThisEvent) {
#ifdef TRACE_OUT
    DBG_printf("RunSMBusFSM() %s\n", GetEventName(ThisEvent.EventType));
#endif
    event_t ReturnEvent, CurrentEvent;
    ReturnEvent.EventType = SM_NO_EVENT; // assume no errors

    // Variables for state transitions
    bool makeTransition = false; //assume no transition
    SMBusState_t nextState = currentState; // next state to transition to
    // first time reading/sending a byte flag (for block read and write)
    static bool firstByte = true;

    switch (currentState) {
        case InitPState: // If current state is initial Pseudo State
        {
#ifdef TRACE_OUT
            DBG_printf("RunSMBusFSM() InitPState\n");
#endif
            if (ThisEvent.EventType == SM_INIT) // only respond to ES_Init
            {
                // Put machine into Idle state
                nextState = SMBusIdle;
                makeTransition = true;
            }
        }
            break;
            /*------------------------------------------------------------- SMBusIdle */
        case SMBusIdle: // If current state is idle
        {
#ifdef TRACE_OUT
            DBG_printf("RunSMBusFSM() SMBusIdle\n");
#endif

            if (ThisEvent.EventType == SMBUS_START) {
                // clear the collision bit
                if (SSP1CON1bits.WCOL) {
                    SSP1CON1bits.WCOL = 0;
                }
                if (SSP1CON1bits.SSPOV) {
                    SSP1CON1bits.SSPOV = 0;
                }

                // if SCL or SDA is low, try giving it 500 uS to clear the bus
                if (I2C_SCL_GetValue() == false || I2C_SDA_GetValue() == false) {
                    for (uint8_t i = 0; i < 5; ++i) {
                        __delay_us(100);
                        if (I2C_SCL_GetValue() && I2C_SDA_GetValue()) {
                            continue;
                        }
                    }
                }
                // if the stop was detected bus is idle
                if (SSP1STATbits.P || !(SSP1STATbits.P | SSP1STATbits.S)) {
                    SSP1CON2bits.SEN = 1;

                    // set transition to SendingStart
                    nextState = SendingStart;
                    makeTransition = true;
                } else //bus is busy already, can't hold line
                {
#ifdef SMBUS_I2C_DEBUG
                    DBG_printf("* SMBusIdle detecting Bus Busy. Retries: %d P:%u S%u WCOL:%u %u \r\n", retries, SSP1STATbits.P, SSP1STATbits.S, SSP1CON1bits.WCOL);
#endif
                    if (retries++ < MAX_RETRIES) {
                        event_t Event2Post = {SMBUS_START, 0};
                        // disable and re-enable the  the MSSP
                        SSP1CON1bits.SSPEN = 0;
                        __delay_ms(1);
                        SSP1CON1bits.SSPEN = 1;
                        //
                        PostSMBusFSM(Event2Post);
                    } else {
                        retries = 0;
                        currentError = SMBUS_TIMEOUT;
                        //post to everyone
                        PostEOT();
                    }
                }
            }// entry function
            else if (ThisEvent.EventType == SM_ENTRY) {
                // clear error status
                currentError = SMBUS_NO_ERROR;
                // set the firstByte flag
                firstByte = true;
            }
            // there is no exit function
        }
            break;
            /*---------------------------------------------------------- SendingStart */
        case SendingStart: // If machine is sending START
        {
#ifdef TRACE_OUT
            DBG_printf("RunSMBusFSM() SendingStart\n");
#endif
            switch (ThisEvent.EventType) {
                    // event is MSSP interrupt condition
                case SMBUS_SSPIF_SET:
                {
                    //Load address to buffer
                    if (modeSelect == RECV_BYTE) {
                        SSP1BUF = (unsigned char) ((targetAddr << 1) + 1);
                    } else {
                        SSP1BUF = (unsigned char) (targetAddr << 1);
                    }
                    // set transition to SendingAddr
                    nextState = SendingAddr;
                    makeTransition = true;
                }
                    break;
                    // Event is a bus collision
                case SMBUS_BUS_COLL:
                {
                    // set current error to bus collision
                    currentError = BUS_COLLISION;
                    // Post EOT to everyone
                    PostEOT();

                    // transition back to idle
                    nextState = SMBusIdle;
                    makeTransition = true;
                }
                    break;

                    // there is no entry function
                    // there is no exit function
                default:
                {
                }
                    break;
            }
        }
            break;
            /*----------------------------------------------------------- SendingAddr */
        case SendingAddr: // If machine is shifting out write address
        {
            switch (ThisEvent.EventType) {
                case SMBUS_SSPIF_SET:

                    if (SSP1CON2bits.ACKSTAT == 0) {
                        switch (modeSelect) //Choose action based on mode
                        {
                            case SEND_BYTE:
                                nextState = SendingBytes; // transition to sending byte
                                break;
                            case RECV_BYTE:
                                nextState = ReceivingBytes;
                                break;

                            default: // all other modes use this sequence
                            {
                                SSP1BUF = cmd2Send; // load command into buffer
                                // transition to Sending Command
                                nextState = SendingCommand;
                            }
                        }
                        makeTransition = true;

                    } else // else slave did not ack
                    {
#ifdef SMBUS_ERROR_DEBUG
                        DBG_printf("RunSMBusFSM() ADDR_NACK in SendingAddr\n");
#endif
                        currentError = ADDR_NACK; // set error to ADDR_NACK
                        nextState = SendingStop; // transition to sending stop
                        makeTransition = true;

                    }
                    timer_StopTimer(SMBUS_TIMER);
                    break;

                case SM_ENTRY:
                    timer_InitTimer(SMBUS_TIMER, 100);
                    break;

                case SM_TIMEOUT:
                    if (ThisEvent.data == SMBUS_TIMER) {
                        currentError = SMBUS_TIMEOUT;
                        PostEOT(); // Post EOT to everyone
                        nextState = SMBusIdle;
                        makeTransition = true;
                    }
                    break;
                default:
                    break;
            } // end of switch ThisEvent.EventType
        } // end of case SendingAddr
            break;

            /*-------------------------------------------------------- SendingCommand */
        case SendingCommand:
        {
            switch (ThisEvent.EventType) {

                case SMBUS_SSPIF_SET:
                    timer_StopTimer(SMBUS_TIMER);
                    if (SSP1CON2bits.ACKSTAT == 0) {
                        // if in Read_Word or Read_Block mode
                        if ((modeSelect == READ_WORD)
                                || (modeSelect == READ_BLOCK)) {
                            // transition to Sending repeated start
                            nextState = SendingRepStart;
                        }// else if in Write_Word or Write_Block mode
                        else if ((modeSelect == WRITE_WORD)
                                || (modeSelect == WRITE_BLOCK)) {
                            // transition to Sending bytes
                            nextState = SendingBytes;
                        }
                        makeTransition = true;
                    } else // slave did not ack
                    {
                        // mark error
#ifdef SMBUS_ERROR_DEBUG
                        DBG_printf("RunSMBusFSM() ADDR_NACK in SendingCommand\n");
#endif
                        currentError = CMD_NACK;
                        // transition to sending stop
                        nextState = SendingStop;
                        makeTransition = true;
                    }                    
                    break;
                case SM_ENTRY:
                    timer_InitTimer(SMBUS_TIMER, 100);
                    break;
                case SM_TIMEOUT:
                    if (ThisEvent.data == SMBUS_TIMER) {
                        currentError = SMBUS_TIMEOUT;
                        PostEOT(); // Post EOT to everyone
                        nextState = SMBusIdle;
                        makeTransition = true;
                    }
                    break;
                default:
                    break;

            } // end of switch ThisEvent
        } // end of switch SendingCommand
            break;


            /*---------------------------------------------------------- SendingBytes */
        case SendingBytes: // If machine is shifting out byte to slave
        {
            // if interrupt flag set and there is an ACK
            if ((ThisEvent.EventType == SMBUS_SSPIF_SET)
                    && (SSP1CON2bits.ACKSTAT == 0)) {
                // if there are bytes left to write
                if (numBytesLeft) {
                    // perform an external self-transition
                    nextState = SendingBytes;
                    makeTransition = true;
                }// else no bytes left to send
                else {
                    // transition to SendingStop
                    nextState = SendingStop;
                    makeTransition = true;
                }
            } else if (ThisEvent.EventType == SMBUS_SSPIF_SET)
                    // slave did not ack
            {
                // mark error
                currentError = DATA_NACK;
                // transition to sending stop
                nextState = SendingStop;
                makeTransition = true;
            }// @Entry_function
            else if (ThisEvent.EventType == SM_ENTRY) {
                // if writing block send block size and first byte to send
                if ((modeSelect == WRITE_BLOCK)
                        && firstByte) {
                    SSP1BUF = numBytesLeft;
                }// else load SSP1BUF with next byte
                else {
                    SSP1BUF = *pBuffer;
                    // increment buffer pointer
                    pBuffer++;
                    // decrement byte count
                    numBytesLeft--;
                }

                // set first byte to false
                firstByte = false;
            }
            // there is no exit function
        }
            break;
            /*------------------------------------------------------- SendingRepStart */
        case SendingRepStart: // Machine is sending a repeated START
        {
            if (ThisEvent.EventType == SM_TIMEOUT && ThisEvent.data == SMBUS_TIMER) {
                // timed out waiting for the Interrupt from the MSSP
                currentError = SMBUS_TIMEOUT;
                // Post EOT to everyone
                PostEOT();

                // transition back to idle
                nextState = SMBusIdle;
                makeTransition = true;
            }
            if (ThisEvent.EventType == SMBUS_SSPIF_SET) {

                // load read address to buffer
                SSP1BUF = (unsigned char) ((targetAddr << 1) + 1);

                // transition to SendingAddrR
                nextState = SendingAddrR;
                makeTransition = true;
                timer_StopTimer(SMBUS_TIMER);
            }// Entry function
            else if (ThisEvent.EventType == SM_ENTRY) {
                // start the timer
                timer_InitTimer(SMBUS_TIMER, 100);
                // set RSEN
                SSP1CON2bits.RSEN = 1;
            }
            // There is no exit function
        }
            break;
            /*---------------------------------------------------------- SendingAddrR */
        case SendingAddrR:
        {
            if (ThisEvent.EventType == SMBUS_SSPIF_SET) {
                // if slave ack
                if (SSP1CON2bits.ACKSTAT == 0) {
                    // transition to Receiving Byte
                    nextState = ReceivingBytes;
                    makeTransition = true;
                } else // else slave did not ack
                {
#ifdef SMBUS_ERROR_DEBUG
                    DBG_printf("RunSMBusFSM() ADDR_NACK in SendingAddrR\n");
#endif
                    // set error to ADDR_NACK
                    currentError = ADDR_NACK;
                    // transition to sending stop
                    nextState = SendingStop;
                    makeTransition = true;
                }
            }
            // there is no exit function
            // there is no entry function
        }
            break;
            /*-------------------------------------------------------- ReceivingBytes */
        case ReceivingBytes: //If machine is shifting in byte from slave
        {
            if (ThisEvent.EventType == SMBUS_SSPIF_SET) {
                timer_StopTimer(SMBUS_TIMER);
                // if reading block send block size and first byte to send
                if ((modeSelect == READ_BLOCK)
                        && firstByte) {
                    // set the number of bytes left to first byte
                    numBytesLeft = SSP1BUF + 1;
                }
                // load received byte into user buffer
                *pBuffer = SSP1BUF;
                // increment buffer pointer
                pBuffer++;
                // decrement byte count
                numBytesLeft--;
                // Set/clear ADCDT bit accordingly
                SSP1CON2bits.ACKDT = (numBytesLeft == 0);
                // Set ACKEN to send ACK bit to slave
                SSP1CON2bits.ACKEN = 1;
                // clear firstByte flag
                firstByte = false;

                // Transition to SendingAckStat
                nextState = SendingAckStat;
                makeTransition = true;
            }// @Entry_function
            else if (ThisEvent.EventType == SM_ENTRY) {
                // Set RCEN
                SSP1CON2bits.RCEN = 1;
                timer_InitTimer(SMBUS_TIMER, 100);
            } else if(ThisEvent.EventType == SM_TIMEOUT && ThisEvent.data == SMBUS_TIMER){
                currentError = SMBUS_TIMEOUT;
                PostEOT(); // Post EOT to everyone
                nextState = SendingStop;
                makeTransition = true;
            }
            // there is no exit function
        }
            break;
            /*-------------------------------------------------------- SendingAckStat */

        case SendingAckStat:
        {
            if(ThisEvent.EventType == SMBUS_SSPIF_SET){
                timer_StopTimer(SMBUS_TIMER);
                if (numBytesLeft) {
                        // transition
                        nextState = ReceivingBytes;
                        makeTransition = true;
                    } else {                    
                    // else transition to SendingStop
                    nextState = SendingStop;
                    makeTransition = true;
                    }
            } else if(ThisEvent.EventType == SM_ENTRY){                
                timer_InitTimer(SMBUS_TIMER, 100);
            } else if(ThisEvent.EventType == SM_TIMEOUT && ThisEvent.data == SMBUS_TIMER){
                currentError = SMBUS_TIMEOUT;
                PostEOT(); // Post EOT to everyone
                nextState = SendingStop;
                makeTransition = true;
            }
        }
        break;

            /*----------------------------------------------------------- SendingStop */
//#if 0            
        case SendingStop:
        {
            switch(ThisEvent.EventType){
                case SMBUS_SSPIF_SET:
                    timer_StopTimer(SMBUS_TIMER);                    
                    // Post EOT to everyone
                    PostEOT();
                    // go back to idling
                    nextState = SMBusIdle;
                    makeTransition = true;
                    break;
                case SM_ENTRY:
                     // Set the PEN bit
                    SSP1CON2bits.PEN = 1;
                    timer_InitTimer(SMBUS_TIMER, 100);
                    break;
                case SM_TIMEOUT:
                    if (ThisEvent.data == SMBUS_TIMER) {
                        currentError = SMBUS_TIMEOUT;
                        PostEOT(); // Post EOT to everyone
                        nextState = SMBusIdle;
                        makeTransition = true;
                    }
                    break;
                default:
                    break;   
            }// end of switch EventType
                    
        } // end of sending stop
        break;

            /*---------------------------------------------------------- Waiting4Stop */
        case Waiting4Stop:
        {

            if (ThisEvent.EventType == SMBUS_P_FND || SSP1STATbits.P) {
                // set SEN
                SSP1CON2bits.SEN = 1;
                // transition to sending start
                nextState = SendingStart;
                makeTransition = true;
            } else if (ThisEvent.EventType == SM_TIMEOUT) {
#ifdef SMBUS_ERROR_DEBUG
                DBG_printf("RunSMBusFSM() Waiting4Stop timeout enChk4Stop: %u\n", enChk4Stop);
#endif
                PostEOT();
            }
            // there is no entry function
            // there is no exit function
        }
            break;
            /* -------------------------------------------------------- END OF STATES */
            // repeat state pattern as required for other states
        default:
            ;
    } // end switch on Current State
    // check to see if we are making a transition
    if (makeTransition) {
        // re-run machine to perform exit actions
#if 0
        CurrentEvent.EventType = SM_EXIT;
        ReturnEvent = RunSMBusFSM(CurrentEvent);
#endif
        // set current state to next state
        currentState = nextState;
        // re-run machine to perform entry actions
        CurrentEvent.EventType = SM_ENTRY;
        ReturnEvent = RunSMBusFSM(CurrentEvent);
    }

    return ReturnEvent;
}

/*******************************************************************************
 * Function: QueryI2CSM
 * Arguments: None
 * Returns currentState
 *
 * Created by: R. Merchant
 * Description: Simple getter function for the state of the machine
 ******************************************************************************/
SMBusState_t QuerySMBusFSM(void) {
    return currentState;
}

/**
 * Description: Sets up the machine to write/read word to/from the slave.
 * NOTE: The byte buffer pointer given to this function should be a persistent
 *       variable in the scope of whatever module is using the SMBus machine
 * @param slaveAddr Slave address, typically the battery address
 * @param command
 * @param mode
 * @param pByteBuff
 * @return SMBUS_NO_ERROR for success, specific errors for problems
 */
SMBusError_t SMBus_ReadWriteWord(uint8_t slaveAddr, uint8_t command,
        SMBusMode_t mode, uint8_t *pByteBuff) {
    SMBusError_t funcErr = SMBUS_NO_ERROR;
    static event_t Event2Post = {SMBUS_START, 0};
    retries = 0;
    // check for valid address and command
    if (slaveAddr > 0x7f) {
        funcErr = INVALID_ADDR;
    } else if ((mode != READ_WORD)&&(mode != WRITE_WORD)) {
        funcErr = INVALID_MODE;
    } else if (command > MAX_CMD_VAL) {
        funcErr = INVALID_CMD;
    } else if (currentState != SMBusIdle) {
#ifdef SMBUS_ERROR_DEBUG
        DBG_printf("SMBus_ReadWriteWord() currentState != SMBusIdle currentState:%u\n", currentState);
#endif
        funcErr = SMBUS_BUSY;
    } else // no user errors
    {
        // copy over address
        targetAddr = slaveAddr;
        // copy over the command
        cmd2Send = command;
        //  set mode
        modeSelect = mode;
        // copy pointer to machine buffer pointer
        pBuffer = pByteBuff;
        // set byte count to 2
        numBytesLeft = 2;
        // Post SMBUS_START event
        PostSMBusFSM(Event2Post);
    }
    // return error
    return funcErr;
}

/**
 *  Description: Sets up the machine to send or receive a byte to/from slave.
 * NOTE: The byte pointer given to this function should be a persistent
 *       variable in the scope of whatever module is using the SMBus machine
 * @param slaveAddr
 * @param mode
 * @param pByte
 * @return
 */
SMBusError_t SMBus_SendRecvByte(uint8_t slaveAddr, SMBusMode_t mode,
        uint8_t* pByte) {
    SMBusError_t funcErr = SMBUS_NO_ERROR;
    static event_t Event2Post = {SMBUS_START, 0};
    // check for valid address and command
    if (slaveAddr > 0x7f) {
        funcErr = INVALID_ADDR;
    } else if ((mode != SEND_BYTE)&& (mode != RECV_BYTE)) {
        funcErr = INVALID_MODE;
    } else if (currentState != SMBusIdle) {
        funcErr = SMBUS_BUSY;
    } else // no user errors
    {
        // copy over address
        targetAddr = slaveAddr;
        //  set mode
        modeSelect = mode;
        // copy byte pointer to buffer pointer
        pBuffer = pByte;
        // set byte count to 1
        numBytesLeft = 1;
        // Post SMBUS_START event
        PostSMBusFSM(Event2Post);
    }
    // return error
    return funcErr;
}
/*******************************************************************************
 * Function: SMBus_ReadBlock
 * Arguments: None
 * Returns nothing
 *
 * Created by: R. Merchant

 ******************************************************************************/

/**
 * Description: Sets up machine to perform a block read
 * NOTE: User must allocate at least 33 bytes for the buffer given to ensure
 *       that all data the slave sends back with be accounted for. SMBus
 *       supports block reads up to 32 bytes.
 * @param slaveAddr
 * @param command
 * @param pByteBuff
 * @return SMBUS_NO_ERROR for success, error otherwise
 */

SMBusError_t SMBus_ReadBlock(uint8_t slaveAddr, uint8_t command,
        uint8_t *pByteBuff) {
    SMBusError_t funcErr = SMBUS_NO_ERROR;
    static event_t Event2Post = {SMBUS_START, 0};
    // check for valid address and command
    if (slaveAddr > 0x7f) {
        funcErr = INVALID_ADDR;
    } else if (command > MAX_CMD_VAL) {
        funcErr = INVALID_CMD;
    } else if (currentState != SMBusIdle) {
        funcErr = SMBUS_BUSY;
    } else // no user errors
    {
        // copy over address
        targetAddr = slaveAddr;
        // copy over the command
        cmd2Send = command;
        //  set mode
        modeSelect = READ_BLOCK;
        // copy pointer to machine buffer pointer
        pBuffer = pByteBuff;
        // set byte count to 33, the maximum block size with size in index 0
        numBytesLeft = 33;
        // Post SMBUS_START event
        PostSMBusFSM(Event2Post);
    }
    // return error
    return funcErr;
}

/**
 * Description: Sets up machine to perform a block write
 * @param slaveAddr
 * @param command
 * @param buffSize
 * @param pByteBuff
 * @return
 */
SMBusError_t SMBus_WriteBlock(uint8_t slaveAddr, uint8_t command,
        uint8_t buffSize, uint8_t *pByteBuff) {
    SMBusError_t funcErr = SMBUS_NO_ERROR;
    static event_t Event2Post = {SMBUS_START, 0};
    // check for valid address and command
    if (slaveAddr > 0x7f) {
        funcErr = INVALID_ADDR;
    } else if (command > MAX_CMD_VAL) {
        funcErr = INVALID_CMD;
    } else if (currentState != SMBusIdle) {
        funcErr = SMBUS_BUSY;
    } else // no user errors
    {
        // copy over address
        targetAddr = slaveAddr;
        // copy over the command
        cmd2Send = command;
        //  set mode
        modeSelect = WRITE_BLOCK;
        // copy pointer to machine buffer pointer
        pBuffer = pByteBuff;
        // set byte count to 33, the maximum block size with size in index 0
        numBytesLeft = buffSize;
        // Post SMBUS_START event
        PostSMBusFSM(Event2Post);
    }
    // return error
    return funcErr;
}
/***************************************************************************
 Event Checkers
 ***************************************************************************/

/**
 *  Description: Triggers event if the interrupt flag is set. Clears the flag
 *  much like and ISR would.
 * Check4IntFlag
 * @return true if event has occured, false otherwise
 */
bool Check4IntFlag_SMBus(void) {
#ifndef KEYSTROKE_DEBUG
    static event_t Event2Post = {SMBUS_SSPIF_SET, 0};
    // check to see if interrupt flag has been set
    if (PIR3bits.SSP1IF) {
#ifdef TRACE_OUT
        DBG_printf("Check4IntFlag_SMBus happened!\n");
#endif
        // clear the flag
        PIR3bits.SSP1IF = 0;
        // post the event
        PostSMBusFSM(Event2Post);
        // return true
        return true;
    } else {
        return false;
    }
#else
    return false;
#endif
}

/**
 * Description: Triggers event if the interrupt flag is set.
 * Clears the flag much like and ISR would.
 * @return true if there is a bus collision event, false otherwise
 */
bool Check4BusColl_SMBus(void) {
    static event_t Event2Post = {SMBUS_BUS_COLL, 0};
#ifndef KEYSTROKE_DEBUG
    //Check bus collision interrupt flag
    if (PIR3bits.BCL1IF) {
        // clear flag
        PIR3bits.BCL1IF = 0; // clear the interrupt flag

        // disable and re-enable the  the MSSP
        SSP1CON1bits.SSPEN = 0;
        __delay_ms(1);
        SSP1CON1bits.SSPEN = 1;

        // post the event
        PostSMBusFSM(Event2Post);
        // return true
        return true;
    } else // If flag is clear
    {
        return false;
    }
#else
    return false;
#endif
}

/**
 * Description: Triggers event if a stop bit was last detected on the bus
 * @return true if there is a bus stop condition, false otherwise
 */
bool Check4Stop_SMBus(void) {
    static event_t Event2Post = {SMBUS_P_FND, 0};
    // if event checker is enabled and P bit is set
    if (enChk4Stop & SSP1STATbits.P) {
        //disable the event checker
        enChk4Stop = false;
        // post the event
        PostSMBusFSM(Event2Post);
        // return true
        return true;
    } else {
        return false;
    }
}

/**
 *  Description: Sets up MSSP1 as an I2C master with 100kHz clock
 */
static void SMBus_HWInit(void) {
    // Ensure unit is disabled
    SSP1CON1bits.SSPEN = 0;
    // Set mode for I2C master
    SSP1CON1 = (SSP1CON1 & 0xf0) | 0b1000;
    // Disable slew rate control (using 100kHz clock)
    SSP1STATbits.SMP = 1;
    // Enable SMBus levels
    SSP1STATbits.CKE = 1;
    // Set clock speed to 100kHz
    SSP1ADD = 159;
    SSP1CON3bits.SDAHT = 1;

    // Set RC3 and RC4 and digital inputs
    ANSELCbits.ANSELC3 = 0;
    ANSELCbits.ANSELC4 = 0;
    TRISCbits.TRISC3 = 1;
    TRISCbits.TRISC4 = 1;

    SSP1CLKPPS = 0x13; // I2C clock on RC3
    SSP1DATPPS = 0x14; // I2C data on RC4
    RC3PPS = 0x0F;
    RC4PPS = 0x10;
    // Enable the MSSP
    SSP1CON1bits.SSPEN = 1;

    return;
}

/**
 * function PostEOT
 * Arguments: none
 * Description: Posts the EOT event to all queues.
 */
static void PostEOT(void) {
    event_t EOTEvent;
    EOTEvent.EventType = SMBUS_EOT;
#ifdef SMBUS_ERROR_DEBUG
    if (0 != currentError) {
        DBG_printf("SMBus:PostEOT error:%d\n", currentError);
    }
#endif
    EOTEvent.data = (uint16_t) currentError;
    SM_PostAll(EOTEvent);
    return;
}

/**
 * Reset the SMBus state machine
 */
void SMBusReset() {
    currentError = SMBUS_NO_ERROR;
    currentState = SMBusIdle;
    initQueue(pQueue);
    timer_StopTimer(SMBUS_TIMER);
    InitSMBusFSM(pQueue);
}

/**
 * If the SDA line is stuck low by a slave, turn the MSPP off and
 * toggle the clock line to try and unfreeze the slave.
 */
void I2C_Check() {

    // disable the MSPS
    SSP1CON1bits.SSPEN = 0;

#ifdef TRACE_OUT
    DBG_printf("%s *** I2C_Check:: clocking ***\r\n", GetSecondsMinutesHours());
#endif
    // send 9 clock pulses to try and free the bus
    SSP1CON1bits.SSPEN = 0; // Disable I2C
    I2C_SCL_SetDigitalOutput();
    for (uint8_t j = 0; j < 10; ++j) {
        for (uint8_t i = 0; i < 9; ++i) {
            I2C_SCL_SetLow();
            __delay_us(10);
            I2C_SCL_SetHigh();
            __delay_us(10);
        }
        if (I2C_SDA_GetValue() == 1) {
            break;
        }
    }
    // restore the hardware settings
    SMBus_HWInit();
    SSP1CON1bits.SSPEN = 1; // turn MSSP on
    //SSP1CON2bits.PEN = 1; // send a stop


}
