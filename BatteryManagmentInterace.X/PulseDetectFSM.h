/****************************************************************************
 ** Copyright (c) 2021 Supira Medical
 ** Author(s): rmerchant, Ed Gavin
 ** Purpose: Module detecting battery charger GPIO lines for charger status
 ****************************************************************************/

#ifndef PulseDetectFSM_H
#define PulseDetectFSM_H
#include "sm_queue.h"

#define USE_PIN_STRUCT
// Event Definitions
#include <stdint.h>
#include <stdbool.h>

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  PD_InitPState = 0xff,
  ConstHi = 1,  /* Pin is high all the time */
  ConstLo = 0,  /* Pin is low all the time */
  Blinking = 2  /* Pin is experiencing a pulsing signal */
}PulseState_t;

// define struct to return all info about the pins
typedef struct
{
  PulseState_t state; // What state is the pin in?
  uint8_t value; // What is its most recent value?
  uint16_t period; // What is the most recent period?
}PinInfo_t;

typedef enum // list of the possible ways the pins are behaving
{
    PIN_LOW = 0, PIN_HIGH, BLINK_400, BLINK_2000
} PinInterp_t;

#define PD_NUM_PINS 6
#if PD_NUM_PINS == 0
#error Invalid number of pins selected
#endif

//definitions of the GPIO pinout
#define GPIO1A 2
#define GPIO2A 3
#define GPIO1B 0
#define GPIO2B 1
#define GPIO1C 4
#define GPIO2C 5

// Public Function Prototypes

bool InitPulseDetectFSM(queue_t* pq);
bool PostPulseDetectFSM(event_t ThisEvent);
event_t RunPulseDetectFSM(event_t ThisEvent);
void QueryPulseDetectFSM(PinInfo_t pInfo[]);

bool Check4PinChanges(void);
#endif /* PulseDetectFSM_H */

