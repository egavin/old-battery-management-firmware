/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  @Description
    This header file provides APIs for driver for .
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC18F46Q10
        Driver Version    :  2.11
    The generated drivers are tested against the following:
        Compiler          :  XC8 2.31 and above
        MPLAB 	          :  MPLAB X 5.45	
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

/**
  Section: Included Files
*/

#include <xc.h>

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set GPIO1B aliases
#define GPIO1B_TRIS                 TRISAbits.TRISA0
#define GPIO1B_LAT                  LATAbits.LATA0
#define GPIO1B_PORT                 PORTAbits.RA0
#define GPIO1B_WPU                  WPUAbits.WPUA0
#define GPIO1B_OD                   ODCONAbits.ODCA0
#define GPIO1B_ANS                  ANSELAbits.ANSELA0
#define GPIO1B_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define GPIO1B_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define GPIO1B_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define GPIO1B_GetValue()           PORTAbits.RA0
#define GPIO1B_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define GPIO1B_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define GPIO1B_SetPullup()          do { WPUAbits.WPUA0 = 1; } while(0)
#define GPIO1B_ResetPullup()        do { WPUAbits.WPUA0 = 0; } while(0)
#define GPIO1B_SetPushPull()        do { ODCONAbits.ODCA0 = 0; } while(0)
#define GPIO1B_SetOpenDrain()       do { ODCONAbits.ODCA0 = 1; } while(0)
#define GPIO1B_SetAnalogMode()      do { ANSELAbits.ANSELA0 = 1; } while(0)
#define GPIO1B_SetDigitalMode()     do { ANSELAbits.ANSELA0 = 0; } while(0)

// get/set GPIO2B aliases
#define GPIO2B_TRIS                 TRISAbits.TRISA1
#define GPIO2B_LAT                  LATAbits.LATA1
#define GPIO2B_PORT                 PORTAbits.RA1
#define GPIO2B_WPU                  WPUAbits.WPUA1
#define GPIO2B_OD                   ODCONAbits.ODCA1
#define GPIO2B_ANS                  ANSELAbits.ANSELA1
#define GPIO2B_SetHigh()            do { LATAbits.LATA1 = 1; } while(0)
#define GPIO2B_SetLow()             do { LATAbits.LATA1 = 0; } while(0)
#define GPIO2B_Toggle()             do { LATAbits.LATA1 = ~LATAbits.LATA1; } while(0)
#define GPIO2B_GetValue()           PORTAbits.RA1
#define GPIO2B_SetDigitalInput()    do { TRISAbits.TRISA1 = 1; } while(0)
#define GPIO2B_SetDigitalOutput()   do { TRISAbits.TRISA1 = 0; } while(0)
#define GPIO2B_SetPullup()          do { WPUAbits.WPUA1 = 1; } while(0)
#define GPIO2B_ResetPullup()        do { WPUAbits.WPUA1 = 0; } while(0)
#define GPIO2B_SetPushPull()        do { ODCONAbits.ODCA1 = 0; } while(0)
#define GPIO2B_SetOpenDrain()       do { ODCONAbits.ODCA1 = 1; } while(0)
#define GPIO2B_SetAnalogMode()      do { ANSELAbits.ANSELA1 = 1; } while(0)
#define GPIO2B_SetDigitalMode()     do { ANSELAbits.ANSELA1 = 0; } while(0)

// get/set GPIO1A aliases
#define GPIO1A_TRIS                 TRISAbits.TRISA2
#define GPIO1A_LAT                  LATAbits.LATA2
#define GPIO1A_PORT                 PORTAbits.RA2
#define GPIO1A_WPU                  WPUAbits.WPUA2
#define GPIO1A_OD                   ODCONAbits.ODCA2
#define GPIO1A_ANS                  ANSELAbits.ANSELA2
#define GPIO1A_SetHigh()            do { LATAbits.LATA2 = 1; } while(0)
#define GPIO1A_SetLow()             do { LATAbits.LATA2 = 0; } while(0)
#define GPIO1A_Toggle()             do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define GPIO1A_GetValue()           PORTAbits.RA2
#define GPIO1A_SetDigitalInput()    do { TRISAbits.TRISA2 = 1; } while(0)
#define GPIO1A_SetDigitalOutput()   do { TRISAbits.TRISA2 = 0; } while(0)
#define GPIO1A_SetPullup()          do { WPUAbits.WPUA2 = 1; } while(0)
#define GPIO1A_ResetPullup()        do { WPUAbits.WPUA2 = 0; } while(0)
#define GPIO1A_SetPushPull()        do { ODCONAbits.ODCA2 = 0; } while(0)
#define GPIO1A_SetOpenDrain()       do { ODCONAbits.ODCA2 = 1; } while(0)
#define GPIO1A_SetAnalogMode()      do { ANSELAbits.ANSELA2 = 1; } while(0)
#define GPIO1A_SetDigitalMode()     do { ANSELAbits.ANSELA2 = 0; } while(0)

// get/set GPIO2A aliases
#define GPIO2A_TRIS                 TRISAbits.TRISA3
#define GPIO2A_LAT                  LATAbits.LATA3
#define GPIO2A_PORT                 PORTAbits.RA3
#define GPIO2A_WPU                  WPUAbits.WPUA3
#define GPIO2A_OD                   ODCONAbits.ODCA3
#define GPIO2A_ANS                  ANSELAbits.ANSELA3
#define GPIO2A_SetHigh()            do { LATAbits.LATA3 = 1; } while(0)
#define GPIO2A_SetLow()             do { LATAbits.LATA3 = 0; } while(0)
#define GPIO2A_Toggle()             do { LATAbits.LATA3 = ~LATAbits.LATA3; } while(0)
#define GPIO2A_GetValue()           PORTAbits.RA3
#define GPIO2A_SetDigitalInput()    do { TRISAbits.TRISA3 = 1; } while(0)
#define GPIO2A_SetDigitalOutput()   do { TRISAbits.TRISA3 = 0; } while(0)
#define GPIO2A_SetPullup()          do { WPUAbits.WPUA3 = 1; } while(0)
#define GPIO2A_ResetPullup()        do { WPUAbits.WPUA3 = 0; } while(0)
#define GPIO2A_SetPushPull()        do { ODCONAbits.ODCA3 = 0; } while(0)
#define GPIO2A_SetOpenDrain()       do { ODCONAbits.ODCA3 = 1; } while(0)
#define GPIO2A_SetAnalogMode()      do { ANSELAbits.ANSELA3 = 1; } while(0)
#define GPIO2A_SetDigitalMode()     do { ANSELAbits.ANSELA3 = 0; } while(0)

// get/set GPIO1C aliases
#define GPIO1C_TRIS                 TRISAbits.TRISA4
#define GPIO1C_LAT                  LATAbits.LATA4
#define GPIO1C_PORT                 PORTAbits.RA4
#define GPIO1C_WPU                  WPUAbits.WPUA4
#define GPIO1C_OD                   ODCONAbits.ODCA4
#define GPIO1C_ANS                  ANSELAbits.ANSELA4
#define GPIO1C_SetHigh()            do { LATAbits.LATA4 = 1; } while(0)
#define GPIO1C_SetLow()             do { LATAbits.LATA4 = 0; } while(0)
#define GPIO1C_Toggle()             do { LATAbits.LATA4 = ~LATAbits.LATA4; } while(0)
#define GPIO1C_GetValue()           PORTAbits.RA4
#define GPIO1C_SetDigitalInput()    do { TRISAbits.TRISA4 = 1; } while(0)
#define GPIO1C_SetDigitalOutput()   do { TRISAbits.TRISA4 = 0; } while(0)
#define GPIO1C_SetPullup()          do { WPUAbits.WPUA4 = 1; } while(0)
#define GPIO1C_ResetPullup()        do { WPUAbits.WPUA4 = 0; } while(0)
#define GPIO1C_SetPushPull()        do { ODCONAbits.ODCA4 = 0; } while(0)
#define GPIO1C_SetOpenDrain()       do { ODCONAbits.ODCA4 = 1; } while(0)
#define GPIO1C_SetAnalogMode()      do { ANSELAbits.ANSELA4 = 1; } while(0)
#define GPIO1C_SetDigitalMode()     do { ANSELAbits.ANSELA4 = 0; } while(0)

// get/set GPIO2C aliases
#define GPIO2C_TRIS                 TRISAbits.TRISA5
#define GPIO2C_LAT                  LATAbits.LATA5
#define GPIO2C_PORT                 PORTAbits.RA5
#define GPIO2C_WPU                  WPUAbits.WPUA5
#define GPIO2C_OD                   ODCONAbits.ODCA5
#define GPIO2C_ANS                  ANSELAbits.ANSELA5
#define GPIO2C_SetHigh()            do { LATAbits.LATA5 = 1; } while(0)
#define GPIO2C_SetLow()             do { LATAbits.LATA5 = 0; } while(0)
#define GPIO2C_Toggle()             do { LATAbits.LATA5 = ~LATAbits.LATA5; } while(0)
#define GPIO2C_GetValue()           PORTAbits.RA5
#define GPIO2C_SetDigitalInput()    do { TRISAbits.TRISA5 = 1; } while(0)
#define GPIO2C_SetDigitalOutput()   do { TRISAbits.TRISA5 = 0; } while(0)
#define GPIO2C_SetPullup()          do { WPUAbits.WPUA5 = 1; } while(0)
#define GPIO2C_ResetPullup()        do { WPUAbits.WPUA5 = 0; } while(0)
#define GPIO2C_SetPushPull()        do { ODCONAbits.ODCA5 = 0; } while(0)
#define GPIO2C_SetOpenDrain()       do { ODCONAbits.ODCA5 = 1; } while(0)
#define GPIO2C_SetAnalogMode()      do { ANSELAbits.ANSELA5 = 1; } while(0)
#define GPIO2C_SetDigitalMode()     do { ANSELAbits.ANSELA5 = 0; } while(0)

// get/set RB0 procedures
#define RB0_SetHigh()            do { LATBbits.LATB0 = 1; } while(0)
#define RB0_SetLow()             do { LATBbits.LATB0 = 0; } while(0)
#define RB0_Toggle()             do { LATBbits.LATB0 = ~LATBbits.LATB0; } while(0)
#define RB0_GetValue()              PORTBbits.RB0
#define RB0_SetDigitalInput()    do { TRISBbits.TRISB0 = 1; } while(0)
#define RB0_SetDigitalOutput()   do { TRISBbits.TRISB0 = 0; } while(0)
#define RB0_SetPullup()             do { WPUBbits.WPUB0 = 1; } while(0)
#define RB0_ResetPullup()           do { WPUBbits.WPUB0 = 0; } while(0)
#define RB0_SetAnalogMode()         do { ANSELBbits.ANSELB0 = 1; } while(0)
#define RB0_SetDigitalMode()        do { ANSELBbits.ANSELB0 = 0; } while(0)

// get/set LED_BAT_STAT_YELLOW aliases
#define LED_BAT_STAT_YELLOW_TRIS                 TRISBbits.TRISB1
#define LED_BAT_STAT_YELLOW_LAT                  LATBbits.LATB1
#define LED_BAT_STAT_YELLOW_PORT                 PORTBbits.RB1
#define LED_BAT_STAT_YELLOW_WPU                  WPUBbits.WPUB1
#define LED_BAT_STAT_YELLOW_OD                   ODCONBbits.ODCB1
#define LED_BAT_STAT_YELLOW_ANS                  ANSELBbits.ANSELB1
#define LED_BAT_STAT_YELLOW_SetHigh()            do { LATBbits.LATB1 = 1; } while(0)
#define LED_BAT_STAT_YELLOW_SetLow()             do { LATBbits.LATB1 = 0; } while(0)
#define LED_BAT_STAT_YELLOW_Toggle()             do { LATBbits.LATB1 = ~LATBbits.LATB1; } while(0)
#define LED_BAT_STAT_YELLOW_GetValue()           PORTBbits.RB1
#define LED_BAT_STAT_YELLOW_SetDigitalInput()    do { TRISBbits.TRISB1 = 1; } while(0)
#define LED_BAT_STAT_YELLOW_SetDigitalOutput()   do { TRISBbits.TRISB1 = 0; } while(0)
#define LED_BAT_STAT_YELLOW_SetPullup()          do { WPUBbits.WPUB1 = 1; } while(0)
#define LED_BAT_STAT_YELLOW_ResetPullup()        do { WPUBbits.WPUB1 = 0; } while(0)
#define LED_BAT_STAT_YELLOW_SetPushPull()        do { ODCONBbits.ODCB1 = 0; } while(0)
#define LED_BAT_STAT_YELLOW_SetOpenDrain()       do { ODCONBbits.ODCB1 = 1; } while(0)
#define LED_BAT_STAT_YELLOW_SetAnalogMode()      do { ANSELBbits.ANSELB1 = 1; } while(0)
#define LED_BAT_STAT_YELLOW_SetDigitalMode()     do { ANSELBbits.ANSELB1 = 0; } while(0)

// get/set RB3 procedures
#define RB3_SetHigh()            do { LATBbits.LATB3 = 1; } while(0)
#define RB3_SetLow()             do { LATBbits.LATB3 = 0; } while(0)
#define RB3_Toggle()             do { LATBbits.LATB3 = ~LATBbits.LATB3; } while(0)
#define RB3_GetValue()              PORTBbits.RB3
#define RB3_SetDigitalInput()    do { TRISBbits.TRISB3 = 1; } while(0)
#define RB3_SetDigitalOutput()   do { TRISBbits.TRISB3 = 0; } while(0)
#define RB3_SetPullup()             do { WPUBbits.WPUB3 = 1; } while(0)
#define RB3_ResetPullup()           do { WPUBbits.WPUB3 = 0; } while(0)
#define RB3_SetAnalogMode()         do { ANSELBbits.ANSELB3 = 1; } while(0)
#define RB3_SetDigitalMode()        do { ANSELBbits.ANSELB3 = 0; } while(0)

// get/set PWR_12_VOLT_EN aliases
#define PWR_12_VOLT_EN_TRIS                 TRISBbits.TRISB4
#define PWR_12_VOLT_EN_LAT                  LATBbits.LATB4
#define PWR_12_VOLT_EN_PORT                 PORTBbits.RB4
#define PWR_12_VOLT_EN_WPU                  WPUBbits.WPUB4
#define PWR_12_VOLT_EN_OD                   ODCONBbits.ODCB4
#define PWR_12_VOLT_EN_ANS                  ANSELBbits.ANSELB4
#define PWR_12_VOLT_EN_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define PWR_12_VOLT_EN_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define PWR_12_VOLT_EN_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define PWR_12_VOLT_EN_GetValue()           PORTBbits.RB4
#define PWR_12_VOLT_EN_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define PWR_12_VOLT_EN_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define PWR_12_VOLT_EN_SetPullup()          do { WPUBbits.WPUB4 = 1; } while(0)
#define PWR_12_VOLT_EN_ResetPullup()        do { WPUBbits.WPUB4 = 0; } while(0)
#define PWR_12_VOLT_EN_SetPushPull()        do { ODCONBbits.ODCB4 = 0; } while(0)
#define PWR_12_VOLT_EN_SetOpenDrain()       do { ODCONBbits.ODCB4 = 1; } while(0)
#define PWR_12_VOLT_EN_SetAnalogMode()      do { ANSELBbits.ANSELB4 = 1; } while(0)
#define PWR_12_VOLT_EN_SetDigitalMode()     do { ANSELBbits.ANSELB4 = 0; } while(0)

// get/set BATT_ALARM_SPEAKER aliases
#define BATT_ALARM_SPEAKER_TRIS                 TRISBbits.TRISB5
#define BATT_ALARM_SPEAKER_LAT                  LATBbits.LATB5
#define BATT_ALARM_SPEAKER_PORT                 PORTBbits.RB5
#define BATT_ALARM_SPEAKER_WPU                  WPUBbits.WPUB5
#define BATT_ALARM_SPEAKER_OD                   ODCONBbits.ODCB5
#define BATT_ALARM_SPEAKER_ANS                  ANSELBbits.ANSELB5
#define BATT_ALARM_SPEAKER_SetHigh()            do { LATBbits.LATB5 = 1; } while(0)
#define BATT_ALARM_SPEAKER_SetLow()             do { LATBbits.LATB5 = 0; } while(0)
#define BATT_ALARM_SPEAKER_Toggle()             do { LATBbits.LATB5 = ~LATBbits.LATB5; } while(0)
#define BATT_ALARM_SPEAKER_GetValue()           PORTBbits.RB5
#define BATT_ALARM_SPEAKER_SetDigitalInput()    do { TRISBbits.TRISB5 = 1; } while(0)
#define BATT_ALARM_SPEAKER_SetDigitalOutput()   do { TRISBbits.TRISB5 = 0; } while(0)
#define BATT_ALARM_SPEAKER_SetPullup()          do { WPUBbits.WPUB5 = 1; } while(0)
#define BATT_ALARM_SPEAKER_ResetPullup()        do { WPUBbits.WPUB5 = 0; } while(0)
#define BATT_ALARM_SPEAKER_SetPushPull()        do { ODCONBbits.ODCB5 = 0; } while(0)
#define BATT_ALARM_SPEAKER_SetOpenDrain()       do { ODCONBbits.ODCB5 = 1; } while(0)
#define BATT_ALARM_SPEAKER_SetAnalogMode()      do { ANSELBbits.ANSELB5 = 1; } while(0)
#define BATT_ALARM_SPEAKER_SetDigitalMode()     do { ANSELBbits.ANSELB5 = 0; } while(0)

// get/set RC0 procedures
#define RC0_SetHigh()            do { LATCbits.LATC0 = 1; } while(0)
#define RC0_SetLow()             do { LATCbits.LATC0 = 0; } while(0)
#define RC0_Toggle()             do { LATCbits.LATC0 = ~LATCbits.LATC0; } while(0)
#define RC0_GetValue()              PORTCbits.RC0
#define RC0_SetDigitalInput()    do { TRISCbits.TRISC0 = 1; } while(0)
#define RC0_SetDigitalOutput()   do { TRISCbits.TRISC0 = 0; } while(0)
#define RC0_SetPullup()             do { WPUCbits.WPUC0 = 1; } while(0)
#define RC0_ResetPullup()           do { WPUCbits.WPUC0 = 0; } while(0)
#define RC0_SetAnalogMode()         do { ANSELCbits.ANSELC0 = 1; } while(0)
#define RC0_SetDigitalMode()        do { ANSELCbits.ANSELC0 = 0; } while(0)

// get/set RC1 procedures
#define RC1_SetHigh()            do { LATCbits.LATC1 = 1; } while(0)
#define RC1_SetLow()             do { LATCbits.LATC1 = 0; } while(0)
#define RC1_Toggle()             do { LATCbits.LATC1 = ~LATCbits.LATC1; } while(0)
#define RC1_GetValue()              PORTCbits.RC1
#define RC1_SetDigitalInput()    do { TRISCbits.TRISC1 = 1; } while(0)
#define RC1_SetDigitalOutput()   do { TRISCbits.TRISC1 = 0; } while(0)
#define RC1_SetPullup()             do { WPUCbits.WPUC1 = 1; } while(0)
#define RC1_ResetPullup()           do { WPUCbits.WPUC1 = 0; } while(0)
#define RC1_SetAnalogMode()         do { ANSELCbits.ANSELC1 = 1; } while(0)
#define RC1_SetDigitalMode()        do { ANSELCbits.ANSELC1 = 0; } while(0)

// get/set I2C_SCL aliases
#define I2C_SCL_TRIS                 TRISCbits.TRISC3
#define I2C_SCL_LAT                  LATCbits.LATC3
#define I2C_SCL_PORT                 PORTCbits.RC3
#define I2C_SCL_WPU                  WPUCbits.WPUC3
#define I2C_SCL_OD                   ODCONCbits.ODCC3
#define I2C_SCL_ANS                  ANSELCbits.ANSELC3
#define I2C_SCL_SetHigh()            do { LATCbits.LATC3 = 1; } while(0)
#define I2C_SCL_SetLow()             do { LATCbits.LATC3 = 0; } while(0)
#define I2C_SCL_Toggle()             do { LATCbits.LATC3 = ~LATCbits.LATC3; } while(0)
#define I2C_SCL_GetValue()           PORTCbits.RC3
#define I2C_SCL_SetDigitalInput()    do { TRISCbits.TRISC3 = 1; } while(0)
#define I2C_SCL_SetDigitalOutput()   do { TRISCbits.TRISC3 = 0; } while(0)
#define I2C_SCL_SetPullup()          do { WPUCbits.WPUC3 = 1; } while(0)
#define I2C_SCL_ResetPullup()        do { WPUCbits.WPUC3 = 0; } while(0)
#define I2C_SCL_SetPushPull()        do { ODCONCbits.ODCC3 = 0; } while(0)
#define I2C_SCL_SetOpenDrain()       do { ODCONCbits.ODCC3 = 1; } while(0)
#define I2C_SCL_SetAnalogMode()      do { ANSELCbits.ANSELC3 = 1; } while(0)
#define I2C_SCL_SetDigitalMode()     do { ANSELCbits.ANSELC3 = 0; } while(0)

// get/set I2C_SDA aliases
#define I2C_SDA_TRIS                 TRISCbits.TRISC4
#define I2C_SDA_LAT                  LATCbits.LATC4
#define I2C_SDA_PORT                 PORTCbits.RC4
#define I2C_SDA_WPU                  WPUCbits.WPUC4
#define I2C_SDA_OD                   ODCONCbits.ODCC4
#define I2C_SDA_ANS                  ANSELCbits.ANSELC4
#define I2C_SDA_SetHigh()            do { LATCbits.LATC4 = 1; } while(0)
#define I2C_SDA_SetLow()             do { LATCbits.LATC4 = 0; } while(0)
#define I2C_SDA_Toggle()             do { LATCbits.LATC4 = ~LATCbits.LATC4; } while(0)
#define I2C_SDA_GetValue()           PORTCbits.RC4
#define I2C_SDA_SetDigitalInput()    do { TRISCbits.TRISC4 = 1; } while(0)
#define I2C_SDA_SetDigitalOutput()   do { TRISCbits.TRISC4 = 0; } while(0)
#define I2C_SDA_SetPullup()          do { WPUCbits.WPUC4 = 1; } while(0)
#define I2C_SDA_ResetPullup()        do { WPUCbits.WPUC4 = 0; } while(0)
#define I2C_SDA_SetPushPull()        do { ODCONCbits.ODCC4 = 0; } while(0)
#define I2C_SDA_SetOpenDrain()       do { ODCONCbits.ODCC4 = 1; } while(0)
#define I2C_SDA_SetAnalogMode()      do { ANSELCbits.ANSELC4 = 1; } while(0)
#define I2C_SDA_SetDigitalMode()     do { ANSELCbits.ANSELC4 = 0; } while(0)

// get/set LED_AC_PWR_GREEN aliases
#define LED_AC_PWR_GREEN_TRIS                 TRISDbits.TRISD0
#define LED_AC_PWR_GREEN_LAT                  LATDbits.LATD0
#define LED_AC_PWR_GREEN_PORT                 PORTDbits.RD0
#define LED_AC_PWR_GREEN_WPU                  WPUDbits.WPUD0
#define LED_AC_PWR_GREEN_OD                   ODCONDbits.ODCD0
#define LED_AC_PWR_GREEN_ANS                  ANSELDbits.ANSELD0
#define LED_AC_PWR_GREEN_SetHigh()            do { LATDbits.LATD0 = 1; } while(0)
#define LED_AC_PWR_GREEN_SetLow()             do { LATDbits.LATD0 = 0; } while(0)
#define LED_AC_PWR_GREEN_Toggle()             do { LATDbits.LATD0 = ~LATDbits.LATD0; } while(0)
#define LED_AC_PWR_GREEN_GetValue()           PORTDbits.RD0
#define LED_AC_PWR_GREEN_SetDigitalInput()    do { TRISDbits.TRISD0 = 1; } while(0)
#define LED_AC_PWR_GREEN_SetDigitalOutput()   do { TRISDbits.TRISD0 = 0; } while(0)
#define LED_AC_PWR_GREEN_SetPullup()          do { WPUDbits.WPUD0 = 1; } while(0)
#define LED_AC_PWR_GREEN_ResetPullup()        do { WPUDbits.WPUD0 = 0; } while(0)
#define LED_AC_PWR_GREEN_SetPushPull()        do { ODCONDbits.ODCD0 = 0; } while(0)
#define LED_AC_PWR_GREEN_SetOpenDrain()       do { ODCONDbits.ODCD0 = 1; } while(0)
#define LED_AC_PWR_GREEN_SetAnalogMode()      do { ANSELDbits.ANSELD0 = 1; } while(0)
#define LED_AC_PWR_GREEN_SetDigitalMode()     do { ANSELDbits.ANSELD0 = 0; } while(0)

// get/set LED_AC_PWR_YELLOW aliases
#define LED_AC_PWR_YELLOW_TRIS                 TRISDbits.TRISD1
#define LED_AC_PWR_YELLOW_LAT                  LATDbits.LATD1
#define LED_AC_PWR_YELLOW_PORT                 PORTDbits.RD1
#define LED_AC_PWR_YELLOW_WPU                  WPUDbits.WPUD1
#define LED_AC_PWR_YELLOW_OD                   ODCONDbits.ODCD1
#define LED_AC_PWR_YELLOW_ANS                  ANSELDbits.ANSELD1
#define LED_AC_PWR_YELLOW_SetHigh()            do { LATDbits.LATD1 = 1; } while(0)
#define LED_AC_PWR_YELLOW_SetLow()             do { LATDbits.LATD1 = 0; } while(0)
#define LED_AC_PWR_YELLOW_Toggle()             do { LATDbits.LATD1 = ~LATDbits.LATD1; } while(0)
#define LED_AC_PWR_YELLOW_GetValue()           PORTDbits.RD1
#define LED_AC_PWR_YELLOW_SetDigitalInput()    do { TRISDbits.TRISD1 = 1; } while(0)
#define LED_AC_PWR_YELLOW_SetDigitalOutput()   do { TRISDbits.TRISD1 = 0; } while(0)
#define LED_AC_PWR_YELLOW_SetPullup()          do { WPUDbits.WPUD1 = 1; } while(0)
#define LED_AC_PWR_YELLOW_ResetPullup()        do { WPUDbits.WPUD1 = 0; } while(0)
#define LED_AC_PWR_YELLOW_SetPushPull()        do { ODCONDbits.ODCD1 = 0; } while(0)
#define LED_AC_PWR_YELLOW_SetOpenDrain()       do { ODCONDbits.ODCD1 = 1; } while(0)
#define LED_AC_PWR_YELLOW_SetAnalogMode()      do { ANSELDbits.ANSELD1 = 1; } while(0)
#define LED_AC_PWR_YELLOW_SetDigitalMode()     do { ANSELDbits.ANSELD1 = 0; } while(0)

// get/set LED_BAT_CRG_YELLOW aliases
#define LED_BAT_CRG_YELLOW_TRIS                 TRISDbits.TRISD2
#define LED_BAT_CRG_YELLOW_LAT                  LATDbits.LATD2
#define LED_BAT_CRG_YELLOW_PORT                 PORTDbits.RD2
#define LED_BAT_CRG_YELLOW_WPU                  WPUDbits.WPUD2
#define LED_BAT_CRG_YELLOW_OD                   ODCONDbits.ODCD2
#define LED_BAT_CRG_YELLOW_ANS                  ANSELDbits.ANSELD2
#define LED_BAT_CRG_YELLOW_SetHigh()            do { LATDbits.LATD2 = 1; } while(0)
#define LED_BAT_CRG_YELLOW_SetLow()             do { LATDbits.LATD2 = 0; } while(0)
#define LED_BAT_CRG_YELLOW_Toggle()             do { LATDbits.LATD2 = ~LATDbits.LATD2; } while(0)
#define LED_BAT_CRG_YELLOW_GetValue()           PORTDbits.RD2
#define LED_BAT_CRG_YELLOW_SetDigitalInput()    do { TRISDbits.TRISD2 = 1; } while(0)
#define LED_BAT_CRG_YELLOW_SetDigitalOutput()   do { TRISDbits.TRISD2 = 0; } while(0)
#define LED_BAT_CRG_YELLOW_SetPullup()          do { WPUDbits.WPUD2 = 1; } while(0)
#define LED_BAT_CRG_YELLOW_ResetPullup()        do { WPUDbits.WPUD2 = 0; } while(0)
#define LED_BAT_CRG_YELLOW_SetPushPull()        do { ODCONDbits.ODCD2 = 0; } while(0)
#define LED_BAT_CRG_YELLOW_SetOpenDrain()       do { ODCONDbits.ODCD2 = 1; } while(0)
#define LED_BAT_CRG_YELLOW_SetAnalogMode()      do { ANSELDbits.ANSELD2 = 1; } while(0)
#define LED_BAT_CRG_YELLOW_SetDigitalMode()     do { ANSELDbits.ANSELD2 = 0; } while(0)

// get/set LED_BAT_STAT_GREEN aliases
#define LED_BAT_STAT_GREEN_TRIS                 TRISDbits.TRISD3
#define LED_BAT_STAT_GREEN_LAT                  LATDbits.LATD3
#define LED_BAT_STAT_GREEN_PORT                 PORTDbits.RD3
#define LED_BAT_STAT_GREEN_WPU                  WPUDbits.WPUD3
#define LED_BAT_STAT_GREEN_OD                   ODCONDbits.ODCD3
#define LED_BAT_STAT_GREEN_ANS                  ANSELDbits.ANSELD3
#define LED_BAT_STAT_GREEN_SetHigh()            do { LATDbits.LATD3 = 1; } while(0)
#define LED_BAT_STAT_GREEN_SetLow()             do { LATDbits.LATD3 = 0; } while(0)
#define LED_BAT_STAT_GREEN_Toggle()             do { LATDbits.LATD3 = ~LATDbits.LATD3; } while(0)
#define LED_BAT_STAT_GREEN_GetValue()           PORTDbits.RD3
#define LED_BAT_STAT_GREEN_SetDigitalInput()    do { TRISDbits.TRISD3 = 1; } while(0)
#define LED_BAT_STAT_GREEN_SetDigitalOutput()   do { TRISDbits.TRISD3 = 0; } while(0)
#define LED_BAT_STAT_GREEN_SetPullup()          do { WPUDbits.WPUD3 = 1; } while(0)
#define LED_BAT_STAT_GREEN_ResetPullup()        do { WPUDbits.WPUD3 = 0; } while(0)
#define LED_BAT_STAT_GREEN_SetPushPull()        do { ODCONDbits.ODCD3 = 0; } while(0)
#define LED_BAT_STAT_GREEN_SetOpenDrain()       do { ODCONDbits.ODCD3 = 1; } while(0)
#define LED_BAT_STAT_GREEN_SetAnalogMode()      do { ANSELDbits.ANSELD3 = 1; } while(0)
#define LED_BAT_STAT_GREEN_SetDigitalMode()     do { ANSELDbits.ANSELD3 = 0; } while(0)

// get/set LED_BAT_STAT_RED aliases
#define LED_BAT_STAT_RED_TRIS                 TRISDbits.TRISD4
#define LED_BAT_STAT_RED_LAT                  LATDbits.LATD4
#define LED_BAT_STAT_RED_PORT                 PORTDbits.RD4
#define LED_BAT_STAT_RED_WPU                  WPUDbits.WPUD4
#define LED_BAT_STAT_RED_OD                   ODCONDbits.ODCD4
#define LED_BAT_STAT_RED_ANS                  ANSELDbits.ANSELD4
#define LED_BAT_STAT_RED_SetHigh()            do { LATDbits.LATD4 = 1; } while(0)
#define LED_BAT_STAT_RED_SetLow()             do { LATDbits.LATD4 = 0; } while(0)
#define LED_BAT_STAT_RED_Toggle()             do { LATDbits.LATD4 = ~LATDbits.LATD4; } while(0)
#define LED_BAT_STAT_RED_GetValue()           PORTDbits.RD4
#define LED_BAT_STAT_RED_SetDigitalInput()    do { TRISDbits.TRISD4 = 1; } while(0)
#define LED_BAT_STAT_RED_SetDigitalOutput()   do { TRISDbits.TRISD4 = 0; } while(0)
#define LED_BAT_STAT_RED_SetPullup()          do { WPUDbits.WPUD4 = 1; } while(0)
#define LED_BAT_STAT_RED_ResetPullup()        do { WPUDbits.WPUD4 = 0; } while(0)
#define LED_BAT_STAT_RED_SetPushPull()        do { ODCONDbits.ODCD4 = 0; } while(0)
#define LED_BAT_STAT_RED_SetOpenDrain()       do { ODCONDbits.ODCD4 = 1; } while(0)
#define LED_BAT_STAT_RED_SetAnalogMode()      do { ANSELDbits.ANSELD4 = 1; } while(0)
#define LED_BAT_STAT_RED_SetDigitalMode()     do { ANSELDbits.ANSELD4 = 0; } while(0)

// get/set LED_AC_PWR_RED aliases
#define LED_AC_PWR_RED_TRIS                 TRISDbits.TRISD5
#define LED_AC_PWR_RED_LAT                  LATDbits.LATD5
#define LED_AC_PWR_RED_PORT                 PORTDbits.RD5
#define LED_AC_PWR_RED_WPU                  WPUDbits.WPUD5
#define LED_AC_PWR_RED_OD                   ODCONDbits.ODCD5
#define LED_AC_PWR_RED_ANS                  ANSELDbits.ANSELD5
#define LED_AC_PWR_RED_SetHigh()            do { LATDbits.LATD5 = 1; } while(0)
#define LED_AC_PWR_RED_SetLow()             do { LATDbits.LATD5 = 0; } while(0)
#define LED_AC_PWR_RED_Toggle()             do { LATDbits.LATD5 = ~LATDbits.LATD5; } while(0)
#define LED_AC_PWR_RED_GetValue()           PORTDbits.RD5
#define LED_AC_PWR_RED_SetDigitalInput()    do { TRISDbits.TRISD5 = 1; } while(0)
#define LED_AC_PWR_RED_SetDigitalOutput()   do { TRISDbits.TRISD5 = 0; } while(0)
#define LED_AC_PWR_RED_SetPullup()          do { WPUDbits.WPUD5 = 1; } while(0)
#define LED_AC_PWR_RED_ResetPullup()        do { WPUDbits.WPUD5 = 0; } while(0)
#define LED_AC_PWR_RED_SetPushPull()        do { ODCONDbits.ODCD5 = 0; } while(0)
#define LED_AC_PWR_RED_SetOpenDrain()       do { ODCONDbits.ODCD5 = 1; } while(0)
#define LED_AC_PWR_RED_SetAnalogMode()      do { ANSELDbits.ANSELD5 = 1; } while(0)
#define LED_AC_PWR_RED_SetDigitalMode()     do { ANSELDbits.ANSELD5 = 0; } while(0)

// get/set LED_BAT_CRG_GREEN aliases
#define LED_BAT_CRG_GREEN_TRIS                 TRISDbits.TRISD6
#define LED_BAT_CRG_GREEN_LAT                  LATDbits.LATD6
#define LED_BAT_CRG_GREEN_PORT                 PORTDbits.RD6
#define LED_BAT_CRG_GREEN_WPU                  WPUDbits.WPUD6
#define LED_BAT_CRG_GREEN_OD                   ODCONDbits.ODCD6
#define LED_BAT_CRG_GREEN_ANS                  ANSELDbits.ANSELD6
#define LED_BAT_CRG_GREEN_SetHigh()            do { LATDbits.LATD6 = 1; } while(0)
#define LED_BAT_CRG_GREEN_SetLow()             do { LATDbits.LATD6 = 0; } while(0)
#define LED_BAT_CRG_GREEN_Toggle()             do { LATDbits.LATD6 = ~LATDbits.LATD6; } while(0)
#define LED_BAT_CRG_GREEN_GetValue()           PORTDbits.RD6
#define LED_BAT_CRG_GREEN_SetDigitalInput()    do { TRISDbits.TRISD6 = 1; } while(0)
#define LED_BAT_CRG_GREEN_SetDigitalOutput()   do { TRISDbits.TRISD6 = 0; } while(0)
#define LED_BAT_CRG_GREEN_SetPullup()          do { WPUDbits.WPUD6 = 1; } while(0)
#define LED_BAT_CRG_GREEN_ResetPullup()        do { WPUDbits.WPUD6 = 0; } while(0)
#define LED_BAT_CRG_GREEN_SetPushPull()        do { ODCONDbits.ODCD6 = 0; } while(0)
#define LED_BAT_CRG_GREEN_SetOpenDrain()       do { ODCONDbits.ODCD6 = 1; } while(0)
#define LED_BAT_CRG_GREEN_SetAnalogMode()      do { ANSELDbits.ANSELD6 = 1; } while(0)
#define LED_BAT_CRG_GREEN_SetDigitalMode()     do { ANSELDbits.ANSELD6 = 0; } while(0)

// get/set LED_BAT_CRG_RED aliases
#define LED_BAT_CRG_RED_TRIS                 TRISDbits.TRISD7
#define LED_BAT_CRG_RED_LAT                  LATDbits.LATD7
#define LED_BAT_CRG_RED_PORT                 PORTDbits.RD7
#define LED_BAT_CRG_RED_WPU                  WPUDbits.WPUD7
#define LED_BAT_CRG_RED_OD                   ODCONDbits.ODCD7
#define LED_BAT_CRG_RED_ANS                  ANSELDbits.ANSELD7
#define LED_BAT_CRG_RED_SetHigh()            do { LATDbits.LATD7 = 1; } while(0)
#define LED_BAT_CRG_RED_SetLow()             do { LATDbits.LATD7 = 0; } while(0)
#define LED_BAT_CRG_RED_Toggle()             do { LATDbits.LATD7 = ~LATDbits.LATD7; } while(0)
#define LED_BAT_CRG_RED_GetValue()           PORTDbits.RD7
#define LED_BAT_CRG_RED_SetDigitalInput()    do { TRISDbits.TRISD7 = 1; } while(0)
#define LED_BAT_CRG_RED_SetDigitalOutput()   do { TRISDbits.TRISD7 = 0; } while(0)
#define LED_BAT_CRG_RED_SetPullup()          do { WPUDbits.WPUD7 = 1; } while(0)
#define LED_BAT_CRG_RED_ResetPullup()        do { WPUDbits.WPUD7 = 0; } while(0)
#define LED_BAT_CRG_RED_SetPushPull()        do { ODCONDbits.ODCD7 = 0; } while(0)
#define LED_BAT_CRG_RED_SetOpenDrain()       do { ODCONDbits.ODCD7 = 1; } while(0)
#define LED_BAT_CRG_RED_SetAnalogMode()      do { ANSELDbits.ANSELD7 = 1; } while(0)
#define LED_BAT_CRG_RED_SetDigitalMode()     do { ANSELDbits.ANSELD7 = 0; } while(0)

// get/set IO_SHIPMENT_MODE aliases
#define IO_SHIPMENT_MODE_TRIS                 TRISEbits.TRISE0
#define IO_SHIPMENT_MODE_LAT                  LATEbits.LATE0
#define IO_SHIPMENT_MODE_PORT                 PORTEbits.RE0
#define IO_SHIPMENT_MODE_WPU                  WPUEbits.WPUE0
#define IO_SHIPMENT_MODE_OD                   ODCONEbits.ODCE0
#define IO_SHIPMENT_MODE_ANS                  ANSELEbits.ANSELE0
#define IO_SHIPMENT_MODE_SetHigh()            do { LATEbits.LATE0 = 1; } while(0)
#define IO_SHIPMENT_MODE_SetLow()             do { LATEbits.LATE0 = 0; } while(0)
#define IO_SHIPMENT_MODE_Toggle()             do { LATEbits.LATE0 = ~LATEbits.LATE0; } while(0)
#define IO_SHIPMENT_MODE_GetValue()           PORTEbits.RE0
#define IO_SHIPMENT_MODE_SetDigitalInput()    do { TRISEbits.TRISE0 = 1; } while(0)
#define IO_SHIPMENT_MODE_SetDigitalOutput()   do { TRISEbits.TRISE0 = 0; } while(0)
#define IO_SHIPMENT_MODE_SetPullup()          do { WPUEbits.WPUE0 = 1; } while(0)
#define IO_SHIPMENT_MODE_ResetPullup()        do { WPUEbits.WPUE0 = 0; } while(0)
#define IO_SHIPMENT_MODE_SetPushPull()        do { ODCONEbits.ODCE0 = 0; } while(0)
#define IO_SHIPMENT_MODE_SetOpenDrain()       do { ODCONEbits.ODCE0 = 1; } while(0)
#define IO_SHIPMENT_MODE_SetAnalogMode()      do { ANSELEbits.ANSELE0 = 1; } while(0)
#define IO_SHIPMENT_MODE_SetDigitalMode()     do { ANSELEbits.ANSELE0 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/