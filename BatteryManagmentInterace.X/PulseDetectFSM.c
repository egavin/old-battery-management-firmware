/****************************************************************************
 ** Copyright (c) 2021 Supira Medical
 ** Author(s): rmerchant, Ed Gavin
 ** Purpose: Module detecting battery charger GPIO lines for charger status
 ****************************************************************************/

#include "sm_queue.h"
#include "sm_timer.h"
#include "sm_port.h"
#include "bitdefs.h"
#include "PulseDetectFSM.h"

/*----------------------------- Module Defines ----------------------------*/
#define TIME_OUT 2500 // 2.5s timeout period

// Here you define the port and pins on the port you want to use.
#define PIN_PORT PORTA
#define PIN_PORT_M 0b00111111 // the pins to use
#define PIN_ANSEL ANSELA // Analog control for the port
#define PIN_TRIS TRISA // IO control for the port

// Here you define the shifting and incrementing you want
#define BASE_PIN_S 0 // start at RA0
#define PIN_INC_S 1 // read every pin

/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/
static void PD_HWInit(void);
static void UpdatePinVals(uint8_t byte);

// Variable t hold the current state of the machine 
static PulseState_t currentState[PD_NUM_PINS];
// Array to store the initial pin states
static uint8_t pinVals[PD_NUM_PINS];
// Value of the port to use for event checking
static uint8_t lastPinPortVal;
// Array to hold the blinking periods
static uint16_t periods[PD_NUM_PINS];
// variables for timing pulses:
static uint16_t timeLastFall[PD_NUM_PINS];
static uint16_t timeLastRise[PD_NUM_PINS];
static queue_t* pQueue = 0;


/**
 * InitPulseDetectFSM
 * sets up the initial transition and does any
   other required initialization for this state machine
 * @param pq
 * @return  bool, false if error in initialization, true otherwise
 */
bool InitPulseDetectFSM(queue_t* pq)
{
  event_t ThisEvent;
  bool initFlag = true;
  pQueue = pq;   
  // initialize machine hardware
  PD_HWInit();
  // post the initial transition event
  ThisEvent.EventType = SM_INIT;
  // put us into the Initial PseudoState
  uint8_t i = 0;
  for(i =0; i< PD_NUM_PINS; i++) 
  {
    // set all to the init pseudo state for pin reading
    currentState[i] = PD_InitPState;
    // set the parameter to distribute the event
    ThisEvent.data = i;
    // post the event and AND the returns
    initFlag &= EnqueueFIFO(pQueue, ThisEvent);
  }
  // return the initFlag
  return initFlag;
}

/**
 * @param ThisEvent
 * @return true if the event is queued, false otherwise
 */
bool PostPulseDetectFSM(event_t ThisEvent)
{
  return EnqueueFIFO(pQueue, ThisEvent);
}

/** 
 * @param ThisEvent event to process
 * @return ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise
 */
event_t RunPulseDetectFSM(event_t ThisEvent)
{
  event_t ReturnEvent;
  ReturnEvent.EventType = SM_NO_EVENT; // assume no errors
  // declare variable to know what machine in the ortho stack we want
  static uint8_t mi; 
  // Assign the machine index from the event parameter. 
  mi = (uint8_t)(ThisEvent.data);
  
  switch (currentState[mi])
  {
    case PD_InitPState:        // If current state is initial Pseudo State
    {
      if (ThisEvent.EventType == SM_INIT)    // only respond to ES_Init
      {
        if(pinVals[mi]){
            currentState[mi] = ConstHi;
        } else 
        {
            currentState[mi] = ConstLo;
        }
      }
    }
    break;
    /*---------------------------------------------------------------- ConstHi*/
    case ConstHi:        // If current state is a high line
    {
      if(ThisEvent.EventType == PD_F_EDGE) // if there was a falling edge
      {
        //start timer
        timer_InitTimer(mi, TIME_OUT);
        // log time of last fall
        timeLastFall[mi] = getSysTickCounter(); 
        // set state to Blinking
        currentState[mi] = Blinking;
      }        
    }
    break;
    /*---------------------------------------------------------------- ConstLo*/
    case ConstLo:        // If current state is a low line
    {
      if(ThisEvent.EventType == PD_R_EDGE) // if there was a falling edge
      {
        //start timer
        timer_InitTimer(mi, TIME_OUT);
        // log time of last rise
        timeLastRise[mi] = getSysTickCounter();
        // set state to Blinking
        currentState[mi] = Blinking;
      }
        
    }
    break;
    /*--------------------------------------------------------------- Blinking*/
    case Blinking:        // If current state is a blinking line
    {
      switch(ThisEvent.EventType) // check the events coming in
      {
        case PD_R_EDGE: // If there was a falling edge
        {
          //update period time
          uint16_t currRiseTime = getSysTickCounter();
          periods[mi] = (currRiseTime - timeLastFall[mi]) << 1;
          // update time last rise
          timeLastRise[mi] = currRiseTime;
          // restart timer
          timer_InitTimer(mi, TIME_OUT);
        }
        break;
        
        case PD_F_EDGE: // if there was a rising edge
        {
          //update period time
          uint16_t currFallTime = getSysTickCounter();
          periods[mi] = (currFallTime - timeLastRise[mi]) << 1;
          // update time last rise
          timeLastFall[mi] = currFallTime;
          // restart timer
          timer_InitTimer(mi, TIME_OUT);
        }
        break;
        
        case SM_TIMEOUT: // if there as a timeout
        {
          // set period to 0 to indicate no valid period
          periods[mi] = 0;
          if(pinVals[mi]) currentState[mi] = ConstHi;
          else currentState[mi] = ConstLo;
        }
        break;
        
        default:
        {} // do nothing for other events
        break;
      }   
    }
    break;
    /*---------------------------------------------------------- END OF STATES*/
    // repeat state pattern as required for other states
    default:
      ;
  }                                   // end switch on Current State
  return ReturnEvent;
}


/**
 * Populates input array with the current states of all orthogonal
 * machines present. Input array size should machine total number
 * of machines.  
 * @param pInfo pointer to array
 */
void QueryPulseDetectFSM(PinInfo_t pInfo[])
{
  uint8_t i = 0;
  //start loop to populate members
  for(i = 0; i < PD_NUM_PINS; i++)
  {
    // populate the state to member
    pInfo[i].state = currentState[i];
    // Populate value to member
    pInfo[i].value = pinVals[i];
    // Populate period to member
    pInfo[i].period = periods[i];
  }
}

/****************************************************************************
 Event Checkers
 ***************************************************************************/

/**
 * Checks for changes on all pins used by this state machine. Returns 
 * true if event occurs on any one of the pins.
 * @return 
 */
bool Check4PinChanges(void)
{
  static event_t PinEvent; //static for speed
  bool returnVal = false; // the value to return
  // get pins vals
  uint8_t currPinPortVal = (PIN_PORT & PIN_PORT_M) >> BASE_PIN_S;
  uint8_t currCopy = currPinPortVal;
  // Start loop to compare bits  
  for(uint8_t i = 0; i < PD_NUM_PINS; i++)
  {
    // compare LSBs
    if((currPinPortVal & BIT0HI) != (lastPinPortVal & BIT0HI))
    {
      // if the pin was high before, we have a falling edge
      if(lastPinPortVal & BIT0HI)
      {
        // Post a falling edge event
        PinEvent.EventType = PD_F_EDGE;
        PinEvent.data = i;
        PostPulseDetectFSM(PinEvent);
      }
      // if pin was low before, we have a rising edge
      else
      {
        // Post a rising edge event
        PinEvent.EventType = PD_R_EDGE;
        PinEvent.data = i;
        PostPulseDetectFSM(PinEvent);
      }
      returnVal = true;
    }
    //Shift
    lastPinPortVal = lastPinPortVal >> PIN_INC_S;
    currPinPortVal = currPinPortVal >> PIN_INC_S;
    
  }
  // update
  lastPinPortVal = currCopy;
  UpdatePinVals(lastPinPortVal);
  return returnVal; 
}

/***************************************************************************
 private functions
 ***************************************************************************/

/**
 * Sets up the pins as digital inputs. Update for pin usage
 */
static void PD_HWInit(void)
{
  // set up the pins as digital inputs
  PIN_ANSEL &= ~PIN_PORT_M;
  PIN_TRIS |= PIN_PORT_M;
  // read the values at initialization
  // perform the base shift
  uint8_t lastPinPortVal = (PIN_PORT & PIN_PORT_M) >> BASE_PIN_S;
  UpdatePinVals(lastPinPortVal);
  return;
}

/**
 * Parses out the bits and stores them in the PinVals array
 * @param byte input byte from reading port
 */
static void UpdatePinVals(uint8_t byte)
{
  // start a loop to look at individual bits
  uint8_t i = 0;
  for(i=0; i<PD_NUM_PINS; i++)
  {
    // look at the LSB
    pinVals[i] = byte & BIT0HI;
    // shift right 1
    byte = byte >> PIN_INC_S;
  }
  return;
}
