/****************************************************************************
 ** Copyright (c) 2021 Supira Medical
 ** Author(s): Ed Gavin
 ** Purpose: Purpose: functions related to state machine events
 ****************************************************************************/

#ifndef MasterFSM_H
#define MasterFSM_H

#include <stdbool.h>
#include "sm_events.h"
#include "sm_queue.h"

// masks for GPIO status
// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  Master_InitPState,  // Pseudo state
  CycleLEDS_Start,    // // Cycle the LED's at startup             
  CycleLEDS_PlugInd_Green, 
  CycleLEDS_BatteryCapacity_Yellow,                        
  CycleLEDS_BatteryCapacity_Red,
  CycleLEDS_BatteryFault_Yellow,        
  CycleLEDS_BatteryFault_Red,                
  CycleLEDS_Complete,        
  Listening2Bus,      // Just idling listening for alarms
  ReadingRegisters,   // Reading registers for monitoring purposes
  WritingRegisters,   // writing registers for shipment mode          
}MasterState_t;

typedef enum
{
    state_NoAlarm = 0,
    state_AlarmDelay,
    state_DisplayAlarm,            
}AlarmState_t;

#define ALARM_DELAY 15

// Public Function Prototypes
bool InitMasterFSM(queue_t* pq);
bool PostMasterFSM(event_t ThisEvent);
event_t RunMasterFSM(event_t ThisEvent);
MasterState_t QueryMasterFSM(void);

#endif /* MasterFSM_H */

