/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Author(s): Ed Gavin
** Purpose:  code for timer service
****************************************************************************/

#include <xc.h>
#include <stdbool.h>
#include "sm_main.h"
#include "sm_timer.h"
#include "sm_queue.h"
#include "sm_events.h"
#include <stdio.h>
#include <string.h>

#ifdef DEBUG_PRINT_ENABLED  
#include "dbprintf.h"
#endif

static timer_struct_t timer_array[NUM_TIMERS];

/**
 * Initializes the timer array
 */
void timer_initialize()
{
    memset(timer_array, 0, NUM_TIMERS * sizeof(timer_array[0]));    
}

/**
 * @param index to timer array
 * @param pPostFunction
 */
void timer_SetPostFunction(uint8_t index, event_t (*pPostFunction)(event_t event))
{        
    if(index <= NUM_TIMERS){
        timer_array[index].postFunction = pPostFunction;
    }    
}

/**
 * Initializes the timer and starts it.
 * @param index
 * @param newTime
 * @return TIMER_OK for success, otherwise TIMER_ERROR
 */
timer_status_t timer_InitTimer(uint8_t index, uint16_t newTime)
{
    timer_status_t retStatus = TIMER_OK;
    if(index >= NUM_TIMERS || timer_array[index].postFunction == 0){
        retStatus =  TIMER_ERROR;
    }
    if(retStatus == TIMER_OK){
        timer_array[index].m_time = newTime;
        timer_array[index].m_flags |= TMR_ACTIVE;
    }    
    return retStatus;
}

/**
 * Description stops the timer at the index.
 * @param index of timer in timer array
 * @return 
 */
timer_status_t timer_StopTimer(uint8_t index)
{
    timer_status_t retStatus = TIMER_OK;
    if(index >= NUM_TIMERS ){
        retStatus = TIMER_ERROR;
    }
    if(retStatus == TIMER_OK){
        timer_array[index].m_flags &= ~TMR_ACTIVE;
    }
    return retStatus;
}

/**
 * 
 * @param index
 * @return timer time for success, 0 for timer index error
 */
uint16_t timer_GetTime(uint8_t index)
{    
    uint16_t retValue = 0;
    if(index >= NUM_TIMERS ){
        return 0;
    } else {
        retValue = timer_array[index].m_time;
    }
    return retValue;
}        

/**
 * Description: Called once per millisecond, this function decrements the 
 * delay time in active timers, and if the time reaches zero, calls the 
 * post function with the SM_TIMEOUT event.
 */
void timer_ProcessTicks()
{
    event_t event;
    // loop thru the timers
    for(uint8_t i = 0; i < NUM_TIMERS; ++i){
        if(timer_array[i].postFunction && timer_array[i].m_flags & TMR_ACTIVE){
            if(--timer_array[i].m_time == 0){
                // stop counting
                timer_StopTimer(i);
                event.EventType = SM_TIMEOUT;
                event.data = i;                
                timer_array[i].postFunction(event);                
            }
        }
    }
}

#ifdef SM_TIMER_DEBUG
uint8_t GetNumActiveTimers()
{
    uint8_t ActiveTimers = 0;
    for(uint8_t i = 0; i < NUM_TIMERS; ++i){
        if(timer_array[i].postFunction && timer_array[i].m_flags & TMR_ACTIVE){
            ++ActiveTimers;
        }
    }
    return ActiveTimers;
}

void LogActiveTimers()
{
    static char* timerNames[] = {
        "PULSE_DET_0", "PULSE_DET_1", "PULSE_DET_2", "PULSE_DET_3", 
        "PULSE_DET_4", "PULSE_DET_5", "IND_0_TMR",  "IND_1_TMR", "IND_2_TMR",
        "SHIP_MODE", "SM_WATCHDOG", "UNUSED_2", "REG_READ_TIMER", 
         "SMBUS_TIMER", "MONITOR_TIMER", "TEST_TIMER"};
        
    
    for(uint8_t i = 0; i < NUM_TIMERS; ++i){        
        DBG_printf("Timer:%s  active: %u \r\n", timerNames[i],  (timer_array[i].m_flags & TMR_ACTIVE));  
    }
}
#endif