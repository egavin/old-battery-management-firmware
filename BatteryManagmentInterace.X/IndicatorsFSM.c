/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Authors: rmerchant, Ed Gavin
** Purpose: Controls the indicator LED's 
****************************************************************************/
#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include <stdint.h>
#include <stdbool.h>
#include "sm_port.h"
#include "sm_main.h"
#include "sm_events.h"
#include "sm_queue.h"
#include "sm_timer.h"
#include "IndicatorsFSM.h"
#ifdef INDICATORS_DEBUG                
#include "dbprintf.h"
#endif

#define FAST_TIME 250  // 2Hz
#define EVENT_OFFSET LED_GRN_C // the first of the indicator events

void SetGreen(uint8_t ind);
void SetYellow(uint8_t ind);
void SetRed(uint8_t ind);
void SetOff(uint8_t ind);
void ToggleRed(uint8_t);
void ToggleYellow(uint8_t);

// pointer to this state machines queue
static queue_t* pQueue;
static IndicatorState_t currentIndicatorState[NUM_INDS];

/** 
 * @param indicator
 * @return the current state
 */
uint8_t GetCurrentIndicatorState(uint8_t ind) {
    return currentIndicatorState[ind];
}

/**
 * Turns all indicators off.  
 */
void SetAllLEDsOff() {
    event_t ThisEvent;
    ThisEvent.EventType = LED_OFF;
    ThisEvent.data = PLUG_IND;
    PostIndicatorFSM(ThisEvent);

    ThisEvent.data = CHRG_IND;
    PostIndicatorFSM(ThisEvent);

    ThisEvent.data = STAT_IND;
    PostIndicatorFSM(ThisEvent);
}

/**
 * Formats the return value to match the communications
 * protocol spec Color is set in bit 0 and 1, blink rate 
 * in 10's of Hz is set in bits 7:4
 * @param ind
 * @return PC status value for indicator.
 */
uint8_t GetStatusIndicatorForPC(uint8_t ind) {
    uint8_t value;
    switch (currentIndicatorState[ind]) {
        case Off:
            value = 0x00;
            break;

        case SolidGrn:
            value = 0x01;
            break;

        case SolidYlw:
            value = 0x02;
            break;

        case SolidRed:
            value = 0x03;
            break;

        case FastRed:
            value = 0x50 | 0x03;
            break;

        case FastYlw:
            value = 0x50 | 0x02;
            break;

        default:
            value = 0xff;
    }
    return value;
}

/**
 * @function InitIndicatorFSM
 * @param pq pointer to the event queue for this FSM
 * @return true if the initial event is queued, false otherwise.
 */
bool InitIndicatorFSM(queue_t* pq) {
    event_t ThisEvent;
    pQueue = pq;

    //  Indicator_HWInit();
    // all indicators start off
    uint8_t i = 0;
    for (i = 0; i < NUM_INDS; i++) {
        currentIndicatorState[i] = Off;
    }
    // post the initial transition event
    ThisEvent.EventType = SM_INIT;
    return EnqueueFIFO(pQueue, ThisEvent);
}

/**
 * @function PostIndicatorFSM
 * @param ThisEvent
 * @return 
 * @note called from MasterFSM.
 */
bool PostIndicatorFSM(event_t ThisEvent) {
    return EnqueueFIFO(pQueue, ThisEvent);
}

/**
 * @function RunIndicatorFSM
 * @param ThisEvent
 * @return event_t return event
 * @note uses nested switch/case to implement the machine.
 */
event_t RunIndicatorFSM(event_t ThisEvent) {
    // setup an array of return events from the run functions
    event_t ReturnEvent = {SM_NO_EVENT, 0};
    uint8_t ind;

    // going to pass timeout events a little differently
    if (ThisEvent.EventType == SM_TIMEOUT) {
        // since we are not starting at timer0 of the framework, need to 
        // subtract the offset from the parameter
        ThisEvent.data -= IND_0_TMR;
    }

    ind = (uint8_t) ThisEvent.data;

    switch (ThisEvent.EventType) {
        case LED_GRN_C:
            if (currentIndicatorState[ind] != SolidGrn) {
                currentIndicatorState[ind] = SolidGrn;
                SetGreen(ind);
            }
            break;

        case LED_YLW_C:
            if (currentIndicatorState[ind] != SolidYlw) {
                currentIndicatorState[ind] = SolidYlw;
                SetYellow(ind);
            }
            break;

        case LED_RED_C:
            if (currentIndicatorState[ind] != SolidRed) {
                currentIndicatorState[ind] = SolidRed;
                SetRed(ind);
            }
            break;

        case LED_YLW_F:
            if (currentIndicatorState[ind] != FastYlw) {
                currentIndicatorState[ind] = FastYlw;
                SetYellow(ind);
                // start the timer for yellow flashing
                timer_InitTimer(IND_0_TMR + ind, FAST_TIME);
            }
            break;

        case LED_RED_F:
            if (currentIndicatorState[ind] != FastRed) {
                currentIndicatorState[ind] = FastRed;
                SetRed(ind);
                // start the timer for fast red               
                timer_InitTimer(IND_0_TMR + ind, FAST_TIME);
            }
            break;

        case SM_TIMEOUT:
            if (currentIndicatorState[ind] == FastRed) {
                ToggleRed(ind);
                timer_InitTimer(IND_0_TMR + ind, FAST_TIME);
            } else if (currentIndicatorState[ind] == FastYlw) {
                ToggleYellow(ind);
                timer_InitTimer(IND_0_TMR + ind, FAST_TIME);
            } else {
                SetOff(ind);
            }
            break;

        case LED_OFF:
            if (currentIndicatorState[ind] != Off) {
                currentIndicatorState[ind] = Off;
                SetOff(ind);
            }
        default:
            break;
    };

    // if the state of the charge or the status indicator is flashing Red,
    // turn on the speaker. Otherwise, turn it off.
    if (currentIndicatorState[CHRG_IND] == FastRed ||
            currentIndicatorState[STAT_IND] == FastRed) {
        BATT_ALARM_SPEAKER_SetHigh();
    } else {
        BATT_ALARM_SPEAKER_SetLow();
    }

    return ReturnEvent;
}

/**
 *  Sets an indicator LED to greem. 
 * @param ind Indicator ((0:2) 
 * Notes: ind corresponds with PLUG_IND, CHRG_IND, STAT_IND to select which
 *  LED is being set.
 */
void SetGreen(uint8_t ind) {
    switch (ind) {
        case PLUG_IND:
            LED_AC_PWR_GREEN_SetHigh();
            LED_AC_PWR_YELLOW_SetLow();
            LED_AC_PWR_RED_SetLow();
            break;

        case CHRG_IND:
            LED_BAT_CRG_GREEN_SetHigh();
            LED_BAT_CRG_RED_SetLow();
            LED_BAT_CRG_YELLOW_SetLow();
            break;

        case STAT_IND:
            LED_BAT_STAT_GREEN_SetHigh();
            LED_BAT_STAT_YELLOW_SetLow();
            LED_BAT_STAT_RED_SetLow();
            break;
    }
}

/**
 * Set an indicator LED to solid yellow
 * @param ind the LED being set
 */
void SetYellow(uint8_t ind) {
    switch (ind) {
        case PLUG_IND:
            LED_AC_PWR_GREEN_SetLow();
            LED_AC_PWR_YELLOW_SetHigh();
            LED_AC_PWR_RED_SetLow();
            break;

        case CHRG_IND:
            LED_BAT_CRG_GREEN_SetLow();
            LED_BAT_CRG_YELLOW_SetHigh();
            LED_BAT_CRG_RED_SetLow();
            break;

        case STAT_IND:
            LED_BAT_STAT_GREEN_SetLow();
            LED_BAT_STAT_YELLOW_SetHigh();
            LED_BAT_STAT_RED_SetLow();
            break;
    }
}

/**
 * Sets the LED's for an indicator LED solid read
 * @param ind which LED is being set
 */
void SetRed(uint8_t ind) {
    switch (ind) {
        case PLUG_IND:
            LED_AC_PWR_GREEN_SetLow();
            LED_AC_PWR_YELLOW_SetLow();
            LED_AC_PWR_RED_SetHigh();
            break;

        case CHRG_IND:
            LED_BAT_CRG_GREEN_SetLow();
            LED_BAT_CRG_YELLOW_SetLow();
            LED_BAT_CRG_RED_SetHigh();
            break;

        case STAT_IND:
            LED_BAT_STAT_GREEN_SetLow();
            LED_BAT_STAT_YELLOW_SetLow();
            LED_BAT_STAT_RED_SetHigh();
            break;
    }
}

/**
 * Toggles the Yellow indicator LED
 * @param ind Led to toggle
 */
void ToggleYellow(uint8_t ind) {
    switch (ind) {
        case PLUG_IND:
            LED_AC_PWR_GREEN_SetLow();
            LED_AC_PWR_YELLOW_Toggle();
            LED_AC_PWR_RED_SetLow();
            break;

        case CHRG_IND:
            LED_BAT_CRG_GREEN_SetLow();
            LED_BAT_CRG_YELLOW_Toggle();
            LED_BAT_CRG_RED_SetLow();
            break;

        case STAT_IND:
            LED_BAT_STAT_GREEN_SetLow();
            LED_BAT_STAT_YELLOW_Toggle();
            LED_BAT_STAT_RED_SetLow();
            break;
    }
}

/**
 * Toggles an indicator LED red
 * @param ind Led to toggle
 */
void ToggleRed(uint8_t ind) {
    switch (ind) {
        case PLUG_IND:
            LED_AC_PWR_GREEN_SetLow();
            LED_AC_PWR_YELLOW_SetLow();
            LED_AC_PWR_RED_Toggle();
            break;

        case CHRG_IND:
            LED_BAT_CRG_GREEN_SetLow();
            LED_BAT_CRG_YELLOW_SetLow();
            LED_BAT_CRG_RED_Toggle();
            break;

        case STAT_IND:
            LED_BAT_STAT_GREEN_SetLow();
            LED_BAT_STAT_YELLOW_SetLow();
            LED_BAT_STAT_RED_Toggle();
            break;
    }
}

/**
 * Turns off an indicator LED's 
 * @param ind indicator LED to turn off
 */
void SetOff(uint8_t ind) {
    switch (ind) {
        case PLUG_IND:
            LED_AC_PWR_GREEN_SetLow();
            LED_AC_PWR_YELLOW_SetLow();
            LED_AC_PWR_RED_SetLow();
            break;

        case CHRG_IND:
            LED_BAT_CRG_GREEN_SetLow();
            LED_BAT_CRG_YELLOW_SetLow();
            LED_BAT_CRG_RED_SetLow();
            break;

        case STAT_IND:
            LED_BAT_STAT_GREEN_SetLow();
            LED_BAT_STAT_YELLOW_SetLow();
            LED_BAT_STAT_RED_SetLow();
            break;
    }
}





