/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Author(s): Ed Gavin
** Purpose:  functions for event queues
****************************************************************************/
#include "sm_queue.h"
#include "sm_events.h"
#include <stdbool.h>
#include <stdio.h>
#include <assert.h>

/**
 * function: initQueue
 * @param pQueue pointer to block of memory for Queue
 * @return queue size
 */
uint8_t initQueue(queue_t* pQueue )
{
    pQueue->info.currentIndex = 0;
    pQueue->info.numEntries = 0;
    pQueue->info.queueSize = MAX_EVENTS;
    return pQueue->info.queueSize;
}

/**  
 * 
 * @param pQueue Pointer to valid and initialized Queue
 * @param newEvent Event to add to Queue
 * @return bool: true if success, false otherwise.
 */
bool PostToQueue(queue_t* pQueue, event_t newEvent)
{
    bool r = EnqueueFIFO(pQueue, newEvent); 
    assert(r == true);
    return r;
}

/**
 * 
 * @param pQueue pointer to an initialized Queue
 * @param newEvent 
 * @return true if event is queued, false otherwise
 */
bool EnqueueFIFO(queue_t* pQueue, event_t newEvent)
{
    bool retValue = true;
    if(pQueue->info.numEntries > pQueue->info.queueSize){
        retValue = false;
    } else {
        uint8_t addAtIndex = (pQueue->info.currentIndex + pQueue->info.numEntries) % pQueue->info.queueSize;
        pQueue->info.numEntries++;    
        pQueue->events[addAtIndex].EventType = newEvent.EventType;
        pQueue->events[addAtIndex].data = newEvent.data;        
    }
    return retValue;    
}

/***
 * function: Dequeue
 * arguments: queue_t* pointer to a queue_t block of memory to hold the queue.
 *          event_t* event location event will be copied to.
 * returns: Number of entries in the queue.
 * notes: An empty event will be returned from an empty queue
 */
uint8_t Dequeue( queue_t* pQueue, event_t *pEvent)
{
    if(pQueue->info.numEntries > 0){
        pEvent->EventType = pQueue->events[pQueue->info.currentIndex].EventType;
        pEvent->data = pQueue->events[pQueue->info.currentIndex].data;            
        
        // can remove after testing, but set dequeued spot to 128.
        //pQueue->events[pQueue->info.currentIndex].EventType = 128;
        //pEvent->data = pQueue->events[pQueue->info.currentIndex].data = 128;            
        
        pQueue->info.currentIndex++;
    
        if(pQueue->info.currentIndex >= pQueue->info.queueSize){
            pQueue->info.currentIndex = pQueue->info.currentIndex % pQueue->info.queueSize;
        }        
        pQueue->info.numEntries--;     
        
    } else {
        // no entries - return a 
        pEvent->EventType = SM_NO_EVENT;
        pEvent->data = 0;    
    }
    return pQueue->info.numEntries;
}

/**
 * Description: Check if Queue is empty
 * @param pQueue
 * @return 
 */
bool isQueueEmpty(queue_t* pQueue)
{ 
    return pQueue->info.numEntries == 0;
}


#ifdef TEST_QUEUE

static queue_t TestQueue;

queue_t* pQueue = &TestQueue;
bool SM_QueueTest()
{
    event_t testEvent;
    bool b_return;
    uint8_t eventIndex = 0;
    
    printf("\n***** Start of Queue FIFO test ******\n");
    
    initQueue(pQueue);
    
    for( uint8_t i = 0; i < MAX_EVENTS; ++i){
        testEvent.data = (uint16_t)(eventIndex * 10);
        testEvent.EventType = eventIndex++;        
        EnqueueFIFO(pQueue, testEvent);
    }
        
    printQueue(pQueue);    
    
    // remove 2 event from the queue
    for(uint8_t i = 0; i < 2; ++i){
        Dequeue(pQueue, &testEvent);
        printf("Dequeue event: %d\n", testEvent.EventType);
    }
    printf("***** Removed 2 events ******\n");
    printQueue(pQueue);
    
    // add 2 more events
    for(uint8_t i = 0; i < 2; ++i){
        testEvent.data = (uint16_t)(eventIndex * 10);
        testEvent.EventType = eventIndex++;        
        EnqueueFIFO(pQueue, testEvent);        
    }
    printf("***** Added 2 events ******\n");
    printQueue(pQueue);
    
    // remove 4 events from the queue
    for(uint8_t i = 0; i < 4; ++i){
        Dequeue(pQueue, &testEvent);
        printf("Dequeue event: %d\n", testEvent.EventType);
    }
    printf("***** Removed 4 events ******\n");
    printQueue(pQueue);
    
    return true;
}


void printQueue(queue_t* pQueue)
{
    event_t testEvent;
    testEvent.EventType = 1;
    testEvent.data = 10;
    
    // print queue_info
    printf("***** print Queue Info ******\n");
    printf("Queue Size: %d, currentIndex: %d, numEntries: %d\n",(int) pQueue->info.queueSize, (int)pQueue->info.currentIndex, (int)pQueue->info.numEntries);
    
    printf("***** print Queue Contents******\n");
    for(int i = 0; i < pQueue->info.queueSize; ++i){                
        printf("Array Index: %d Event: %d data: %d\n", i, (int)pQueue->events[i].EventType, (int)pQueue->events[i].data); 
    }
}

#endif // TEST_QUEUE