/*****************************************************************************
 * MasterFSM.c
 *      Copyright (c) 2021 Rose Garden Software Inc.  All rights reserved.
 *      Reproduction of transmission in whole or in part, in
 *      any form or by any means, electronic, mechanical, or otherwise, is
 *      prohibited without the prior written consent of the copyright owner.
 * Purpose: functions related to state machine events
 *
 *  Authors: R. Merchant, Ed Gavin
 *****************************f***********************************************/

#include "mcc_generated_files/pin_manager.h"
#include <stdio.h>
#include <string.h>
#include "main.h"
#include "sm_main.h"
#include "sm_queue.h"
#include "sm_timer.h"
#include "MasterFSM.h"
#include "SMBusMaster.h"
#include "PulseDetectFSM.h"
#include "ReadRegsFSM.h"
#include "IndicatorsFSM.h"
#include "BattStat.h"
#include "CompComm.h"
#include"sm_port.h"
#include"sm_queue.h"
#include "BattRegDefs.h"
#include <assert.h>

#if  defined(MASTER_FSM_DEBUG) || defined(INDICATOR_DEBUG) || (LOG_LED_STATE)
#define DEBUG_PRINT_ENABLED 1
#include "dbprintf.h"
#endif

// Defines, structs and enums
#define BROADCAST 0x00
#if 0

typedef enum // list of the possible ways the pins are behaving
{
    PIN_LOW = 0, PIN_HIGH, BLINK_400, BLINK_2000
} PinInterp_t;
#endif

#define REQUEST_M 0x0f
#define MONITOR_TIME 500
#define NUM_POLL_REGS 15    
#define NUM_WRITE_REGS 2 // actually, register 0 twice for shipping mode
#define SHIPMENT_CMD 0x0010

// Run Time to Empty limits
#define RTE_LIMIT_TWENTY 20
#define RTE_LIMIT_TEN    10



// Broadcast message
#define SIZE_BROADCAST 0x20
#define BC_ARRAY_SIZE SIZE_BROADCAST + 20

// Important indices needed for status updates. Temp and status must always be
// included in a query.

typedef enum {
    tempertuare_index = 0,
    voltage_index,
    current_index,
    average_current_index,
    max_error_index,
    relative_charge_index,
    absolute_charge_index,
    remaining_capacity_index,
    full_charge_capacity_index,
    run_time_too_empty_index,
    average_time_to_empty_index,
    status_index,
    cycle_count_index,
    manufacturing_date_index,
    serial_number_index
} battery_register_indexes;

#ifdef STATS

typedef struct timeStats {
    uint16_t n;
    uint32_t sum;
    uint16_t min;
    uint16_t max;
    uint16_t startTic;
} timeStats_t;

void initializeStats(timeStats_t* stats);
uint16_t average(timeStats_t* stats);
void startStatsTiming(timeStats_t *stats);
void finishStatsTiming(timeStats_t *stats);

// for tracking time in read registers
static timeStats_t rrStats;
#endif

#ifdef MASTER_FSM_DEBUG
#define PRINT_DEBUG
#define DEBUG
#endif

// local function prototypes
static void SendBroadCast(uint16_t *pRegData);
static void ProcessPinState(PinInterp_t* pData);
static void SetStatIndicator(BattError_t BatteryError);
static void logLEDState(void);
void UpdateGPIOValues(void);
static BattError_t BatteryError[NUM_BATTS];



// variable to know what battery is currently being monitored
static uint8_t battNumMon;
//  variable to store the request the PC sent
static uint8_t rawReqByte; //the whole raw byte
static uint8_t registersRead = 0; // used at startup to inhibit status errors
static bool softRestart = false;
#define READ_REGISTERS_TWENTY 20  // num registers to read beforew updating LED's

static const uint8_t pollRegs[NUM_POLL_REGS]
        = {TEMP_REG, VOLTAGE, CURRENT, AVG_CURRENT, MAX_ERROR, REL_CHRG_REG,
    ABS_CHRG_REG, REM_CAP, FULL_CHRG_CAP, RUN_TIME_2E, AVG_TIME_2E, STATUS_REG,
    CYCLE_CNT, MANU_DATE, SERIAL_NUM};

// we write twice to register 0 to set shipment mode
static const uint8_t shipmentRegisters[NUM_WRITE_REGS]
        = {MANU_ACCESS, MANU_ACCESS};

static const uint8_t shipmentCmds[NUM_WRITE_REGS * 2]
        = {LOW_BYTE(SHIPMENT_CMD), HIGH_BYTE(SHIPMENT_CMD), LOW_BYTE(SHIPMENT_CMD), HIGH_BYTE(SHIPMENT_CMD)};

static uint8_t CycleLEDIndex;
static uint8_t CycleLEDIndicator;
// constants to hold the GPIO pins indices. Makes run time easier
static const uint8_t gpioIndices[3][2] = {
    {GPIO1A, GPIO2A},
    {GPIO1B, GPIO2B},
    {GPIO1C, GPIO2C}
};
// type of state variable should match that of enum in header file
static MasterState_t currentState;

// queue for this FSM
static queue_t* pQueue;

/**
 * 
 * @param queue_t pq pointer to queue for this FSM to use
 * @return bool true if event added to queue, otherwise false.
 */
bool InitMasterFSM(queue_t* pq) {
    event_t ThisEvent;
    pQueue = pq;
    // put us into the Initial PseudoState
    currentState = Master_InitPState;
    // post the initial transition event
    ThisEvent.EventType = SM_INIT;
    if (EnqueueFIFO(pQueue, ThisEvent) == true) {
        return true;
    } else {
        return false;
    }
}

/**
 * PostMasterFSM Posts an event to this FSM's queue
 * @param ThisEvent
 * @return bool true if Queued, false otherwise
 */
bool PostMasterFSM(event_t ThisEvent) {
    return EnqueueFIFO(pQueue, ThisEvent);
}


static char * smbBusStateNames[] =
        {
            "InitPState","SMBusIdle", "SendingStart","SendingAddr","SendingAddrR",
            "SendingCommand", "SendingBytes","SendingRepStart", "ReceivingBytes", 
            "SendingAckStat", "SendingStop", "Waiting4Stop"
        };

static char* readRegistersNames[] = {"RR_InitPState", "RR_Idle", "Writing2Mux",
            "Listening", "ReadingRegs", "WritingRegs"};
        
// Allocate memory for arrays we need every time the machine runs
static uint8_t rcBuffer[MAX_PACKET_SIZE]; //data bytes from PC
static uint8_t regs2Read[MAX_REGS]; // list of regs to read
static uint16_t regData[MAX_REGS]; // array to store data from read regs
static PinInfo_t pinData[PD_NUM_PINS]; // array to hold info on GPIO pins 
static PinInterp_t pinInterp[PD_NUM_PINS]; // array to hold pin interps
static bool ShippingModeSet;
static uint8_t regsToWrite[NUM_WRITE_REGS];
static uint8_t regWriteCmds[NUM_WRITE_REGS * 2];
static WriteSetup_t writeSetup;
static ReadSetup_t readSetup;
static uint16_t RunTimeToEmpty;
static event_t ReturnEvent;
static BattError_t currError;
static event_t CycleLEDEvent;
    
/**
 * Handles the high-level master behavior of the device in terms of polling
 * the batteries for information and handling requests from the PC. Relies
 * on other machines for communication processes. 
 * @param event_t
 * @return event_t SM_NO_EVENT if no error 
 */
event_t RunMasterFSM(event_t ThisEvent) {
    ReturnEvent.EventType = SM_NO_EVENT; // assume no errors
#if 0
    DBG_printf("RunMasterFSM() %s\n", GetEventName(ThisEvent.EventType));
#endif   
    

    if (ThisEvent.EventType == SM_TIMEOUT && ThisEvent.data == SM_WATCHDOG) {
        softRestart = true;
#ifdef   MASTER_FSM_DEBUG                                                                  
        DBG_printf("*** TIMEOUT READING BAT %u REGISTERS ***\r\n", battNumMon);
        // what state is SMBusMaster in?
        SMBusState_t busState = QuerySMBusFSM();
        ReadRegState_t readState = QueryReadRegsSM();
        DBG_printf("SMbusState %s ReadRegState: %s\r\n", smbBusStateNames[(int)busState], readRegistersNames[(int)readState] );                        
#endif  
#ifdef LOG_ALL        
        logAll();
#endif
        currentState = Listening2Bus;
        sm_initialize();
        //SMBusReset();
        ReadRegistersReset();
        timer_InitTimer(MONITOR_TIMER, 60);
        return ReturnEvent;

    }

    if (SET_SHIPMENT_MODE == ThisEvent.EventType) {
        ShippingModeSet = true;
    }


    switch (currentState) {

        case Master_InitPState: // If current state is initial Psedudo State
        {
            if (ThisEvent.EventType == SM_INIT) // only respond to SM_Init
            {

                // Init the monitor timer for a short timeout to get going
                timer_InitTimer(MONITOR_TIMER, 40);
                if (softRestart == true) {
                    currentState = CycleLEDS_Complete;
                } else {
                    currentState = CycleLEDS_Start;
                }

#ifdef STATS
                initializeStats(&rrStats);
#endif                
            }
        }
            break;

            /**** Cycle the LED'as at startup *****/
        case CycleLEDS_Start:
        {
            if (ThisEvent.EventType == SM_TIMEOUT) {
                SetAllLEDsOff();
                // next timeout, move to next state
                currentState = CycleLEDS_PlugInd_Green;
                timer_InitTimer(MONITOR_TIMER, 60);
            }
        }
            break;
            /**** CycleLEDS_PlugInd_Green *****/
        case CycleLEDS_PlugInd_Green:
        {
            if (ThisEvent.EventType == SM_TIMEOUT) {
                SetAllLEDsOff();
                CycleLEDEvent.data = PLUG_IND;
                CycleLEDEvent.EventType = LED_GRN_C;
                PostIndicatorFSM(CycleLEDEvent);
                timer_InitTimer(MONITOR_TIMER, 1000);
                currentState = CycleLEDS_BatteryCapacity_Yellow;
            }
        }
            break;
            /*** CycleLEDS_BatteryCapacity_Yellow *****/
        case CycleLEDS_BatteryCapacity_Yellow:
        {
            if (ThisEvent.EventType == SM_TIMEOUT) {
                SetAllLEDsOff();
                CycleLEDEvent.data = CHRG_IND;
                CycleLEDEvent.EventType = LED_YLW_C;
                PostIndicatorFSM(CycleLEDEvent);
                timer_InitTimer(MONITOR_TIMER, 1000);
                currentState = CycleLEDS_BatteryCapacity_Red;
            }
        }
            break;
            /*** CycleLEDS_BatteryCapacity_Red *****/
        case CycleLEDS_BatteryCapacity_Red:
        {
            if (ThisEvent.EventType == SM_TIMEOUT) {
                SetAllLEDsOff();
                // set the LED Indicator 
                CycleLEDEvent.data = CHRG_IND;
                CycleLEDEvent.EventType = LED_RED_C;

                currentState = CycleLEDS_BatteryFault_Yellow;
                PostIndicatorFSM(CycleLEDEvent);
                timer_InitTimer(MONITOR_TIMER, 1000);
            }
        }
            break;
            /**** CycleLEDS_BatteryFault_Yellow *****/
        case CycleLEDS_BatteryFault_Yellow:
        {
            if (ThisEvent.EventType == SM_TIMEOUT) {
                SetAllLEDsOff();
                CycleLEDEvent.data = STAT_IND;
                CycleLEDEvent.EventType = LED_YLW_C;
                PostIndicatorFSM(CycleLEDEvent);
                timer_InitTimer(MONITOR_TIMER, 1000);
                currentState = CycleLEDS_BatteryFault_Red;
            }
        }
            break;
            /*** CycleLEDS_BatteryFault_Red *****/
        case CycleLEDS_BatteryFault_Red:
        {
            if (ThisEvent.EventType == SM_TIMEOUT) {
                SetAllLEDsOff();
                // set the LED Indicator 
                CycleLEDEvent.data = STAT_IND;
                CycleLEDEvent.EventType = LED_RED_C;
                PostIndicatorFSM(CycleLEDEvent);
                timer_InitTimer(MONITOR_TIMER, 1000);
                currentState = CycleLEDS_Complete;
            }
        }
            break;

            /*** CycleLEDS_Complete *****/
        case CycleLEDS_Complete:
        {
            if (ThisEvent.EventType == SM_TIMEOUT) {
                SetAllLEDsOff();
                currentState = Listening2Bus;
                timer_InitTimer(MONITOR_TIMER, 60);
            }
        }
            break;

            /*----- Listening2Bus -----*/
        case Listening2Bus:
        {
            //if the monitoring timer has timed out
            if (ThisEvent.EventType == SM_TIMEOUT) {
                if (ShippingModeSet == true) {
#ifdef MASTER_FSM_DEBUG                        
                    DBG_printf("\rRunMasterFSM() Timeout: Shipment Mode\r\n");
#endif                    
                    // setup writing to battery registers to put the batteries 
                    // in shipment mode
                    for (uint8_t i = 0; i < NUM_WRITE_REGS; ++i) {
                        regsToWrite[i] = shipmentRegisters[i];
                    }
                    for (uint8_t i = 0; i < NUM_WRITE_REGS * 2; ++i) {
                        regWriteCmds[i] = shipmentCmds[i];
                    }
                    BatteryError[battNumMon] =
                            writeSetup.battNum = battNumMon;
                    writeSetup.numRegs = NUM_WRITE_REGS;
                    writeSetup.pRegs2Write = regsToWrite;
                    writeSetup.pRegWriteData = regWriteCmds;
                    WriteRegs_Start(&writeSetup);
                    // increment the count to the next battery                    
                    if (++battNumMon == NUM_BATTS) {
                        battNumMon = 0;
                    }
                    currentState = WritingRegisters;
                } else {
                    if (QueryReadRegsSM() != RR_Idle) {
                        // poll is likely in progress - don't start a new read yet
                    } else {
                        //start to read the registers that are always polled

                        // zero the registers so we don't carryover between batteries                    
                        memset(regData, 0, MAX_REGS * 2);
                        uint8_t i = 0;
                        for (i = 0; i < NUM_POLL_REGS; i++) {
                            // copy from const list
                            regs2Read[i] = pollRegs[i];
                        }
                        //ReadSetup_t readSetup;
                        readSetup.battNum = battNumMon;
                        readSetup.numRegs = NUM_POLL_REGS;
                        readSetup.pRegs2Read = regs2Read;
                        readSetup.pRegData = regData;
#ifdef STATS                    
                        startStatsTiming(&rrStats);
#endif                    
                        timer_InitTimer(SM_WATCHDOG, 1000);
                        if (ReadRegs_Start(&readSetup) == true) {
                            // increment the count to the next battery                    
                            if (++battNumMon == NUM_BATTS) {
                                battNumMon = 0;
                            }
                            // transition to Reading Registers
                            currentState = ReadingRegisters;
                        } else {
                            // couldn't start read registers, 
                        }
                    }
                }
            }
        }
            break;
            /*----- WritingRegisters _____*/
        case WritingRegisters:
            timer_InitTimer(MONITOR_TIMER, MONITOR_TIME);
            currentState = Listening2Bus;
            break;
            /*----- ReadingRegisters -----------*/
        case ReadingRegisters:
        {

            if (ThisEvent.EventType == RR_READ_DONE) {
#ifdef STATS                
                finishStatsTiming(&rrStats);
#endif                
                timer_StopTimer(SM_WATCHDOG);

                event_t IndEvent;
                if (registersRead < READ_REGISTERS_TWENTY) {
                    ++registersRead;
                }
                UpdateGPIOValues();

                if (ThisEvent.data == 0) //machine returned no errors
                {
                    // update the status and store the returned error
                    currError = BattStat_Update(readSetup.battNum,
                            regData[relative_charge_index],
                            regData[tempertuare_index],
                            regData[status_index],
                            regData[run_time_too_empty_index],
                            regData[full_charge_capacity_index]);

                    // we had a good read, set the battery status to reflect
                    BattState_SetError(readSetup.battNum, BATT_NO_ERROR);

                    // check the charge                        
                    RunTimeToEmpty = BattState_QueryMinimumTimeToRun();
                    // if the RunTimeToEmpty > 20 minutes, or the 
                    // power supply is attached and charging,
                    // turn off the battery capacity LED                        
                    if ((RunTimeToEmpty > RTE_LIMIT_TWENTY) || IsPsuAttached()) {
                        // turn off the battery capacity LED
                        IndEvent.EventType = LED_OFF;
                        IndEvent.data = CHRG_IND;
                        PostIndicatorFSM(IndEvent);
                    } else if ((RunTimeToEmpty < RTE_LIMIT_TWENTY) && RunTimeToEmpty > RTE_LIMIT_TEN) {
                        // set battery LED to yellow flashing
                        IndEvent.EventType = LED_YLW_F;
                        IndEvent.data = CHRG_IND;
                        PostIndicatorFSM(IndEvent);
                    } else if (RunTimeToEmpty < RTE_LIMIT_TEN) {
                        IndEvent.EventType = LED_RED_F;
                        IndEvent.data = CHRG_IND;
                        PostIndicatorFSM(IndEvent);
                    }

                    // check the BATT_CHARGE_CAPACITY_LIMIT
                    uint8_t minCapacityRatio = BattState_QueryBatteryMinAbsChargePercent();                    
                    if (minCapacityRatio < BATT_CHARGE_CAPACITY_LIMIT) {
                        BattState_SetError(readSetup.battNum, BATT_CAPACITY_LOW);
                        currError = BATT_CAPACITY_LOW;
                    }                     
                } else if (ThisEvent.data == ADDR_NACK) {
                    BattStat_SetOffBus(readSetup.battNum);
                    BattState_SetError(readSetup.battNum, BATT_NOT_ON_BUS);
                    currError = BATT_NOT_ON_BUS;

                } else if (ThisEvent.data == MUX_ERR) {
                    BattState_SetError(readSetup.battNum, BATT_NOT_ON_BUS);
                    currError = SYS_MUX_FAILURE;

                } else { // else if the error is a command nack or something minor
                    currError = BATT_UNKNOWN_ERROR; // not specific yet                    
                    BattState_SetError(readSetup.battNum, BATT_NOT_ON_BUS);
                    // don't worry, hopefully it will be cleared up on the next poll                    
                }
                // because the GPIO pins can float high, set the PSU as not present                
                if (currError != BATT_NO_ERROR) {
                    SetPSUState(readSetup.battNum, false);
                }
                SendBroadCast(regData);
                if (registersRead >= READ_REGISTERS_TWENTY) {
                    SetStatIndicator(currError);
                }


                // Restart the monitor timer
                timer_InitTimer(MONITOR_TIMER, MONITOR_TIME);
                // go back to Listening2Bus
                currentState = Listening2Bus;
                logLEDState();
            } // end of  ThisEvent.EventType == RR_READ_DONE
        } // end of case ReadingRegisters:
            break;

            /*--------------------------------------------------------- END OF STATES */
            // repeat state pattern as required for other states
        default:
            ;
    } // end switch on Current State
    return ReturnEvent;
}

/**
 * QueryMasterFSM
 * @return MasterState_t state of the FSM
 */
MasterState_t QueryMasterFSM(void) {
    return currentState;
}


/***************************************************************************
 private functions
 ***************************************************************************/

/**
 * @function SendBroadCast
 * @param pRegData pointer to battery register data
 * @returns none
 * @description sends a data packet with the battery status
 *
 */
static void SendBroadCast(uint16_t *pRegData) {    
    UpdateGPIOValues();

    uint8_t bytesToSend = SIZE_BROADCAST + 34;
    // Allocate static array to hold the bytes in a broadcast
    static uint8_t bcArray[BC_ARRAY_SIZE]; // static for speed           
    memset((void *) bcArray, 0, BC_ARRAY_SIZE);
    uint8_t i = 0;
    bcArray[i++] = 0x1F; //SIZE_BROADCAST + 2 - 1; // Gerry wants 0x1F        
    bcArray[i++] = readSetup.battNum;
    uint8_t status = 0;
    gpioStatus_t gpioStatus = GetGPIOStatus(readSetup.battNum);
    BattError_t batteryError = BattStat_QueryError(readSetup.battNum);
    uint8_t minCapacityRatio = BattState_QueryBatteryMinAbsChargePercent();     
    
    if (gpioStatus.psu) {
        status |= 0x01; // PSU attached        
    }
    if (gpioStatus.gpio1_error) {
        status |= 0x02;
    }
    if (gpioStatus.gpio2_error) {
        status |= 0x04;
    }
    
    if (minCapacityRatio < BATT_CHARGE_CAPACITY_LIMIT) {
        status |= 0x08;
    }
    if (batteryError != BATT_NO_ERROR) {
        status |= 0x10;
    }
    bcArray[i++] = status;


    // loop through to add register words to the packet            
    for (uint8_t regIndex = 0; regIndex < NUM_POLL_REGS; ++regIndex) {
        if ((regIndex == max_error_index) ||
                (regIndex == relative_charge_index) ||
                (regIndex == absolute_charge_index)) {
            // we send a single byte for these registers
            bcArray[i++] = *pRegData & 0x00ff;
        } else {
            // place lower byte first
            bcArray[i++] = *pRegData & 0x00ff;
            // placer higher byte next
            bcArray[i++] = *pRegData >> 8;
        }
        // move to next value
        pRegData++;
    }
    // Power Status LED
    bcArray[i++] = GetStatusIndicatorForPC(PLUG_IND);
    // capacity status led
    bcArray[i++] = GetStatusIndicatorForPC(CHRG_IND);
    // fault status led
    bcArray[i++] = GetStatusIndicatorForPC(STAT_IND);

    // loop through the bytes to get the sum
    uint8_t sum = 0;
    for (uint8_t j = 0; j < i; j++) {
        sum += bcArray[j];
    }
    //place checksum into last byte
    bcArray[i] = 0xff - sum;

    // send byte packet to PC
    CompComm_Write(bcArray, bytesToSend);
              

    
            

#ifdef WATCHDOG_TEST
#else    
    CLRWDT(); // clear watchdog
#endif    
    return;
}

/**
 * @function ProcessPinState
 * @param pData enum pin state
 */
static void ProcessPinState(PinInterp_t* pData) {
    // query state of pins using struct definition
    PinInfo_t pinStates[PD_NUM_PINS];
    QueryPulseDetectFSM(pinStates);
    // loop through each pin to get the interp
    uint8_t i = 0;
    for (i = 0; i < PD_NUM_PINS; i++) {
        switch (pinStates[i].state) {
                // if pin is the constant low state
            case ConstLo:
            {
                // set value to LOW
                *pData = PIN_LOW;
            }
                break;
                // else if pin in constant high state
            case ConstHi:
            {
                // set value to HIGH
                *pData = PIN_HIGH;
            }
                break;
                // else if pin in blinking state
            default:
            {
                uint16_t period = pinStates[i].period;
                // check the period
                if (period == 0) {
                    // check the pin value
                    *pData = (pinStates[i].value) ? PIN_HIGH : PIN_LOW;
                } else if ((period < 410) && (period > 390)) {
                    // set to blink 400
                    *pData = BLINK_400;
                } else if ((period < 2010) && (period > 1990)) {
                    // set to blink 2000
                    *pData = BLINK_2000;
                }
            }
                break;
        }
        // increment data pointer
        pData++;
    }
}

/**
 * @function SetStateIndicator
 * @description Sets the state indicator LED according to the current state.
 * @returns none
 */
static void SetStatIndicator(BattError_t CurrError) {
    static AlarmState_t alarmState = state_NoAlarm;
    static uint32_t delayTimeEnd;
    bool isAlarm = false;
    bool displayAlarm = false;
    event_t IndEvent;
    IndEvent.data = STAT_IND;

    // test - cause a wacthdog reset
#ifdef WATCHDOG_TEST    
    while (1) {
        ;
    }
#endif    
    // if any battery has an error or any GPIO has an error, 
    // light the status indicator. Otherwise, turn it off
    BattError_t batteryError = BattStat_QueryErrorAllBatteries();
    isAlarm = ((batteryError != BATT_NO_ERROR) || IsGPIOError());

    switch(alarmState)
    {
        case state_NoAlarm:
            if(isAlarm == true){
                alarmState = state_AlarmDelay;
                delayTimeEnd = getSysSecondCounter() + ALARM_DELAY;                
            }
            break;
            
        case state_AlarmDelay:
            if(!isAlarm){
                alarmState = state_NoAlarm;
            } else if(getSysSecondCounter() >= delayTimeEnd){
                alarmState = state_DisplayAlarm;
            }
            break;
        case state_DisplayAlarm:        
            if(!isAlarm){
                alarmState = state_NoAlarm;
            } else {
                displayAlarm = true;
            }
            break;
        default:
            alarmState = state_NoAlarm;
            break;
    };
    
    if (displayAlarm == true) {
        // turn the LED on
        if (IsPsuAttached()) {
            IndEvent.EventType = LED_YLW_F;
        } else {
            IndEvent.EventType = LED_RED_F;
        }
    } else {
        IndEvent.EventType = LED_OFF;
    }

    PostIndicatorFSM(IndEvent);
    
#ifdef INDICATOR_DEBUG
  // get the status of the battery status
    static uint8_t previousStatusState = Off;
    static uint8_t previousChargeState = Off;
    
    uint8_t state = GetCurrentIndicatorState(STAT_IND);
    
    if( state == FastRed && previousStatusState != state){
        previousStatusState = state;
        DBG_printf("%s** Battery Status LED Flashing Red **\n", GetSecondsMinutesHours() );
    } else if (state == FastYlw && previousStatusState != state){
        previousStatusState = state;
        DBG_printf("%s** Battery Status LED Flashing Yellow **\n", GetSecondsMinutesHours());
    } else if ( state == Off && previousStatusState != state){
        previousStatusState = state;
        DBG_printf("%s** Battery Status LED Off **\n", GetSecondsMinutesHours());
    }   
    // get the status of the battery capacity alarms
    state = GetCurrentIndicatorState(CHRG_IND);
    
    if( state == FastRed && previousChargeState != state){
        previousChargeState = state;
        DBG_printf("%s** Battery Capacity LED Flashing Red **\n", GetSecondsMinutesHours() );
    } else if (state == FastYlw && previousChargeState != state){
        previousChargeState = state;
        DBG_printf("%s** Battery Capacity LED Flashing Yellow **\n", GetSecondsMinutesHours());
    }   else if( state == Off && previousChargeState != state) {
        previousChargeState = state;
        DBG_printf("%s** Battery Capacity LED Off **\n", GetSecondsMinutesHours());
    }
    
    
#endif
    return;
}
/**
 * updates the GPIO values in the battery BattStat. Handles the 
 * PSU status LED state
 */
void UpdateGPIOValues() {    
    event_t IndEvent;
    // query the pins to see the state, periods, and values
    QueryPulseDetectFSM(pinData);
    // process to see condition of pins
    ProcessPinState(pinInterp);
    PinInterp_t gpio1 = pinInterp[gpioIndices[readSetup.battNum][0]];
    PinInterp_t gpio2 = pinInterp[gpioIndices[readSetup.battNum][1]];

    gpioStatus_t gpioStatus = GetGPIOStatus(readSetup.battNum);
    gpioStatus.gpio1_state = gpio1;
    gpioStatus.gpio2_state = gpio2;
    gpioStatus.gpio1_error = (gpio1 == BLINK_400 ? 1 : 0);
    gpioStatus.gpio2_error = (gpio2 == BLINK_400 ? 1 : 0);
    gpioStatus.psu = (gpio2 == PIN_HIGH) ? 1 : 0;

    SetGPIOState(readSetup.battNum, gpioStatus);

    // Set the Plug indicator                        
    IndEvent.data = PLUG_IND;
    if (IsPsuAttached()) {
        IndEvent.EventType = LED_GRN_C;
    } else {
        IndEvent.EventType = LED_OFF;
    }
    PostIndicatorFSM(IndEvent);

    return;
}



#ifdef INDICATOR_DEBUG 

/**
 * Logs various values to the serial debug port. 
 */
static void logLEDState() {
        static uint32_t last_seconds;
    
    // output once every 30 seconds
    if ((getSysSecondCounter() % 30) != 0) {
        return;
    }
    
    // avoid printing twice for one second
    if (last_seconds == getSysSecondCounter()) {
        return;
    }
    
    uint8_t attached = IsPsuAttached();    
    DB_printf("%s Chrg:%d PSU:%u RT:%d\n\r", GetSecondsMinutesHours(), 
                                             (int)BattStat_QueryCharge(),                                                    
                                              attached, 
                                              (int)BattState_QueryMinimumTimeToRun());
                                              
                                               
        
 
    last_seconds = getSysSecondCounter();


}
#else  //INDICATOR_DEBUG not defined

static void logLEDState() {
}
#endif

#ifdef STATS

void initializeStats(timeStats_t* stats) {
    stats->max = 0;
    stats->min = UINT16_MAX;
    stats->n = 0;
    stats->sum = 0;
    stats->startTic = 0;
}

uint16_t average(timeStats_t *stats) {
    uint16_t average = 0;
    if (stats->n > 0) {
        average = (uint16_t) (stats->sum / stats->n);
    }
    return average;
}

void finishStatsTiming(timeStats_t *stats) {
    uint16_t count = getSysTickCounter();
    uint16_t diff = 0;
    if (count < stats->startTic) {
        diff = UINT16_MAX - stats->startTic;
    } else {
        diff = count - stats->startTic;
    }
    stats->sum += diff;
    stats->n++;
    if (diff > stats->max) {
        stats->max = diff;
    }
    if (diff < stats->min) {
        stats->min = diff;
    }
}

void startStatsTiming(timeStats_t *stats) {
    stats->startTic = getSysTickCounter();
}
#endif