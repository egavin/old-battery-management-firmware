/****************************************************************************
 ** Copyright (c) 2021 Supira Medical
 ** Author(s): rmerchant, Ed Gavin
 ** Purpose: Module for Indicator State Machine
 ****************************************************************************/

#ifndef FSMTemplate_H
#define FSMTemplate_H

#include "sm_queue.h"

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  Off = 0,    // Indicator is off
  SolidGrn,   // Indicator is solid green, not blinking
  SolidRed,   // Indicator is solid red, not blinking
  SolidYlw,   // Indicator is solid yellow, not blinking
  FastRed,    // Indicator is blinking red quickly
  FastYlw,    // Indicator is blinking yellow quickly
}IndicatorState_t;

#define NUM_INDS 3 // three indicators
// Number definitions for each indicator
#define PLUG_IND 0 // unit plugged in or not
#define CHRG_IND 1 // charge level of the battery
#define STAT_IND 2 // error status indicator
// Public Function Prototypes

bool InitIndicatorFSM(queue_t* qp);
bool PostIndicatorFSM(event_t ThisEvent); 
event_t RunIndicatorFSM(event_t ThisEvent);
uint8_t GetCurrentIndicatorState(uint8_t ind);
uint8_t GetStatusIndicatorForPC(uint8_t ind);
void SetAllLEDsOff(void);


#endif /* FSMTemplate_H */

