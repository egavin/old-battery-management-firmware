/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Author(s): Ed Gavin
** Purpose:  data and functions related to porting the framework to a specific 
* microprocessor and pin configuration
****************************************************************************/

#ifndef SM_PORT_H
#define	SM_PORT_H

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include "sm_main.h"
#include "sm_events.h"
#define REENTRANT __reentrant
// variable to hold interrupt mask 
extern uint8_t _INTCON_temp;

// enable high and low priority interrupts
#define USE_INT_PRIORITY

// Macro's to disable interrupts during critical code
#define EnterCritical() { _INTCON_temp = INTCON; GIE=0;}
#define ExitCritical() { INTCON = _INTCON_temp; }

/* Rate constants for programming the SysTick Period to generate tick interrupts.
   These assume that we are using TMR6 to generate the periodic interrupt. Based
   on the structure to T6CON, the values in the enum are the values to write 
   directly to the T6CON register with bit 7 cleared
 */
typedef enum
{
  ES_Timer_RATE_OFF  = 0,
  ES_Timer_RATE_1mS = 0x60,     /* Prescale of 64, Postscale of 1 */ 
  ES_Timer_RATE_2mS = 0x61,     /* Prescale of 64, Postscale of 2 */
  ES_Timer_RATE_4mS = 0x63,     /* Prescale of 64, Postscale of 4 */
  ES_Timer_RATE_5mS = 0x64,      /* Prescale of 64, Postscale of 5 */
  ES_Timer_RATE_10ms = 0x69     /* Prescale of 64, Postscale of 10 */
}TimerRate_t;

// map the generic functions for testing the serial port to actual functions
// for this platform. If the C compiler does not provide functions to test
// and retrieve serial characters, you should write them in ES_Port.c
#ifdef BOARD_V2
#define IsNewKeyReady() (RC2IF)
#else
#define IsNewKeyReady() (RC2IF)
#endif
#define GetNewKey() getchar()

// this function connects printf & puts to the serial port on the PIC
void putch(char data);

#ifdef PC_SERIAL_INPUT_ENABLED
bool kbhit(void);                // is a character ready on the EUSART?
#endif

// prototypes for the hardware specific routines
void _HW_PIC18F26Q10Init(void);
void _HW_Timer_Init(const TimerRate_t Rate);
bool _HW_Process_Pending_Ints(void);
uint16_t _HW_GetTickCount(void);
void _HW_SysTickIntHandler(void);
void Timer6InterruptHandler();
bool processPendingInts(void);
void smHWInit();
uint16_t getSysTickCounter(void);
uint32_t getSysSecondCounter(void);

#endif	/* SM_PORT_H */

