/****************************************************************************
 ** Copyright (c) 2021 Supira Medical
 ** Authors: Ed Gavin
 ** Purpose: State machine to monitor the shipment mode jumper and cause the
 * system to set the batteries in shipment mode if the jumper is in for ten
 * seconds.
 ****************************************************************************/


#include "sm_queue.h"
#include "sm_events.h"
#include "sm_timer.h"
#include "sm_port.h"
#include "ShipmentModeFSM.h"
#include "MasterFSM.h"
#include "BattStat.h"
#include "mcc_generated_files/pin_manager.h"

#ifdef DEBUG_SHIPMENT_MODE
#define DEBUG_PRINT_ENABLED  
#include "dbprintf.h"
#endif

// variables
static ShipmentModeState_t state;
static int SwitchClosedCount;

/**
 * Initialize the state machine
 * @param pq
 * @return true if successful, false otherwise.
 */
bool InitShipmentModeFSM(queue_t *pq) {
    event_t ThisEvent;
    pQueue = pq;

    ThisEvent.EventType = SM_INIT;
    ThisEvent.data = 0;
    state = SHIP_MODE_IDLE;
    return PostShipmentModeFSM(ThisEvent);
}

/**
 * Post event to the shipment mode queue
 * @param ThisEvent
 * @return 
 */
bool PostShipmentModeFSM(event_t ThisEvent) {
    return EnqueueFIFO(pQueue, ThisEvent);
}

/**
 * Run the Shipment mode FSM
 * @param ThisEvent
 * @return event_t result event
 */
event_t RunShipmentModeFSM(event_t ThisEvent) {
    event_t ReturnEvent;
    ReturnEvent.EventType = SM_NO_EVENT;

    event_t ShipmentEvent;
    ShipmentEvent.EventType = SET_SHIPMENT_MODE;
    ShipmentEvent.data = 0;

#ifdef TRACE_OUT
    DBG_printf("RunShipmentModeFSM() %s\n", GetEventName(ThisEvent.EventType));
#endif    
    // this state machine only needs to handle Init and timeout.
    switch (ThisEvent.EventType) {
        case SM_INIT:
            state = SHIP_MODE_INIT;
            break;

        case SM_TIMEOUT:
            break;

        default:
            return ReturnEvent;
    }

    switch (state) {
        case SHIP_MODE_INIT:
            state = SHIP_MODE_IDLE;
            SwitchClosedCount = 0;
            timer_InitTimer(SHIP_MODE, 100);
            break;

        case SHIP_MODE_IDLE:
            if ((IO_SHIPMENT_MODE_GetValue() == SHIPMENT_SWITCH_ON) &&
                    (IsPsuAttached() == false)) {
                state = SHIP_MODE_COUNTING;
                SwitchClosedCount = 1;
            }
            break;

        case SHIP_MODE_COUNTING:
            if (ThisEvent.EventType == SM_TIMEOUT) {
#ifdef TRACE_OUT                
                DBG_printf("RunShipmentModeFSM: SHIP_MODE_COUNTING count:%d\n", SwitchClosedCount);
#endif                
                if (IO_SHIPMENT_MODE_GetValue() == SHIPMENT_SWITCH_OFF) {
                    state = SHIP_MODE_IDLE;
                    SwitchClosedCount = 0;
                } else {
                    if (++SwitchClosedCount >= MAX_SWITCH_CLOSED_COUNT) {
                        state = SHIP_MODE_SHIPMENT;
                    }
                }
            }
            break;

        case SHIP_MODE_SHIPMENT:
#ifdef TRACE_OUT            
            DBG_printf("RunShipmentModeFSM: SHIP_MODE_SHIPMENT\n");
#endif            
            SM_PostAll(ShipmentEvent);
            state = SHIP_MODE_FINISHED;
            break;

        case SHIP_MODE_FINISHED:
#ifdef TRACE_OUT            
            DBG_printf("RunShipmentModeFSM: SHIP_MODE_FINISHED\n");
#endif            
            break;
    }

    // set the timer for another 100 mS unless we are done
    if ((ThisEvent.EventType == SM_TIMEOUT) &&
            (state != SHIP_MODE_FINISHED)) {
        timer_InitTimer(SHIP_MODE, 100);
    }
    return ReturnEvent;
}