/****************************************************************************
 ** Copyright (c) 2021 Supira Medical
 ** Authors: Ed Gavin
 ** Purpose: State machine to monitor the shipment mode jumper and cause the
 * system to set the batteries in shipment mode if the jumper is in for ten
 * seconds.
 ****************************************************************************/
#include "sm_events.h"

// variables
queue_t* pQueue;


#ifndef SHIPMENTMODEFSM_H
#define	SHIPMENTMODEFSM_H

#define SHIPMENT_SWITCH_ON 0
#define SHIPMENT_SWITCH_OFF 1
#define MAX_SWITCH_CLOSED_COUNT 100

typedef enum {
    SHIP_MODE_INIT = 255,
    SHIP_MODE_IDLE = 0,
    SHIP_MODE_COUNTING = 1,
    SHIP_MODE_SHIPMENT = 2,
    SHIP_MODE_FINISHED = 3
} ShipmentModeState_t;

bool InitShipmentModeFSM(queue_t* pq);
bool PostShipmentModeFSM(event_t ThisEvent);
event_t RunShipmentModeFSM(event_t ThisEvent);
ShipmentModeState_t QueryShipmentModeFSM();

#endif	/* SHIPMENTMODEFSM_H */

