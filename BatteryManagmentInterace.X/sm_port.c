/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Author(s): Ed Gavin
** Purpose:  data and functions related to porting the framework to a specific 
* microprocessor and pin configuration
****************************************************************************/

#include <xc.h>
#include "mcc_generated_files/mcc.h"
#include <stdint.h>
#include <stdbool.h>
#include "sm_main.h"
#include "sm_port.h"
#include "sm_timer.h"
#define DEBUG_PRINT_ENABLED
#include "dbprintf.h"


// Defines
#define BAUD_CONST 34
#define BAUD_SPEED 1
// period to generate a 0.5 mS tick with the postscaler
#define TMR6_PERIOD 249

/*--- Local Functions ---*/
static void _HW_ConsoleInit(void);
static void _HW_InterruptInit(void);
static void _HW_DebugInit(void);

void Timer6InterruptHandler(void);

/* --- Local Variables ---*/
// SysTickIntCount incremented by the timer interrupt, decremented when
// timer ticks are processed
static volatile uint8_t SysTickIntCount;

// Global 1 mS tick counter
static volatile uint16_t SysTickCounter = 0; 

// global second counter
static volatile uint32_t SysSecondsCounter;

// counter 
static volatile uint16_t MillisecondsCounter = 0;

// variable to store the state of the interrupt mase when using
// the EnterCritical/ExitCritical macros
uint8_t _INITCON_temp;

/**
 * Initialize the hardware on the PIC.
 */
void _HW_PIC18F26Q10Init(void)
{
    _HW_DebugInit();
    _HW_ConsoleInit();
    _HW_InterruptInit();
    _HW_Timer_Init(ES_Timer_RATE_1mS);
}

/**
 * Initializes the hardware timer for 1 mS interrupts
 * @param Rate value to set the timer period - 1 mS in this case.
 */
void _HW_Timer_Init(const TimerRate_t Rate)
{
  // If timer rate is enabled
  if(Rate)
  {
    // Set the prescale/postcale based on the rate selected
    T6CON = (uint8_t)Rate;
    // Select FOSC/4 as the clock source
    T6CLKCON = 0x01; 
    // Set mode to Software gate and sync to FOSC/4
    T6HLT = 0x00;
    T6HLTbits.CKSYNC = 1;
    T6HLTbits.PSYNC = 1;
    // Load the rate value into the period register
    T6PR = TMR6_PERIOD;
    // Clear TMR6
    T6TMR = 0;
    // Clear any TMR6 interrupt
    PIR4bits.TMR6IF = 0;
#ifdef USE_INT_PRIORITY
    // Set priority to high
    IPR4bits.TMR6IP = 1;
#endif
    // Enable TMR6 interrupt
    PIE4bits.TMR6IE = 1;
    // Start TMR6
    T6CONbits.ON = 1; 
  }  
}

/**
 * Handles the timer interrupt. Updates the timers
 */
void _HW_SysTickIntHandler()
{
INTCONbits.GIE = 0;
  PIR4bits.TMR6IF = 0;  // clear the source of the interrupt
  ++SysTickIntCount;          /* flag that it occurred and needs a response */
  ++SysTickCounter;     // keep the free running time going
  if(++MillisecondsCounter == 1000){
      ++SysSecondsCounter;
       MillisecondsCounter = 0;
  }
  INTCONbits.GIE = 1;  
#ifdef STAT_DEBUG  
  if(SysTickCounter % 1000 == 0){
      ++SysSecondsCounter;
  }
#endif

}

/**
 * Called to update the state machine timers after a timer interrupt.
 * @return always true. 
 */
bool processPendingInts()
{
    while(SysTickIntCount > 0){
        timer_ProcessTicks();
        --SysTickIntCount;
    }
    return true;
}

/**
 * function: getSysTickCounter
 * arguments: none
 * @return uint16_t system tick count
 */
uint16_t getSysTickCounter()
{ 
    return SysTickCounter;
}

/**
 * Gets the second counter.
 * @return 
 */
uint32_t getSysSecondCounter(){ return SysSecondsCounter;}

/**
 * Write a character to USART 1
 * @param data
 */
void putch(char data) {
    
  while( ! TX1IF)   // check if last character finished
  {}
  TX1REG = data;    // send data  
}

/**
 * Checks if there is a character available on UART 2
 * @return 
 */
int getchar(void) {
  while( ! RC2IF)   // check buffer
  {}                // wait till ready
  return RC2REG;
}

/**
 * Setup the PIC hardware that isn't already setup my MCC
 */
static void _HW_ConsoleInit(void)
{
  // Configure pins for UART 
  ANSELB4 = 0;
  ANSELB5 = 0;
  RC1PPS = 0x09;            // maps RX1 to RC1
  RX1PPS = 0x10;            // maps TX1 to RC0

  // Initialize the SPBRG(H) register(s) for the appropriate baud rate. 
  // If a high-speed baud rate is desired, set bit BRGH 
  BAUD1CONbits.BRG16 = BAUD_SPEED;
  SP2BRGL = BAUD_CONST;
  // Enable the asynchronous serial port by clearing bit SYNC and setting SPEN  
  TX1STAbits.SYNC = 0;
  RC1STAbits.SPEN = 1;
  
  // Enable the transmission by setting bit TXEN which will also set 
  // bit TXIF in PIR1.
  TX1STAbits.TXEN = 1;
  // Enable the reception by setting the CREN bit.
  RC1STAbits.CREN = 1; 
  
} 

/**
 * Enables interrupts
 */
static void _HW_InterruptInit(void)
{
  // Determine if interrupt priority is enabled
#ifdef USE_INT_PRIORITY // use priority feature
  // Set the IPEN bit
  INTCONbits.IPEN = 1;
  // Set GIEH and GIEL
  INTCONbits.GIEH = 1;
  INTCONbits.GIEL = 1;
#else // Don't use prioty
  // Clear IPEN bit
  INTCONbits.IPEN = 0;
  // enable peripheral and gloabl ints
  INTCONbits.GIE = 1;
  INTCONbits.PEIE = 1;
#endif
  
  return;
}

/**
 * Initialize hardware for a Debug LED, if one is available
 */
static void _HW_DebugInit(void)
{
 #ifdef LED_DEBUG  
  //Set up RA2 for LED
    ANSELAbits.ANSELA2 = 0;
    LATAbits.LATA2 = 0; // start with LED off
    TRISAbits.TRISA2 = 0; // set TRIS bit
#endif
}

#ifdef LED_DEBUG
/**
 * Toggles the debug LED if available
 */
static void BlinkLED(void)
{
    // toggle state of LED
    LATAbits.LATA2 = ~LATAbits.LATA2; 
}
#endif