/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Author: Ed Gavin
** Purpose: Functions related to running the state machine framework
****************************************************************************/

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include "sm_main.h"
#include "sm_queue.h"
#include "sm_events.h"
#include "sm_port.h"
#include "sm_timer.h"
#include "MasterFSM.h"
#include "SMBusMaster.h"
#include "IndicatorsFSM.h"
#include "PulseDetectFSM.h"
#include "IndicatorsFSM.h"
#include "ReadRegsFSM.h"
#include "CompComm.h"
#include "ShipmentModeFSM.h"
#include "mcc_generated_files/mcc.h"

#ifdef SM_MAIN_DEBUG
#define DEBUG_PRINT_ENABLED  
#include "dbprintf.h"
#endif


#ifdef CHECK_EVENT_STATS

typedef struct timeStats {
    uint16_t n;
    uint32_t sum;
    uint16_t min;
    uint16_t max;
    uint16_t startTic;
} timeStats_t;

void initializeEventStats(timeStats_t* stats);
void updateEventStatsTiming(timeStats_t *stats);

// for tracking time in read registers
static timeStats_t eventStats;

void initializeEventStats(timeStats_t* stats) {
    stats->max = 0;
    stats->min = UINT16_MAX;
    stats->n = 0;
    stats->sum = 0;
    stats->startTic = 0;
}


void updateEventStatsTiming(timeStats_t *stats) {    
    uint16_t TicCount = getSysTickCounter();
    uint16_t diff = 0;
    if (TicCount < stats->startTic) {
        stats->startTic = TicCount;
        return; // the counter rolled over. Ignore for averages.
    } else {
        diff = TicCount - stats->startTic;
    }
    diff = TicCount - stats->startTic;
    stats->sum += diff;
    stats->n++;
    if (diff > stats->max) {
        stats->max = diff;
    }
    if (diff < stats->min) {
        stats->min = diff;
    }
    stats->startTic = TicCount;
    if(stats->n == 50000){
        DBG_printf("Event Stats: average:%u Min:%u Max:%u\r\n", (uint16_t) (stats->sum / stats->n), stats->min, stats->max); 
        // clear the stats
        stats->n = 0;
        stats->max = 0;
        stats->min = 0;
        stats->sum = 0;
    }
}
#endif

// local functions
void LogQueueEvents(queue_t* queue, char* QueueName);
void initializeQueues(void);
void initializeTimers(void);
static uint8_t MaxQueueEvents = 0;
// the Event Queues are declared here.
queue_t MasterQueue;
queue_t SMBusMasterEventQueue;        
queue_t ReadRegsQueue;        
queue_t IndicatorQueue;
queue_t PulseDetectQueue;
queue_t ShipmentModeQueue;

// array of pointer to queues
queue_t* Queues[] = {
    &MasterQueue,
    &SMBusMasterEventQueue,
    &ReadRegsQueue,
    &IndicatorQueue,
    &PulseDetectQueue,
    &ShipmentModeQueue            
};

char* QueueNames[] = { 
        "Master",
        "SMBus",
        "Read Registers",
        "Indicators",
        "Pulse Detectors",
        "Shipment Mode"
};


#define NUM_QUEUES 6
uint8_t GetMaxQueueSize(){ return MaxQueueEvents;}

/**
 *  initialize the framework
 */
void sm_initialize()
{
    initializeQueues();
    initializeTimers();
    // initialize the FSM's
    InitMasterFSM(&MasterQueue);
    InitSMBusFSM(&SMBusMasterEventQueue);
    InitReadRegsFSM(&ReadRegsQueue);
    InitIndicatorFSM(&IndicatorQueue);
    InitPulseDetectFSM(&PulseDetectQueue);
    InitShipmentModeFSM(&ShipmentModeQueue);
#ifdef CHECK_EVENT_STATS
    initializeEventStats(&eventStats);
#endif     
}

static CheckFunc *const SM_EventList[] = {
  EVENT_CHECK_LIST
};

/**
 * initializeQueues calls each queue's initQueue function
 * @param event
 * @return 
 */
void initializeQueues()
{
    for(uint8_t i = 0; i < NUM_QUEUES; ++i){
        initQueue( Queues[i]);
    }        
}

/**
 * Connect timers to state machines.
 */
void initializeTimers()
{    
    timer_initialize();
    // pulse detect
    timer_SetPostFunction(PULSE_DET_0, RunPulseDetectFSM);
    timer_SetPostFunction(PULSE_DET_1, RunPulseDetectFSM);
    timer_SetPostFunction(PULSE_DET_2, RunPulseDetectFSM);
    timer_SetPostFunction(PULSE_DET_3, RunPulseDetectFSM);
    timer_SetPostFunction(PULSE_DET_4, RunPulseDetectFSM);
    timer_SetPostFunction(PULSE_DET_5, RunPulseDetectFSM);    
    // reg read
    timer_SetPostFunction(REG_READ_TIMER, RunReadRegsFSM);
    // SM Bus    
    timer_SetPostFunction(SMBUS_TIMER, RunSMBusFSM);
    // Master FSM
    timer_SetPostFunction(MONITOR_TIMER, &RunMasterFSM);
    timer_SetPostFunction(SM_WATCHDOG, &RunMasterFSM);
    // indicators
    timer_SetPostFunction(IND_0_TMR, RunIndicatorFSM);
    timer_SetPostFunction(IND_1_TMR, RunIndicatorFSM);
    timer_SetPostFunction(IND_2_TMR, RunIndicatorFSM);   
    timer_SetPostFunction(SHIP_MODE, RunShipmentModeFSM);
}
/**
 * post to all event queues
 * @param event
 * @return 
 */
bool SM_PostAll(event_t event)
{
    for(uint8_t i = 0; i < NUM_QUEUES; ++i){
        EnqueueFIFO(Queues[i], event);
    }    
    
    return true;
}

/**
 * List of functions called to check hardware events.
 */
static CheckFunc *const ES_EventList[] = {
  EVENT_CHECK_LIST
};

/**
 * SM_CheckUserEvents checks for events from the SM_Bus or communications.
 * Review the definition of the EVENT_CHECK_LIST, which is a list of 
 * function pointers to functions that check for specific events.
 * @return true if an event is created, false otherwise
 */
bool SM_CheckUserEvents()
{
#ifdef CHECK_EVENT_STATS
    updateEventStatsTiming(&eventStats);
#endif
    
    uint8_t i;
  // loop through the array executing the event checking functions
  for (i = 0; i < ARRAY_SIZE(ES_EventList); i++)
  {
    if (ES_EventList[i]() == true)
    {
      break; // found a new event, so process it first
    }
  }
  if (i == ARRAY_SIZE(ES_EventList)){   // if no new events  
    return false;
  } else {      
    return true;
  }
}

/**
 * Description Runs the state machines that events, then checks for hardware
 * events which will add new events to the queues.*
 */
void SM_Run(void) {
    
    // enable the hardware watchdog
    WWDT_SoftEnable(); 
    // static to improve speed    
    static event_t ThisEvent;
        
    while (1) // stay here unless we detect an error condition
    { 
        
        // loop through the list executing the run functions for services        
        // with a non-empty queue. Process any pending ints before testing
        // Ready
        while ((processPendingInts())) {          
                                    
            for (uint8_t i = 0; i < NUM_QUEUES; ++i) {               
                 while (Queues[i]->info.numEntries > 0) {
                    if(Queues[i]->info.numEntries > MaxQueueEvents){
                        (MaxQueueEvents = Queues[i]->info.numEntries);
                    }
                    Dequeue(Queues[i], &ThisEvent);                    
                    switch (i) {
                        case 0:                                                          
                            RunMasterFSM(ThisEvent);                            
                            break;
                        case 1:                              
                            RunSMBusFSM(ThisEvent);
                            break;
                        case 2:                              
                            RunReadRegsFSM(ThisEvent);
                            break;
                        case 3:                              
                            RunIndicatorFSM(ThisEvent);
                            break;
                        case 4:                              
                            RunPulseDetectFSM(ThisEvent);
                            break;                            
                        case 5:
                            RunShipmentModeFSM(ThisEvent);
                            break;
                    } // end of switch                    
                } // end of while                 
            } // end of loop         

            // check for new events            
            SM_CheckUserEvents();
        } // end of while               
    }
}


#ifdef SM_MAIN_DEBUG
/**
 * Used for debugging. #ifdef out for production
 * @param queue to log
 * @param QueueName 
 */
void LogQueueEvents(queue_t* queue, char* QueueName)
{
    event_t pEvent;
    uint8_t i, j;
      
    DBG_printf("Queue:%s has %u entries\r\n", QueueName, queue->info.numEntries);
        
    for( i = 0, j = queue->info.currentIndex ; i < queue->info.numEntries; ++i){
        pEvent = queue->events[j];
        DBG_printf("Queue:%s Event Type: %s Event Data: %u\r\n", QueueName, GetEventName(pEvent.EventType), pEvent.data);
        
        if(++j >= queue->info.queueSize){
            j = pQueue->info.currentIndex % queue->info.queueSize;
        }
    }
    
}
/**
 * Used for debugging. #ifdef out for production
 * @param queue
 * @param QueueName
 */
void LogAllQueues()
{    
    for(uint8_t i = 0; i < 5; ++i){
        LogQueueEvents(Queues[i], QueueNames[i]);
    }           
}



/**
 * For diagnostics. #ifdef out for production code
 */
#ifdef SM_LOG_MACHINE_STATE
static char* masterStateNames[] = { "Master_InitPState", "CycleLEDS_Start",
    "C_LEDS_PlugInd_Green","C_LEDS_BatteryCap_Yellow", "C_LEDS_BatteryCap_Red",
    "C_LEDS_BF_Yellow", "C_LEDS_BF_Red", "CycleLEDS_Complete", "Listening2Bus", "ReadingRegisters",
    "WritingRegisters"};
 
    static char* readRegistersNames[] = {"RR_InitPState", "RR_Idle", "Writing2Mux",
            "Listening", "ReadingRegs", "WritingRegs"};
            
    static char* SMBusMasterStateNames[] = {"InitPState", "SMBusIdle", "SendingStart", 
            "SendingAddr", "SendingAddrR", "SendingCommand", "SendingBytes", "SendingRepStart",
            "ReceivingBytes", "SendingAckStat", "SendingStop", "Waiting4Stop"};
      
void logStateMachinesState()
{
  
    
  
    MasterState_t masterState = QueryMasterFSM();
    ReadRegState_t readRegistersState = QueryReadRegsSM();    SMBusState_t smbusState = QuerySMBusFSM();
    SMBusState_t smBusMasterState = QuerySMBusFSM();    
    
    DBG_printf("STATES Master: %s\r\n", masterStateNames[masterState]); 
    DBG_printf("STATES ReadRegisters: %s\r\n", readRegistersNames[readRegistersState]); 
    DBG_printf("STATES SMBusMaster: %s\r\n", SMBusMasterStateNames[smBusMasterState]);            
}

#else // SM_LOG_MACHINE_STATE
    void logStateMachinesState(){};
#endif

void logAll()
{
    logStateMachinesState();
    LogAllQueues();
    //LogActiveTimers();
}

#else
void logAll(){};
#endif