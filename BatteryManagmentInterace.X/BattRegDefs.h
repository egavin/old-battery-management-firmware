/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Author(s): rmerchant
** Purpose: constants related to SMbus battery registers
****************************************************************************/

#ifndef BATTREGDEFS_H
#define	BATTREGDEFS_H

#ifdef	__cplusplus
extern "C" {
#endif

#define MANU_ACCESS 0x00
#define REM_CAP_ALARM 0x01
#define REM_TIME_ALARM 0x02
#define BATT_MODE 0x03
#define AT_RATE 0x04
#define AT_RATE_T2F 0x05
#define AT_RATE_T2E 0x06
#define AT_RATE_OK 0x07
#define TEMP_REG 0x08
#define VOLTAGE 0x09
#define CURRENT 0x0a
#define AVG_CURRENT 0x0b
#define MAX_ERROR 0x0c
#define REL_CHRG_REG 0x0d
#define ABS_CHRG_REG 0x0e
#define REM_CAP 0x0f
#define FULL_CHRG_CAP 0x10
#define RUN_TIME_2E 0x11
#define AVG_TIME_2E 0x12
#define AVG_TIME_2F 0x13
#define CHRG_CURRENT 0x14
#define CHRG_VOLTAGE 0X15
#define STATUS_REG 0x16
#define CYCLE_CNT 0x17
#define DESIGN_CAP 0x18
#define DESIGN_VOLTAGE 0x19
#define SPEC_INFO 0x1a
#define MANU_DATE 0x1b
#define SERIAL_NUM 0X1c
#ifdef	__cplusplus
}
#endif

#endif	/* BATTREGDEFS_H */

