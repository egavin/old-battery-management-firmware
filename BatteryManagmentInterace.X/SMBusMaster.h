/****************************************************************************
 ** Copyright (c) 2021 Supira Medical
 ** Authors: rmerchant, Ed Gavin
 ** Purpose: State machine to read the battery registers
 ****************************************************************************/

#include "sm_events.h"
#include "sm_queue.h"

#ifndef SMBUSMASTER_H
#define	SMBUSMASTER_H

#define MAX_CMD_VAL 0x23

//typedef for errors

typedef enum {
    SMBUS_NO_ERROR = 0, /* All good, process occurred without errors */
    ADDR_NACK = 1, /* NACK received after sending address header */
    CMD_NACK = 2, /* NACK received while trying to write command */
    DATA_NACK = 3, /* NACK received after sending data byte */
    BUS_COLLISION = 4, /* Collision was detected */
    SMBUS_BUSY, /* SMBus is still on last request or line is busy */
    INVALID_ADDR, /* User provided bad address */
    INVALID_CMD, /* User provided bad command */
    INVALID_MODE, /* User provide bad mode */
    SMBUS_TIMEOUT, /* Some timeout occurred because the machine hung */
} SMBusError_t;

// typedefs for the states

typedef enum {
    InitPState, /* Sets up machine during init phase */
    SMBusIdle, /* I2C machine is not active, ready to start */
    SendingStart, /* MSSP is sending out the START condition */
    SendingAddr, /* MSSP is shifting out the first address */
    SendingAddrR, /* MSSP is shifting out the second read address */
    SendingCommand, /* MSSP is shifting out the command byte */
    SendingBytes, /* MSSP is shifting out the SSP buffer */
    SendingRepStart, /* MSSP is performing a repeated START */
    ReceivingBytes, /* MSSP is shifting in byte from slave */
    SendingAckStat, /* MSSP is sending ACKDT bit */
    SendingStop, /* MSSP is sending STOP condition */
    Waiting4Stop /* MSSP is waiting for the line to be free */
} SMBusState_t;

// definitions for the modes

typedef enum {
    WRITE_WORD,
    READ_WORD,
    WRITE_BLOCK,
    READ_BLOCK,
    SEND_BYTE,
    RECV_BYTE,
} SMBusMode_t;

// Prototypes to initiate machine
SMBusError_t SMBus_ReadWriteWord(uint8_t slaveAddr, uint8_t command,
        SMBusMode_t mode, uint8_t *pByteBuff);
SMBusError_t SMBus_SendRecvByte(uint8_t slaveAddr, SMBusMode_t mode,
        uint8_t* pByte);
SMBusError_t SMBus_ReadBlock(uint8_t slaveAddr, uint8_t command,
        uint8_t *pByteBuff);
SMBusError_t SMBus_WriteBlock(uint8_t slaveAddr, uint8_t command,
        uint8_t buffSize, uint8_t *pByteBuff);

// Public Function Prototypes for machine
bool InitSMBusFSM(queue_t* pQueue);
bool PostSMBusFSM(event_t ThisEvent);
__reentrant event_t RunSMBusFSM(event_t ThisEvent);
SMBusState_t QuerySMBusFSM(void);
char* GetSMBusErrorString(uint16_t error);

// Event Checkers
bool Check4IntFlag_SMBus(void);
bool Check4BusColl_SMBus(void);
bool Check4Stop_SMBus(void);
void SMBusReset(void);
void I2C_Check(void);

#endif	/* SMBUSMASTERFSM_H */

