/****************************************************************************
 ** Copyright (c) 2020 Supira Medical
 ** Authors: rmerchant, Ed Gavin
 ** Purpose: This code handles PIC Interrupts.
 ****************************************************************************/
#include "sm_main.h" 
#include "sm_port.h"
#include "mcc_generated_files/eusart2.h"
#include <xc.h>
#include "CompComm.h"

/**
 * High priority interrupt handler
 */
void __interrupt(high_priority) myHpIsr(void) {
    // Place high priority interrupts here

    // interrupt handler
    if (PIR4bits.TMR6IF == 1) {
        _HW_SysTickIntHandler();
    }
}

/**
 * Low priority interrupt handler
 */
void __interrupt(low_priority) myLpIsr(void) {
    // Place low priority interrupts here
}


