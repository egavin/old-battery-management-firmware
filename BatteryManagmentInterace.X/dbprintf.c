/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Authors: rmerchant, Ed Gavin
** Purpose: Provides a light weight but limited printf
****************************************************************************/

//#define TEST
#pragma warning disable 1496 
/*----------------------------- Include Files -----------------------------*/
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include "dbprintf.h"


char buffer[32];
char*  GetSecondsMinutesHours()
{        
    uint32_t seconds = getSysSecondCounter();
    uint16_t hours = (uint8_t) (seconds / 3600);
    seconds -= hours * 3600;    
    uint16_t minutes  = (uint16_t) (seconds / 60);    
    seconds = seconds % 60;
    sprintf(buffer, "%2.2u:%2.2u:%2.2u", hours, minutes, (uint16_t)seconds);
    return buffer;
}   

/*----------------------------- Module Defines ----------------------------*/
#define LINE_LEN    60
#define FIELD_LEN   6

#define CR 0x0d
#define LF 0x0a
/*---------------------------- Module Functions ---------------------------*/
static void uitoa(char **LineBuffer, unsigned int i, unsigned int baseN);

/*---------------------------- Module Variables ---------------------------*/
static char FieldBuf[FIELD_LEN + 1];

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     DB_printf

 Parameters
     a char * format string, followed by a variable number of arguments

 Returns
     None.

 Description
     a printf() like function that has been
     stripped down to reduce its code size & memory usage.  The only format
     specifiers  recognized are : %d, %x, %u, %c, %s . It can not print
     floats or longs. If it is called with a format specifier other than
     those recognized, it will print BAD. Any values after that are garbage.
     Two macros are provided in the header file to allow printing the upper &
     lower halves of longs. Floats must be explicitly  cast to int before
     printing.

****************************************************************************/
void DB_printf(const char *Format, ...)
{    
  va_list Arguments;
   char *pBuffer,
        *pString;
   int   i;
	unsigned int u;
   char  LineBuffer[LINE_LEN+1];

   va_start(Arguments,Format);
   pBuffer = LineBuffer;
   *pBuffer = 0;                 /* make sure that Line starts out NULL term */
   while (*Format)               /* step through the format string */
      if (*Format != '%')            /* if not a format specifier */
            *pBuffer++ = *Format++;  /* simply copy to the output buffer */
      else
      {
         switch (*++Format)         /* otherwise see what kind of format spec */
         {
            case 'd':               /* %d, decimal signed number */
               i = va_arg(Arguments,int);
               if (i < 0)
               {
                  *pBuffer++ = '-'; /* add '-' to the buffer for neg. numbers */
                  i = -1*i;         /* and continue with the positive version */
               }
               uitoa(&pBuffer, (unsigned int)i, 10);
               break;
            case 'x':               /* %x, hexadecimal unsigned number */
               u = va_arg(Arguments,unsigned int);
//               *pBuffer++ = '0'; removed to allow cleaner printing of longs
//               *pBuffer++ = 'x';
               uitoa(&pBuffer, u, 16);
               break;
            case 'u':               /* %u, decimal unsigned number */
               u = va_arg(Arguments,unsigned int);
               uitoa(&pBuffer, u, 10);
               break;
            case 'c':               /* %c, a single character */
               *pBuffer++ = (char) va_arg(Arguments,unsigned int);
               break;
            case 's':               /* %s, a string of characters */
               pString = va_arg(Arguments,char *);
               if (!pString)
                  pString = "(null)";
               while (*pString)
                  *pBuffer++ = *pString++;
               break;
            case '%':               /* quoted % */
               *pBuffer++ = '%';
                break;
            default:                /* anything else is a bad spec. */
                *pBuffer++ = 'B';
                *pBuffer++ = 'A';
                *pBuffer++ = 'D';
                break;
         }
         Format++;
      }
   *pBuffer = 0;                     /* null terminate the output string */

/* now, spit the built up line out 1 character at a time */
   for (pBuffer = LineBuffer; *pBuffer != 0; pBuffer++)
   {
      if (*pBuffer != '\n')
         putch(*pBuffer);
      else
      {
         putch(CR);
         putch(LF);
      }
   }
   return;
}
/* integer to ascii conversion for unsigned numbers */
static void uitoa(char **LineBuffer, unsigned int i, unsigned int baseN)
{
   char *s;
   unsigned int   rem;

   FieldBuf[FIELD_LEN] = 0;      /*start by NULL terminating the local buffer */
   if (i == 0)
   {
      (*LineBuffer)[0] = '0';
      ++(*LineBuffer);
      return;
   }
   s = &FieldBuf[FIELD_LEN];
   while (i)
   {
      rem = i % baseN;
      if (rem < 10)
         *--s = (char)rem + '0';
      else
      if (baseN == 16)
         *--s = "abcdef"[rem - 10];
      i /= baseN;
   }
   while (*s)                    /* copy local buffer into passed buffer */
   {
      (*LineBuffer)[0] = *s++;
      ++(*LineBuffer);
   }
}


#ifdef TEST
#define LONGTEST
#include <limits.h>
//#include <hc11defs.h>
#include <SC1_comm.h>

#define UINT_MIN 0
#define UCHAR_MIN 0

void main(void)
{
#ifdef LONGTEST
   unsigned char ucl=UCHAR_MIN;
   unsigned char uch=UCHAR_MAX;
   unsigned int  uil=UINT_MIN;
   unsigned int  uih=UINT_MAX;
   int   il = INT_MIN;
   int   ih = INT_MAX;
   signed char  cl = SCHAR_MIN;
   signed char  ch = SCHAR_MAX;
   char  c='A';
   char  String[]="Hello World\n";
   float Floater = 1.23;
   unsigned long LongOne = 100000L;

   Init_SC1(BAUD38400);

   DB_printf("Beginning DB_printf() test:\n");

   DB_printf("Printing an unsigned char UCHAR_MIN, Decimal mode(%%u): %u\n",ucl);
   DB_printf("Printing an unsigned char UCHAR_MIN, Hex mode: %x\n",ucl);

   DB_printf("Printing an unsigned char UCHAR_MAX, Decimal mode(%%u): %u\n",uch);
   DB_printf("Printing an unsigned char UCHAR_MAX, Hex mode: %x\n",uch);

   DB_printf("Printing an unsigned int UINT_MIN, Decimal mode(%%u): %u\n",uil);
   DB_printf("Printing an unsigned int UINT_MIN, Hex mode: %x\n",uil);

   DB_printf("Printing an unsigned int UINT_MAX, Decimal mode(%%u): %u\n",uih);
   DB_printf("Printing an unsigned int UIINT_MAX, Hex mode: %x\n",uih);

   DB_printf("Printing an signed char SCHAR_MIN, Decimal mode(%%d): %d\n",cl);
   DB_printf("Printing an signed char SCHAR_MAX, Decimal mode(%%d): %d\n",ch);
   DB_printf("Printing an signed int INT_MIN, Decimal mode(%%d): %d\n",il);
   DB_printf("Printing an signed int INT_MAX, Decimal mode(%%d): %d\n",ih);

   DB_printf("Printing a char as a single character: %c\n", c);
   DB_printf("Printing a string w/ embedded NL: %s\n", String);

   DB_printf("Attempting to print a long: %ld\n",LongOne);
   DB_printf("Attempting to print a float: %f\n",Floater);
   DB_printf("A way to print a long value: %x%x\n", HIWORD(LongOne),LOWORD(LongOne));

#else
   Init_SC1(BAUD38400);
   DB_printf("Hello World!\n");
#endif

/* -----------------2/22/98 11:57PM------------------
Note: without out the following line, you may not be able to re-load after
 running your code without a reset. The reason is that the start-up code
 starts an interrupt response that is left running when your code exits.
 --------------------------------------------------*/
//disable();
}
#endif

