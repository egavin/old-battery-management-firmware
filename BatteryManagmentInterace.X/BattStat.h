/****************************************************************************
** Copyright (c) 2021 Supira Medical
**  Author(s): rmerchant, Ed Gavin
** Purpose:  constants and function prototypes for battery statistics.
****************************************************************************/

#ifndef BATTSTAT_H
#define	BATTSTAT_H
#include "PulseDetectFSM.h"

#ifdef	__cplusplus
extern "C" {
#endif
#include <stdint.h>
  
// Battery charge capacity limit
#define BATT_CHARGE_CAPACITY_LIMIT 70
    
// Define a list of the possible errors that can occur 
typedef enum
{
  BATT_NO_ERROR = 0,
  NOT_INIT,             // Battery hardware not initialized
  OVER_TEMP,            // Battery above temperature alarm value
  BAD_TEMP,             // Below -20C or above 80C while operating
  BAD_CHRG_TEMP,        // Below 10C or above 40C while charging
  LOW_CAP,              // Capacity below capacity alarm value
  GPIO2_CHARGER_ERR,    // GPIO2 Charger experienced an error          
  OVER_CHARGE,          // Battery is over charged
  BATT_NOT_ON_BUS,      // Unable to address battery on the SMBus
  BATT_CAPACITY_LOW,    // battery capacity is below 70%        
  SYS_BATT_FAILURE,     // Unused currently
  SYS_MUX_FAILURE,      // SMBus mux failed to respond on SMBus
 BATT_UNKNOWN_ERROR,     // Error during read registers
  TOTAL_BATTERY_ERRORS,
}BattError_t;

typedef struct  GPIOStatus
{
    unsigned gpio1_error :1;
    unsigned gpio2_error :1;
    unsigned psu :1;
    unsigned capacityLow :1;    
    PinInterp_t gpio1_state;
    PinInterp_t gpio2_state;
}gpioStatus_t;

// define structure to handle battery status data
typedef struct BattStatus
{
  unsigned overChrgAlarm :1;  // Indicates battery over charged
  unsigned termChrgAlarm :1;  // Indicated battery requested a charge stop 
  unsigned overTempAlarm :1;  // Indicates battery too hot 
  unsigned remCapAlarm :1;    // Indicates battery is critically low
  unsigned initialized :1;    // Indicates battery hardware is up 
  unsigned discharge :1;      // true if battery is discharging, false if charging
  uint8_t relCharge;          // Stores percentage charge of the battery 
  uint16_t runTimeToEmpty;    // Run time to Empty in Minutes
  uint8_t absChargePercent;   // Absolute State of Charge percentage
  uint8_t  capacityRatio;      // percentage of battery life left
  uint16_t capacity_ma;           // battery capacity in mA/Hr  
  uint16_t tempK;             // Stores temperature in 0.1K (val/10 = K) 
  BattError_t error;          // Stores battery error state 
  gpioStatus_t gpioStatus;    // holds GPIO errors, capacity flag, comm error 
}BattStatus_t;


// Bits in the status word, see 5.1.21 iv SB Data Spec
#define OVER_CHRG_BIT   0x8000
#define TERM_CHRG_BIT   0x4000
#define OVER_TEMP_BIT   0x1000
#define REM_CAP_BIT     0x0200
#define INIT_BIT        0x0080
#define DISCHARGE_BIT   0x0040
// shifts to convert to bit flag
#define OVER_CHRG_S 15
#define TERM_CHRG_S 14
#define OVER_TEMP_S 12
#define REM_CAP_S   9
#define INIT_S      7
#define DISCHARGE_S 6

// temperature constants
#define MAX_BATT_TEMP 3531 // +80 deg C
#define MIN_BATT_TEMP 2532 // -20 deg C
#define MAX_CHRG_TEMP 3131 // +40 deg C
#define MIN_CHRG_TEMP 2932 // +10 deg C

// battery capacity
// the battery design capacity is 6900 mAh. We divide that by 100 so we
// get an integer percentage from absolute capacity / (design capacity /100)
#define BATTERY_DESIGN_CAPACITY_DIV  69  

#define NUM_BATTS 3 // specified number of batteries on the bus
#if (NUM_BATTS > 3) || (NUM_BATTS < 1)
#error Invalid battery numner
#endif

// Public Function Prototypes
BattError_t BattStat_Update(uint8_t num, 
                            uint16_t capWord,  
                            uint16_t tempWord, 
                            uint16_t statusWord, 
                            uint16_t RunTimeToEmpty,
                            uint16_t PercentBatteryCapacity);                            


void SetGPIO1State(uint8_t battNum, uint8_t state);
void SetGPIO2State(uint8_t battNum, uint8_t state);
void SetPSUState(uint8_t battNum, uint8_t psu);
uint8_t IsPsuAttached(void);
void SetGPIOState(uint8_t batteryNum, gpioStatus_t gpioState);
gpioStatus_t GetGPIOStatus(uint8_t batteryNum);
uint8_t IsGPIOError(void);
void BattState_SetError(uint8_t battNum, BattError_t error);
BattError_t BattStat_QueryError(uint8_t battNum);
BattError_t BattStat_QueryErrorAllBatteries(void);

void BattStat_SetOffBus(uint8_t battNum);
uint8_t BattStat_QueryCharge(void);
uint16_t BattStat_QueryTemp(void);
uint8_t BattState_QueryBatteryCharge(uint8_t battery);
uint16_t BattState_QueryTemp(uint8_t battery);
uint16_t BattState_QueryMinimumTimeToRun(void);
uint8_t BattState_QueryAbsChargePercent(uint8_t battery);
uint8_t BattState_QueryBatteryMinAbsChargePercent(void);
uint16_t BattStat_QueryMaCap(uint8_t battery);
#ifdef	__cplusplus
}
#endif

#endif	/* BATTSTAT_H */

