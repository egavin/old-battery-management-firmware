/****************************************************************************
 ** Copyright (c) 2021 Supira Medical
 ** Author(s): rmerchant, Ed Gavin
 ** Purpose: Module for battery status
 ****************************************************************************/
#include "PulseDetectFSM.h"
#include "BattStat.h"
#include <stdint.h>

// Array to hold statuses of all batteries in unit
static BattStatus_t indBattStat[NUM_BATTS];

/**
 * description a function for finding the minimum of two 16 bit values.
 * @param a uint16_t
 * @param b uint16_t 
 * @return the minimum of a or b for a unit16_t type. 
 */
uint16_t minimum(uint16_t a, uint16_t b) {
    return a < b ? a : b;
}

/**
 * updates the battery statistics and alarm
 * @param uint8_t batteryNum battery number being updated
 * @param uint16_t relativeCharge 0:100 percent
 * @param uint16_t temperature battery temperature in Kelvin
 * @param uint16_t statusWord battery status
 * @param uint16_t RunTimeToEmpty 
 */
BattError_t BattStat_Update(uint8_t batteryNum,
        uint16_t relativeCharge,
        uint16_t temperature,
        uint16_t statusWord,
        uint16_t RunTimeToEmpty,
        uint16_t AbsChargePercent) {
    // assume no errors on call
    BattError_t battError = BATT_NO_ERROR;

    indBattStat[batteryNum].discharge = (statusWord & DISCHARGE_BIT) >> DISCHARGE_S;
    // load temperature and charge
    indBattStat[batteryNum].tempK = temperature;
    indBattStat[batteryNum].relCharge = (uint8_t) relativeCharge;
    indBattStat[batteryNum].runTimeToEmpty = RunTimeToEmpty;
    indBattStat[batteryNum].capacity_ma = AbsChargePercent;
    indBattStat[batteryNum].absChargePercent = (uint8_t) (AbsChargePercent / BATTERY_DESIGN_CAPACITY_DIV);
    indBattStat[batteryNum].error = battError;
    return battError;
}

/**
 * Set the GPIO1 error state
 * @param batteryNum 0:2
 * @param state true for error, false for no error
 */
void SetGPIO1State(uint8_t batteryNum, uint8_t state) {
    indBattStat[batteryNum].gpioStatus.gpio1_error = state;
}

/**
 * Set the GPIO2 error state
 * @param batteryNum
 * @param state Set the gpio 2 error state
 */
void SetGPIO2State(uint8_t batteryNum, uint8_t state) {
    indBattStat[batteryNum].gpioStatus.gpio2_error = state;
}

/**
 * Set the GPIO Status
 * @param batteryNum
 * @param gpioStatus
 */
void SetGPIOState(uint8_t batteryNum, gpioStatus_t gpioStatus) {
    indBattStat[batteryNum].gpioStatus = gpioStatus;
}

/**
 * Get the GPIO State from the battery stats
 * @param batteryNum
 * @return 
 */
gpioStatus_t GetGPIOState(uint8_t batteryNum) {
    return indBattStat[batteryNum].gpioStatus;
}

/**
 * Sets the state of the battery charger for this battery
 * @param batteryNum
 * @param psu
 */
void SetPSUState(uint8_t batteryNum, uint8_t psu) {
    indBattStat[batteryNum].gpioStatus.psu = psu;
}

/**
 * @return 0 if there is no error, 1 if there is
 */
uint8_t IsGPIOError() {
    uint8_t gpioError = 0;
    for (uint8_t i = 0; i < NUM_BATTS; ++i) {
        if (indBattStat[i].gpioStatus.gpio1_error ||
                indBattStat[i].gpioStatus.gpio2_error) {
            gpioError = 1;
            break;
        }
    }
    return gpioError;
}

/**
 * If any PSU is attached, return true. We added the battery error
 * check because if the data cable from the charger to the main board is
 * removed, GPIO floats high so it looks like a PSU is attached.
 * @return true if a PSU is attached, false otherwise
 */
uint8_t IsPsuAttached() {
    uint8_t attached = 0;
    for (uint8_t i = 0; i < NUM_BATTS; ++i) {
        if (indBattStat[i].gpioStatus.psu && indBattStat[i].error == BATT_NO_ERROR) {
            attached = 1;
            break;
        }
    }
    return attached;
}

/**
 * Description: returns the gpio status of this battery.
 * @param batteryNum
 * @return gpioStatus
 */
gpioStatus_t GetGPIOStatus(uint8_t batteryNum) {
    return indBattStat[batteryNum].gpioStatus;
}

/**
 * Sets the error  - used for errors such as battery not on bus, Mux error, etc
 * @param battNum battery number 0:2
 * @param error value to set
 */
void BattState_SetError(uint8_t battNum, BattError_t error) {
    if (battNum < NUM_BATTS) {
        indBattStat[battNum].error = error;
    }
}

/**
 * @param battNum battery to query (0:2)
 * @return BattError_t battery error status 
 */
BattError_t BattStat_QueryError(uint8_t battNum) {
    return indBattStat[battNum].error;

}

/**
 * returns an error from the first battery with an error
 * @return battery error
 */
BattError_t BattStat_QueryErrorAllBatteries() {
    BattError_t retError = BATT_NO_ERROR;
    for (uint8_t i = 0; i < 3; ++i) {
        if (indBattStat[i].error != BATT_NO_ERROR && indBattStat[i].error > retError) {
            retError = indBattStat[i].error;
        }
    }
    return retError;
}

/**
 * 
 * @param battNum battery number to set as off of the bus
 */
void BattStat_SetOffBus(uint8_t battNum) {
    indBattStat[battNum].error = BATT_NOT_ON_BUS;
}

/** 
 * @return returns the average change of the batteries as a percentage
 */
uint8_t BattStat_QueryCharge(void) {
    uint16_t sum16 = 0;
    //start loop to sum
    uint8_t i = 0;
    for (i = 0; i < NUM_BATTS; i++) {
        sum16 += indBattStat[i].relCharge;
    }
    // divide the sum, cast to 8 bit
    uint8_t avg = (uint8_t) (sum16 / NUM_BATTS);
    // return the average
    return avg;
}

/**
 * @return returns the average battery temperature
 */
uint16_t BattStat_QueryTemp(void) {
    uint32_t sum32 = 0;
    //start loop to sum  
    for (uint8_t i = 0; i < NUM_BATTS; i++) {
        sum32 += indBattStat[i].tempK;
    }
    // divide the sum, cast to 8 bit
    uint16_t avg = (uint16_t) (sum32 / NUM_BATTS);
    // return the average
    return avg;
}

/**
 * returns the batteries percent change
 * @param battery (0:2)
 * @return returns the charge of a battery
 */
uint8_t BattState_QueryBatteryCharge(uint8_t battery) {
    return indBattStat[battery].relCharge;
}

/**
 * return the battery temperature in degrees K
 * @param battery (0:2)
 * @return the temperature of a battery in degrees Kelvin
 */
uint16_t BattState_QueryTemp(uint8_t battery) {
    return indBattStat[battery].tempK;
}

/**
 * Returns the minimum run time of the batteries.
 * @return 
 */
uint16_t BattState_QueryMinimumTimeToRun() {
    uint16_t minTime = 0;
    minTime = minimum(indBattStat[0].runTimeToEmpty, indBattStat[1].runTimeToEmpty);
    minTime = minimum(minTime, indBattStat[2].runTimeToEmpty);
    return minTime;
}

/** 
 * @param battery (0:2)
 * @return percent of absolute charge capacity remaining.
 */
uint8_t BattState_QueryAbsChargePercent(uint8_t battery) {
    return (uint8_t) indBattStat[battery].absChargePercent;
}

uint16_t BattStat_QueryMaCap(uint8_t battery)
{
    return indBattStat[battery].capacity_ma;
}
/**
 * Returns the battery capacity ratio of a battery
 * @param battery 
 * @return capacity ratio
 */
uint8_t BattState_QueryBatteryMinAbsChargePercent() {
    uint8_t temp;
    temp = (uint8_t) minimum(indBattStat[0].absChargePercent, indBattStat[1].absChargePercent);
    temp = (uint8_t) minimum(temp, indBattStat[2].absChargePercent);
    return temp;
}

