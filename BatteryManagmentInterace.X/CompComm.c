/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Authors: rmerchant, Ed Gavin
** Purpose
** Module holds the functions that support packet communication to the PC
** over the EUSART1 module of the PIC
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
#include <xc.h>
#include <stdio.h>
#include <string.h>
#include "mcc_generated_files\eusart2.h"
#include "sm_port.h"
#include "bitdefs.h"

// This module
#include "CompComm.h"
#include "sm_main.h"
#include "sm_events.h"
#include "sm_queue.h"
#include "sm_main.h"
#define DEBUG_PRINT_ENABLED
#include "dbprintf.h"

/*----------------------------- Module Defines ----------------------------*/
typedef struct CommFlags
{
  unsigned txReady      :1;
  unsigned txBuffEmpty  :1;
  unsigned rxDone       :1;
  unsigned rxBuffFull   :1;
  unsigned rxError      :1;
  
}CommFlags_t;

#define START_DELIM '$' // 36d
#define STOP_DELIM '\r' // 13d  
#define BATT_MANGM 0x08
#define INTERFACE_V 0x01 // clinical
#define PACKET_SIZE 0x22

//#define DEBUG_PRINT // uses printf, not debug pin

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
static void WriteToCommPort(void);
static void Copy2TxISR(void);
static void AsciiEncode(uint8_t** pDataIn, uint8_t bytes);
static bool AsciiDecode(uint8_t** pData);
static uint8_t ASCII2uint4(char letter);
/*---------------------------- Module Variables ---------------------------*/
// Flags
static volatile CommFlags_t CommStatus ={1, 1, 1, 0, 0}; 
// Variables associated with transmission
static uint8_t txBuffer[MAX_PACKET_SIZE*2];
static volatile uint8_t txISRBuffer[MAX_PACKET_SIZE*2];
static volatile uint8_t txCnt;
// Variables associated with reception
static volatile uint8_t rxISRBuffer[MAX_PACKET_SIZE*2];
static volatile uint8_t rxCnt;
// character map for ASCII encoding
static const char hexMap[16] = "0123456789ABCDEF";
// variables for outside modules to query
static bool compDataReady; 
/*------------------------------ Module Code ------------------------------*/
/**
 * @function CompCom_HWInit
 * @description Sets up the PIC communications to use USART1 to communicate
 * with the PC host.
 */
void CompComm_HWInit(void) {

/*******************************************************    
 * EUSART2_Initialize takes care of initializing UART2
 */
    // this is already called by MCC, but this insures that
    // some other part of the code didn't modify the settings.sd
    EUSART2_Initialize(); 
 /**********************************/ 

  return;
}


/**
 * @function CompComm_Write
 * @param pData data to write. 
 * @return true if successful, false if the tx buffer isn't empty.
 */
bool CompComm_Write(uint8_t* pData, uint8_t bytesToWrite)
{
  bool status = false;
  
  // If the buffer is empty and size is appropriate  
  if((CommStatus.txBuffEmpty) && (MAX_PACKET_SIZE > *pData))
  {
    // call ASCII encoding function
    AsciiEncode(&pData, bytesToWrite);
    // clear buffer empty flag
    CommStatus.txBuffEmpty = 0;
    // If EUSART is ready to transmit
    if(CommStatus.txReady)
    {
        WriteToCommPort();      
    }
    // successful buffer placement, set status
    status = true;
  }
  return status;
}

/**
 * Retrieves the bytes captures by EUSART1 reception. Calling this
 * function will disable the receive the reception interrupt so
 * use this function with caution. 
 * @param pData pointer to array of bytes to read
 * @return 
 */
#ifdef PC_SERIAL_INPUT_ENABLED
bool CompComm_Read(uint8_t* pData)
{
  bool status = false;
  // Check to see if there is anything to actually read
  if(CommStatus.rxBuffFull & (~CommStatus.rxError))
  {
    // disable Rx interrupts
    PIE3bits.RC1IE = 0;
    // set status
    status = AsciiDecode(&pData);
    // set rxCnt back to 0
    rxCnt = 0;
    // clear buffer full flag
    CommStatus.rxBuffFull = 0;
    // re-enable interrupts
    PIE3bits.RC1IE = 1;
  }
  return status;
}
#endif 

/**
 * 
 * @return true if there is data waiting, false otherwise
 */
#ifdef PC_SERIAL_INPUT_ENABLED
bool CompComm_PCDataReady(void)
{
  // see if there is indeed data
  if(compDataReady)
  {  
    compDataReady = false;
    return true;    
  } else {
    return false;
  }
}
#endif
/**
 * Determines whether there is indeed information from the PC that
 * is ready to process. Flag is cleared after it is sent. 
 * @return 
 */
bool Check4RcBuffFull(void)
{
  // if the buffer is full and we had valid reception
  if(CommStatus.rxBuffFull & CommStatus.rxDone & (~CommStatus.rxError))
  {
    compDataReady = true;
  }
  // We are using this as a psuedo event checker. There is no posting done
  // here so ALWAYS RETURN FALSE!!!
  return false; 
}

/**
 * Description: Checks to see if the Tx buffer is full and transmission is ready
 * @return returns falose
 */
bool Check4TxBuffFull(void)
{
  // if the Tx buffer is full and we have another packet ready to go
  if((~CommStatus.txBuffEmpty) & CommStatus.txReady)
  {
    WriteToCommPort();
  }
  // We are using this as a psuedo event checker. There is no posting done
  // here so ALWAYS RETURN FALSE!!!
  return false; 
}

/**
 * WriteToCommPort uses the MCC library to handle writing data to the port. 
 */
static void WriteToCommPort()
{    
  bool valid = true;
  uint8_t i = 0;
  while(valid)
  {
      EUSART2_Write(txBuffer[i]);
      valid = (txBuffer[i] !='\r');    
      i++;
  }      
  CommStatus.txBuffEmpty = 1;  //set the buffer empty flag  
  txCnt = 0;       // set txCnt to 0
}

/**
 * AsciiEncode
 * @param pData pointer to byte pointer
 * @param bytesToWrite
 */
static void AsciiEncode(uint8_t** pData, uint8_t bytesToWrite)
{
    // clear the transmit buffer
    memset(txBuffer, 0, sizeof(txBuffer)/sizeof(txBuffer[0]));
  // get the size, it's the first byte
  uint8_t size = bytesToWrite + 1; //(uint8_t)((**pData) << 1) + 1;
  // place start delim into buffer
  txBuffer[0] = START_DELIM;
  // place interface version
  txBuffer[1] = hexMap[INTERFACE_V >> 4];
  txBuffer[2] = hexMap[INTERFACE_V & 0x0f];
  // place payload type
  txBuffer[3] = hexMap[BATT_MANGM >> 4];
  txBuffer[4] = hexMap[BATT_MANGM & 0x0f];
  
  // start loop to convert bytes into the ascii encoding
  uint8_t i = 0;
  for(i=0; i<size; i+=2)
  {      
    // send higher nibble first
    txBuffer[i+5] = hexMap[**pData >> 4];
    // send lower nibble ahead 1
    txBuffer[i+6] = hexMap[**pData & 0x0f];
      
    // increment to next byte
    (*pData)++;
  }
  // place stop delim at end
  txBuffer[i+5] = STOP_DELIM;
  txBuffer[i+6] = 0;  
}

/**
 * AsciiDecode - decodes an ASSCII string from the PC
 * @param pData pointer to byte pointer
 * @return returns false
 */

#ifdef  PC_SERIAL_INPUT_ENABLED
static bool AsciiDecode(uint8_t** pData)
{
  // return variable
  bool status = false;
  // flag to know we have reached the stop delim
  bool valid = true;
  // look at index 4 and see if is correct
  if(ASCII2uint4((char)rxISRBuffer[4]) == BATT_MANGM)
  {
    // set status to true
    status = true;
    // counter to know where we are.
    uint8_t cnt = 5; //Start at 5 since we will skip to the first payload byte
    // declare some place holders
    uint8_t hiNibble, loNibble;
    // start loop to look at groups
    do
    {
      // get the value pair
      hiNibble = rxISRBuffer[cnt];
      loNibble = rxISRBuffer[cnt+1];
      // convert to actual number nibbles
      hiNibble = ASCII2uint4((char)hiNibble);
      loNibble = ASCII2uint4((char)loNibble);
      // convert to byte and store to array
      **pData = (uint8_t)((hiNibble << 4) + loNibble);
      // check what the next value is
      if(rxISRBuffer[cnt+2] == '\r')
      {
        valid = false;
      } else {
        // increment count by 2
        cnt += 2;
        // increment pointer
        (*pData)++;
      }
    }
    while(valid);
  }
  
  return status;
}
#endif

/**
 * 
 * @param letter
 * @return unsigned integer
 */
static uint8_t ASCII2uint4(char letter)
{
  // if ASCII 0-9
  if((letter & 0xf0) == 0x30)
  {
    return (letter&0xf);
  }
  // else ASCII A-F
  else 
  {
    return ((letter & 0x0f) + 9);
  }
}

/**
 * checks to see if a new key from the keyboard is detected and, if so,
   retrieves the key and posts an ES_NewKey event to TestHarnessService0
 * @return true if a keystroke is detected, false otherwise
 */
#ifdef  PC_SERIAL_INPUT_ENABLED
bool Check4Keystroke(void)
{
  if (IsNewKeyReady())   // new key waiting?
  {
    event_t ThisEvent;
    ThisEvent.EventType   = ES_NEW_KEY;
    ThisEvent.data  = (uint16_t)GetNewKey();
    SM_PostAll(ThisEvent);
    return true;
  }
  return false;
}
#endif