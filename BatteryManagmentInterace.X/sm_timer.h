/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Author(s): Ed Gavin
** Purpose:  defines and function declarations related to the timer service
****************************************************************************/
#ifndef SM_TIMERS_H
#define	SM_TIMERS_H

// Includes
#include <stdbool.h>
#include <stdint.h>
#include "sm_events.h"

// Enums and Defines
#define NUM_TIMERS 16

// timers, from 0:15
enum {
    PULSE_DET_0,
    PULSE_DET_1,
    PULSE_DET_2,
    PULSE_DET_3,
    PULSE_DET_4,
    PULSE_DET_5,
    IND_0_TMR = 6,
    IND_1_TMR = 7,
    IND_2_TMR = 8,    
    SHIP_MODE = 9,
    SM_WATCHDOG = 10,
    SM_WATCHDOG2 = 11,
    REG_READ_TIMER = 12,
    SMBUS_TIMER = 13,
    MONITOR_TIMER =14,
    TEST_TIMER = 15
};

typedef enum 
{
    TIMER_ERROR = -1,    
    TIMER_OK = 0,     
}timer_status_t;

typedef  enum {
    TMR_ACTIVE =    0x1,
    TMR_ROLLOVER =  0x2
    } TIMER_FLAGS;
    
// function pointer called when timer expires
typedef event_t (*postFunction_t)(event_t);

typedef struct
{
    uint16_t m_time;
    uint8_t m_flags;
    postFunction_t  postFunction;
} timer_struct_t;

void timer_initialize(void);
void timer_ProcessTicks(void);
void timer_SetPostFunction(uint8_t index, event_t (*PostFunction)(event_t));
timer_status_t timer_InitTimer(uint8_t index, uint16_t newTime);
timer_status_t timer_StopTimer(uint8_t index);
uint16_t timer_GetTime(uint8_t index);
#ifdef SM_TIMER_DEBUG
uint8_t GetNumActiveTimers(void);
void LogActiveTimers(void);
#endif

#endif	/* SM_TIMERS_H */

