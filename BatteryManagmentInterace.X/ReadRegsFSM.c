/****************************************************************************
 ** Copyright (c) 2021 Supira Medical
 ** Author(s): RMerchant, Ed Gavin
 ** Purpose:  defines and function declarations related to the ReadRegister 
 * state machine.
 ****************************************************************************/

#include <stdint.h>
#include "sm_queue.h"
#include "sm_timer.h"
#include "sm_events.h"
#include "ReadRegsFSM.h"
#include "SMBusMaster.h"
#include "sm_main.h"
#include "BattStat.h"
#include "MasterFSM.h"
#include <assert.h>

#ifdef READ_REGISTERS_DEBUG
#define DEBUG_PRINT_ENABLED  
#include "dbprintf.h"
//#define TRACE_ERRORS 1
#endif
#ifdef TEST_RETRY_TIME                
#define DEBUG_PRINT_ENABLED  
#include "dbprintf.h"
#endif 

/*----------------------------- Module Defines ----------------------------*/
#define MUX_ADDR 0x70
#define BATT_ADDR 0b1011
#define MAX_MUX_RETRIES 5
#define MAX_REG_RETRIES 3
#define LISTENING_TIME  5
#define TIMEOUT_TIME    20

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
 */
static uint16_t buffer2Word(uint8_t* pBuff);
/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static ReadRegState_t currentState;
// the mux channels available 
static const uint8_t muxChnls[4] = {0x01, 0x02, 0x04, 0x08};
// struct holding the setup
static ReadSetup_t readSetup;
static WriteSetup_t writeSetup;
// pointer to queue for this FSM
static queue_t* pQueue;

/**
 * Function InitReadRegsFSM
 * @param queue_t* qp
 * @return true if the initial event is added to queue, false if not.
 */
bool InitReadRegsFSM(queue_t* qp) {
#ifdef TRACE_ERRORS    
    DBG_printf("***** InitReadRegsFSM() ******\n");
#endif
    event_t ThisEvent;

    pQueue = qp;
    // put us into the Initial PseudoState
    currentState = RR_InitPState;
    // post the initial transition event
    ThisEvent.EventType = SM_INIT;
    return EnqueueFIFO(pQueue, ThisEvent);
}

/**
 * PostReadRegsFSM
 * @description: posts an event to the Read Registers Queue
 * @param event_t ThisEvent
 * @return bool true if posted to queue, false otherwise.
 */
bool PostReadRegsFSM(event_t ThisEvent) {
    return EnqueueFIFO(pQueue, ThisEvent);
}

/**
 * @function RunReadingRegistersFSM
 * @param ThisEvent event to process
 * @return event_t 
 */
event_t RunReadRegsFSM(event_t ThisEvent) {
#ifdef TRACE_OUT    
    DBG_printf("RunReadRegsFSM() %s\n", GetEventName(ThisEvent.EventType));
#endif    
    event_t ReturnEvent;
    ReturnEvent.data = SM_NO_EVENT; // assume no errors
    static uint8_t retries;
    static uint8_t regBytes[2];
    static uint8_t muxChnl;
    static bool Writing;

    switch (currentState) {
        case RR_InitPState: // If current state is initial Psedudo State
#ifdef TRACE_OUT            
            DBG_printf("RunReadRegsFSM(): RR_InitPState\n");
#endif            
        {
            if (ThisEvent.EventType == SM_INIT) // only respond to SM_Init
            {
                // this is where you would put any actions associated with the
                // transition from the initial pseudo-state into the actual
                // initial state
                Writing = false;
                // now put the machine into the actual initial state
                currentState = RR_Idle;
                retries = 0;
            }
        }
            break;
            /* -------------------------------------------------------------- RR_Idle */
        case RR_Idle: // If current state is state one
        {
#ifdef TRACE_OUT            
            DBG_printf("RunReadRegsFSM(): RR_Idle\n");
#endif
            // if event is Start Read
            if (ThisEvent.EventType == RR_START_READ) {
                Writing = false;
#ifdef TRACE_OUT                
                DBG_printf("RunReadRegsFSM(): RR_START_READ\n");
#endif
                // send byte to mux to set the channel
                muxChnl = muxChnls[readSetup.battNum];
                SMBus_SendRecvByte(MUX_ADDR, SEND_BYTE, &muxChnl);
                // transition to Writing2Mux
                currentState = Writing2Mux;

            } else if (ThisEvent.EventType == RR_START_WRITE) {
                Writing = true;
#ifdef TRACE_OUT                
                DBG_printf("RunReadRegsFSM(): RR_START_WRITE\n");
#endif
                // send byte to mux to set the channel
                muxChnl = muxChnls[writeSetup.battNum];
                SMBus_SendRecvByte(MUX_ADDR, SEND_BYTE, &muxChnl);
                // transition to Writing2Mux
                currentState = Writing2Mux;
            }
        }
            break;
            /* ---------------------------------------------------------- Writing2Mux */
        case Writing2Mux: // If current state is state one
        {
#ifdef TRACE_OUT            
            DBG_printf("RunReadRegsFSM(): Writing2Mux\n");
#endif
            // if event is EOT
            if (ThisEvent.EventType == SMBUS_EOT) {
                // if there are no errors
                if (ThisEvent.data == SMBUS_NO_ERROR) {
                    // start the listening timer
                    timer_InitTimer(REG_READ_TIMER, LISTENING_TIME);
                    // transition to listening
                    currentState = Listening;
                    // zero the retry count
                    retries = 0;
                }// else if there is an error and there are retries left
                else if ((ThisEvent.data != SMBUS_NO_ERROR)
                        && (retries < MAX_MUX_RETRIES)) {
#ifdef TRACE_OUT                    
                    DBG_printf("RunReadRegsFSM(): error: %s Writing2Mux\n", GetSMBusErrorString(ThisEvent.data));
#endif              
                    I2C_Check(); // try and free the bus
                    // retry to write to the MUX
                    muxChnl = muxChnls[readSetup.battNum];
                    SMBus_SendRecvByte(MUX_ADDR, SEND_BYTE, &muxChnl);
                    // increment the retry count
                    retries++;
                }// else there was an error and we are out of retries
                else {
                    // abort and post read done with MUX error
#ifdef TRACE_ERRORS                    
                    DBG_printf("RunReadRegsFSM(): Writing2Mux Out of Retries\n");
#endif                    
                    event_t Event2Post;
                    Event2Post.EventType = RR_READ_DONE;
                    Event2Post.data = MUX_ERR;
                    SM_PostAll(Event2Post);
                    // transition back to idle
                    currentState = RR_Idle;
                    // zero the retry count
                    retries = 0;
                }// end if 
            }// end if
        }
            break;
            /* ------------------------------------------------------------ Listening */
        case Listening:
        {
#ifdef TRACE_OUT            
            DBG_printf("RunReadRegsFSM(): Listening\n");
#endif           

            if (ThisEvent.EventType == SM_TIMEOUT) {
                if (Writing) {
                    // setup the SMBus to write the cmd 
                    SMBus_ReadWriteWord(BATT_ADDR, *writeSetup.pRegs2Write,
                            WRITE_WORD, writeSetup.pRegWriteData);

                    writeSetup.numRegs--;
                    currentState = WritingRegs;
                } else {
                    // setup the SMBus machine to read the first register
                    SMBus_ReadWriteWord(BATT_ADDR, *readSetup.pRegs2Read,
                            READ_WORD, regBytes);
                    // increment pointer to regs
                    readSetup.pRegs2Read++;
                    // decrement numRegs
                    readSetup.numRegs--;
                    // transition to ReadingRegs
                    currentState = ReadingRegs;
                }
            }
        }
            break;
            /* ---------------------------------------------------------- ReadingRegs */
        case ReadingRegs: // If current state is state one
        {
#ifdef  TRACE_OUT  
            DBG_printf("RunReadRegsFSM(): ReadingRegs\n");
#endif                    
            // if event is EOT
            if (ThisEvent.EventType == SMBUS_EOT) {
#ifdef TEST_RETRY_TIME                
                uint16_t startTime = getSysTickCounter();
#endif                
                // if there is no error
                if (ThisEvent.data == SMBUS_NO_ERROR) {
                    // convert receive buffer into word         
                    // place word in given regData array
                    *readSetup.pRegData = buffer2Word(regBytes);
                    // if there are more register to read
                    if (readSetup.numRegs) {
                        // increment pointer to regData
                        readSetup.pRegData++;
                        // Restart the listening timer
                        timer_InitTimer(REG_READ_TIMER, LISTENING_TIME);
                        // transition to Listening
                        currentState = Listening;
                    } else // else done reading registers
                    {
                        // Post Read done with no errors
                        event_t Event2Post;
                        Event2Post.EventType = RR_READ_DONE;
                        Event2Post.data = SMBUS_NO_ERROR;
                        SM_PostAll(Event2Post);
                        // transition back to idle
                        currentState = RR_Idle;
                        // set retries to 0
                        retries = 0;
                    }
                } else // else if there was some error
                {
                    // if we have retries
#ifdef TRACE_OUT                                        
                    DBG_printf("RunReadRegsFSM(): Error %s retries %u\n",  GetSMBusErrorString(ThisEvent.data), retries);
#endif                    
                    if (retries < MAX_REG_RETRIES) {
#ifdef TEST_RETRY_TIME  
                        uint16_t elapsedTime = getSysTickCounter() - startTime;
                        DBG_printf("RunReadRegsFSM(): Error Reading retry %u: retry time %u mS\n", retries, elapsedTime);     
#endif                        
                        // re-perform
                        SMBus_ReadWriteWord(BATT_ADDR, *readSetup.pRegs2Read,
                                READ_WORD, regBytes);
                        // increment retry count
                        retries++;
                    } else {                   
                        //Post read done with error
#ifdef TRACE_OUT                             
                        DBG_printf("RunReadRegsFSM(): Posting Error %s\n", GetSMBusErrorString(ThisEvent.data));
#endif                        
                        event_t Event2Post;
                        Event2Post.EventType = RR_READ_DONE;
                        Event2Post.data = ThisEvent.data;
                        SM_PostAll(Event2Post);
                        // transition back to idle
                        currentState = RR_Idle;
                        // set retries to 0
                        retries = 0;
                    }//end if
                }// end if
            }// end if
        }
            break;
            /* case Writing  registers----------------------------------------------------------------------------*/
        case WritingRegs: // If current state is state one
        {
#ifdef  TRACE_OUT
            DBG_printf("RunWritingRegsFSM(): WritingRegs\n");
#endif                    
            // if event is EOT
            if (ThisEvent.EventType == SMBUS_EOT) {
                // if there is no error
                if (ThisEvent.data == SMBUS_NO_ERROR) {
                    // if there are more register to write
                    if (writeSetup.numRegs) {
                        // increment pointer to the register and write command
                        //writeSetup.pRegs2Write 
                        //writeSetup.pRegWriteData// += 2; // two bytes at a time
                        // Restart the listening timer
                        timer_InitTimer(REG_READ_TIMER, LISTENING_TIME);
                        // transition to Listening
                        currentState = Listening;
                    } else // else done writing registers
                    {
                        // Post Read done with no errors
                        event_t Event2Post;
                        Event2Post.EventType = RR_WRITE_DONE;
                        Event2Post.data = SMBUS_NO_ERROR;
                        SM_PostAll(Event2Post);
                        // transition back to idle
                        currentState = RR_Idle;
                        // set retries to 0
                        retries = 0;
                    }
                } else // else if there was some error
                {
                    // if we have retries
#ifdef TRACE_ERRORS
                    DBG_printf("RunReadRegsFSM(): Error reading register. %u retries remaining\n", (MAX_REG_RETRIES - retries));
#endif                    
                    if (retries < MAX_REG_RETRIES) {
                        // re-perform
                        SMBus_ReadWriteWord(BATT_ADDR, *writeSetup.pRegs2Write,
                                WRITE_WORD, writeSetup.pRegWriteData);
                        // increment retry count
                        retries++;
                    } else // else
                    {
                        //Post read done with error
#ifdef TRACE_ERRORS                        
                        DBG_printf("RunReadRegsFSM(): Read failed %u\n");
#endif                        
                        event_t Event2Post;
                        Event2Post.EventType = RR_READ_DONE;
                        Event2Post.data = ThisEvent.data;
                        SM_PostAll(Event2Post);
                        // transition back to idle
                        currentState = RR_Idle;
                        // set retries to 0
                        retries = 0;
                    }//end if
                }// end if
            }// end if
        }
            break;
            /* -------------------------------------------------------- END OF STATES */
            // repeat state pattern as required for other states
        default:
            ;
    } // end switch on Current State
    return ReturnEvent;
}

/**
 * QueryReadRegsSM
 * @return ReadRegState_t current state
 */
ReadRegState_t QueryReadRegsSM(void) {
    return currentState;
}

/**
 * @function ReadRegs_Start
 * @param pUserSetup contains data about which battery and registers to read.
 * @return true if the start read event is posted to the Queue, false otherwise.
 */
bool ReadRegs_Start(ReadSetup_t *pUserSetup) {
#ifdef DEBUG_OUT
    DBG_printf("ReadRegs_Start\n");
#endif    
    event_t readRegistersEvent = {RR_START_READ, 0};
    // if battery value and size makes sense and not reading right now

    __conditional_software_breakpoint(currentState == RR_Idle);
    if ((pUserSetup->battNum < NUM_BATTS)
            && (pUserSetup->numRegs < MAX_REGS)
            && (currentState == RR_Idle)) {
        // copy over
        readSetup = *pUserSetup;
        // post to start  
        return EnqueueFIFO(pQueue, readRegistersEvent);

    } else {
#ifdef TRACE_ERRORS                                                                        
        DBG_printf("*** ReadRegs_Start failed *** \r\n");
#endif        
        return false;
    }
}

/**
 * @function WriteRegs_start
 * @param WriteSetup_t pointer to write setup
 * @return bool true if successful, false otherwise
 */
bool WriteRegs_Start(WriteSetup_t *pWriteSetup) {
#ifdef TRACE_ERRORS                                  
    DBG_printf("WriteRegs_Start\n");
#endif            
    event_t writeRegistersEvent = {RR_START_WRITE, 0};
    if ((pWriteSetup->battNum < NUM_BATTS)
            && (pWriteSetup->numRegs < MAX_REGS)
            && (currentState == RR_Idle)) {
        // copy over
        writeSetup = *pWriteSetup;
        // post to start  
        return EnqueueFIFO(pQueue, writeRegistersEvent);
    } else {
        return false;
    }

}

/**
 * Description: resets the readRegister state machine to recover from an error.
 */
void ReadRegistersReset() {
    initQueue(pQueue);
    currentState = RR_Idle;
    timer_StopTimer(REG_READ_TIMER);
    InitReadRegsFSM(pQueue);
}

/***************************************************************************
 private functions
 ***************************************************************************/

/**
 * @function buffer2Word
 * @description Converts the 2 byte array from a register into a register word
 *              Based on the SMBus Data spec for the battery, bytes are
 *              transmitted with the low byte first. 
 * @param pBuff convert data from buffer to a work. 
 * @return uint16_t word 16 bit battery register value
 */
static uint16_t buffer2Word(uint8_t* pBuff) {
    // 1st byte in the LoByte and 2nd is the HiByte
    uint16_t word = (uint16_t) ((*(pBuff + 1) << 8) + *pBuff);
    return word;
}


