/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Authors: rmerchant, Ed Gavin
** Purpose: Controls the indicator LED's 
****************************************************************************/

#ifndef COMPCOMM_H
#define	COMPCOMM_H

#ifdef	__cplusplus
extern "C" {
#endif

#define MAX_PACKET_SIZE 80 
    
#define SERIAL_SIZE  
#define SIZE_INDEX 0
#define REQ_INDEX 1

// Public function prototypes
void CompComm_HWInit(void);
bool CompComm_Write(uint8_t* pData, uint8_t bytes);

#ifdef  PC_SERIAL_INPUT_ENABLED
bool CompComm_Read(uint8_t* pData);
bool CompComm_PCDataReady(void);
#endif 

//Event Checkers (pseudo)
bool Check4RcBuffFull(void);
bool Check4TxBuffFull(void);

// ISRs
void ISR_TxCompComm(void);
void ISR_RxCompComm(void);

// Event Checkers
#ifdef  PC_SERIAL_INPUT_ENABLED
bool Check4Keystroke(void);
#endif

#ifdef	__cplusplus
}
#endif

#endif	/* COMPCOMM_H */

