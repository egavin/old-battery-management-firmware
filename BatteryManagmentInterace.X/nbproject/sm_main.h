/*****************************************************************************
* sm_queue.h
*      Copyright (c) 2021 Rose Garden Software Inc.  All rights reserved.
*      Reproduction of transmission in whole or in part, in
*      any form or by any means, electronic, mechanical, or otherwise, is
*      prohibited without the prior written consent of the copyright owner.
* Purpose: functions related to the event framework
*
*  Author: Ed Gavin
****************************************************************************/

#ifndef SM_MAIN_H
#define	SM_MAIN_H

extern static queue_t SMBusMasterEventQueue;


#endif	/* SM_MAIN_H */

