/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Authors: rmerchant, Ed Gavin
** Purpose: State machine to read the battery registers
****************************************************************************/

#ifndef ReadRegsFSM_H
#define ReadRegsFSM_H

#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include "sm_events.h"
#include "sm_queue.h"

#define MAX_REGS 22
// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  RR_InitPState,  // Initial Pseudo State
  RR_Idle,        // Machine is idle
  Writing2Mux,    // Machine is sending a byte to the mux to select the channel
  Listening,      // Listening to the bus between reads
  ReadingRegs,     // Reading the registers from the battery
  WritingRegs      // Writing to battery registers        
}ReadRegState_t;

// define struct for info needed to setup a read
typedef struct ReadSetup
{
  unsigned battNum :2;  // Battery to read
  uint8_t numRegs;      // number of registers to read
  uint8_t* pRegs2Read;  // pointer to array of the registers to read
  uint16_t* pRegData;   // pointer to array to store register words
}ReadSetup_t;

typedef struct WriteSetup
{
unsigned battNum :2;  // Battery to write
  uint8_t numRegs;      // number of registers to write
  uint8_t* pRegs2Write;  // pointer to array of the registers to write
  uint8_t* pRegWriteData;   // pointer to array of data to write to registers    
}WriteSetup_t;

#define MUX_ERR 0xff //define a mux error
// Public Function Prototypes
bool InitReadRegsFSM(queue_t* qp);
bool PostReadRegsFSM(event_t ThisEvent);
event_t RunReadRegsFSM(event_t ThisEvent);
ReadRegState_t QueryReadRegsSM(void);

bool ReadRegs_Start(ReadSetup_t *pUserSetup);

bool WriteRegs_Start(WriteSetup_t *pWriteSetup);
void ReadRegistersReset(void);

#endif /* ReadRegsFSM_H */

