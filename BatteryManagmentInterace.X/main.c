/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Authors: Ed Gavin
** Purpose: Entry point for program execution
****************************************************************************/

#include "mcc_generated_files/mcc.h"
#include "CompComm.h"
#include "sm_port.h"
#include "sm_events.h"
#include "sm_timer.h"
#include "sm_queue.h"
#include "sm_main.h"

#ifdef MAIN_DEBUG_PRINT_ENABLED
#define DEBUG_PRINT_ENABLED
#include "dbprintf.h"
#endif

// for checksum
typedef unsigned char readType;
typedef unsigned int resultType; 
extern const readType checksumData[0x100] __at(0x0);
extern const resultType hexmateChecksum  __at(0x100);

#define POLYNOMIAL   0x1021
#define WIDTH   (8 * sizeof(resultType))
#define MSb     ((resultType)1 << (WIDTH - 1))
 
resultType crc(const readType * data, unsigned n, resultType remainder) {
    unsigned pos;
    unsigned char bitp;
 
    for (pos = 0; pos != n; pos++) {
        remainder ^= ((resultType)data[pos] << (WIDTH - 8));
        for (bitp = 8; bitp > 0; bitp--) {
            if (remainder & MSb) {
                remainder = (remainder << 1) ^ POLYNOMIAL;
            } else {
                remainder <<= 1;
            }
        }
    }
 
    return remainder;
}    

/*
                         Main application
 */
void main(void)
{
    
    // Initialize the device
     SYSTEM_Initialize();
    _HW_PIC18F26Q10Init();
    timer_initialize();
    sm_initialize();
    CompComm_HWInit();    
    PWR_12_VOLT_EN_SetLow(); // enable the 12 volt power supply

    if( hexmateChecksum != crc(checksumData,sizeof(checksumData)/sizeof(readType), 0xFFFF )){
        // if the CRC fails, set the LED's red and stop execution
        LED_AC_PWR_RED_SetHigh();
        LED_BAT_CRG_RED_SetHigh();
        LED_BAT_STAT_RED_SetHigh();
        // endless loop
        while(1){
            ;
        }                
    }
    

#ifdef  MAIN_DEBUG_PRINT_ENABLED  
DBG_printf("%s ***** Battery Manager Application Start******\r\n", GetSecondsMinutesHours());
#endif

    while (1)
    {
            SM_Run();                        
    }
}
/**
 End of File
*/