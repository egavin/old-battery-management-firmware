/****************************************************************************
** Copyright (c) 2021 Supira Medical
** Author(s): Ed Gavin
** Purpose:  defines and function declarations related to the 
** state machine framework
****************************************************************************/

#ifndef SM_MAIN_H
#define	SM_MAIN_H
#include "sm_queue.h"

// #defines 
#define BOARD_V2        // second version of board
#define FIRMWARE_VERSION 002

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

#ifdef DEBUG_LOGGING
#define DEBUG_ENABLED // enable for serial debug output
#define MAIN_DEBUG_PRINT_ENABLED
#define MASTER_FSM_DEBUG 1
#define SMBUS_I2C_DEBUG 1
#define INDICATOR_DEBUG 1
#define READ_REGISTERS_DEBUG 1
#endif

// define global defines for debugging
#ifdef DEBUG_LOGGING_ALL
#define MAIN_DEBUG_PRINT_ENABLED
#define SM_MAIN_DEBUG 1
#define MASTER_FSM_DEBUG 1
#define LOG_LED_STATE 
#define READ_REGISTERS_DEBUG 1
#define SMBUS_ERROR_DEBUG 1
#define SMBUS_I2C_DEBUG 1
#define INDICATOR_DEBUG 1
#define SM_BUSMASTER_DEBUG 1
#define SM_TIMER_DEBUG
#define MAIN_DEBUG
#endif

//#define CHECK_EVENT_STATS
#ifdef CHECK_EVENT_STATS
#define SM_MAIN_DEBUG
#endif

// function typdef's 
typedef bool CheckFunc (void);
typedef CheckFunc (*pCheckFunc);

typedef enum
{
  Success     = 0,
  FailedPost  = 1,
  FailedRun,
  FailedPointer,
  FailedIndex,
  FailedInit
}SM_return_t;

#ifdef  PC_SERIAL_INPUT_ENABLED
#define EVENT_CHECK_LIST Check4IntFlag_SMBus, Check4Stop_SMBus, \
                          Check4BusColl_SMBus, Check4RcBuffFull,\
                          Check4PinChanges, Check4TxBuffFull, Check4Keystroke
#else
#define EVENT_CHECK_LIST Check4IntFlag_SMBus, Check4Stop_SMBus, \
                          Check4BusColl_SMBus, Check4RcBuffFull,\
                          Check4PinChanges, Check4TxBuffFull
#endif
extern queue_t  MasterQueue;
extern queue_t SMBusMasterEventQueue;

void sm_initialize(void);
void SM_Run(void);
bool SM_PostAll(event_t event);
bool SM_CheckUserEvents();
uint8_t GetMaxQueueSize(void);
void LogAllQueues(void);
void logStateMachinesState();
void logAll(void);
#endif	/* SM_MAIN_H */

